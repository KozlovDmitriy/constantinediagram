﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram.Elements
{
    /// <summary>
    /// Реализует стрелки для <see cref="Conditions"/>.
    /// </summary>
    public class ConditionArrows : Arrows
    {
        /// <summary>
        /// Возвращает или задает, объект <see cref="Conditions"/>,
        /// которому принадлежит данная <see cref="ConditionArrows"/>.
        /// </summary>
        public Conditions Condition { get; private set; }

        /// <summary>
        /// Инициализирующий конструктор для <see cref="ConditionArrows"/>
        /// </summary>
        /// <param name="StartPoint">Начальная точка стрелки.</param>
        /// <param name="EndPoint">Конечная точка стрелки.</param>
        /// <param name="condition">объект <see cref="Conditions"/>,
        /// которому принадлежит данная <see cref="ConditionArrows"/>.</param>
        public ConditionArrows( Point StartPoint, Point EndPoint, Conditions condition ) {
            base.StartPoint = StartPoint;
            base.EndPoint = EndPoint;
            base.Id = WorkPanel.getNextId();
            this.Condition = condition;
        }
    }
}
