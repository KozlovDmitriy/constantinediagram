﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram.Elements
{
    /// <summary>
    /// Описывает повидение границ элементов типа "Область данных".
    /// </summary>
    public class DataAreaBorder : Borders
    {
        #region Типы данных

        /// <summary>
        /// Описывает стрелку как объект связи с <see cref="DataAreaBorder"/>.
        /// </summary>
        public struct ConnectedArrow
        {
            /// <summary>
            /// Связывваемая стрелка.
            /// </summary>
            public Arrows arrow;
            /// <summary>
            /// Возвращает или задает <see cref="Borders.InOrOutArrow"/> для <see cref="DataAreaBorder.ConnectedArrow"/>.
            /// </summary>
            public InOrOutArrow in_or_out_arrow;
            /// <summary>
            /// Угол смещения относительно оси абсцисс.
            /// </summary>
            public double angle;
        }

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает список присоединенных к <see cref="DataAreaBorder"/> стрелок.
        /// </summary>
        public List<ConnectedArrow> ConnectedArrows {
            get { return this.connected_arrows; }
        }

        #endregion

        #region Поля

        /// <summary>
        /// Список всех связей с <see cref="DataAreaBorder"/>
        /// </summary>
        private List<ConnectedArrow> connected_arrows = new List<ConnectedArrow>();

        #endregion

        #region Функции-члены

        /// <summary>
        /// Позицианирует связь по центру границы элемента.
        /// </summary>
        /// <param name="point">Первоначальная точка связи.</param>
        /// <returns>Новая точка связи</returns>
        private Point getActualBorderConnectPoint( Point point ) {
            Point ret = Center;
            ret.X = Center.X + getEllipseA() * Math.Cos( getAngle( point ) );
            ret.Y = Center.Y + getEllipseB() * Math.Sin( getAngle( point ) );
            return ret;
        }

        /// <summary>
        /// Получает размер большой полуоси элипса представляющего собой центр границы элемента <see cref="DataAreBorder"/>.
        /// </summary>
        /// <returns>Размер большой полуоси.</returns>
        private double getEllipseA() {
            return this.ActualWidth / 2 - BorderThickness.Bottom;
        }

        /// <summary>
        /// Получает размер меньшей полуоси элипса представляющего собой центр границы элемента <see cref="DataAreBorder"/>.
        /// </summary>
        /// <returns>Размер меньшей полуоси.</returns>
        private double getEllipseB() {
            return this.ActualHeight / 2 - BorderThickness.Bottom;
        }

        /// <summary>
        /// Получает угол смещения вектора {<see cref="Border.Center"/>,point} относительно оси абсцисс элипса
        /// представляющего собой центр границы элемента.
        /// </summary>
        /// <param name="point">Конец вектора {<see cref="Border.Center"/>,point}.</param>
        /// <returns>Угол смещения вектора.</returns>
        private double getAngle( Point point ) {
            double psi = Math.Atan2( ( point.Y - Center.Y ), ( point.X - Center.X ) );
            double fi = Math.Atan2( getEllipseA() * Math.Sin( psi ), getEllipseB() * Math.Cos( psi ) );
            return fi;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Обновляет все связи стрелек с <see cref="DataAreBorder"/>.
        /// </summary>
        override public void updateConnections() {
            foreach ( ConnectedArrow connected_arrow in connected_arrows ) {
                Point point = new Point(
                            Center.X + getEllipseA() * Math.Cos( connected_arrow.angle ),
                            Center.Y + getEllipseB() * Math.Sin( connected_arrow.angle )
                        );
                if ( connected_arrow.in_or_out_arrow == InOrOutArrow.OUT ) {
                    connected_arrow.arrow.StartPoint = point;
                }
                else {
                    connected_arrow.arrow.EndPoint = point;
                }
            }
        }

        /// <summary>
        /// Присоединяет стрелку к <see cref="DataAreaBorder"/>.
        /// </summary>
        /// <param name="arrow">Стрелка.</param>
        /// <param name="in_or_out_arrow">Описывает <see cref="InOrOutArrow"/> для присоединяемой стрелки.</param>
        /// <returns>true - если стрелка присоединена.</returns>
        override public bool connectArrow( Arrows arrow, InOrOutArrow in_or_out_arrow ) {
            if ( arrow != null && !this.connected_arrows.Any( item => item.arrow == arrow ) ) {
                ConnectedArrow connected_arrow = new ConnectedArrow();
                connected_arrow.arrow = arrow;
                connected_arrow.in_or_out_arrow = in_or_out_arrow;
                if ( in_or_out_arrow == InOrOutArrow.IN ) {
                    arrow.EndPoint = getActualBorderConnectPoint( arrow.EndPoint );
                    connected_arrow.angle = this.getAngle( arrow.EndPoint );
                }
                else {
                    arrow.StartPoint = getActualBorderConnectPoint( arrow.StartPoint );
                    connected_arrow.angle = this.getAngle( arrow.StartPoint );
                }
                this.connected_arrows.Add( connected_arrow );
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Присоединяет стрелку к <see cref="DataAreaBorder"/>.
        /// </summary>
        /// <param name="connected_arrow">Объект <see cref="ConnectedArrow"/>,
        /// описывающий параметры присоединения стрелки.</param>
        /// <returns>true - если стрелка присоединена успешно.</returns>
        internal bool connectArrow( ConnectedArrow connected_arrow ) {
            if ( connected_arrow.arrow != null
                && !this.connected_arrows.Any( item => item.arrow == connected_arrow.arrow ) ) {
                this.connected_arrows.Add( connected_arrow );
                this.updateConnections();
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Отсоединяет стрелку от <see cref="DataAreaBorder"/>.
        /// </summary>
        /// <param name="arrow">Стрелка.</param>
        /// <param name="in_or_out_arrow">Описывает <see cref="InOrOutArrow"/> для отсоединяемой стрелки.</param>
        /// <returns>true - если стрелка отсоединена.</returns>
        override public bool disconnectArrow( Arrows arrow, InOrOutArrow in_or_out_arrow ) {
            if ( this.connected_arrows.Any( item => item.arrow == arrow && item.in_or_out_arrow == in_or_out_arrow ) ) {
                this.connected_arrows.Remove( this.connected_arrows.First( item => item.arrow == arrow ) );
                return true;
            }
            else {
                return false;
            }
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие MouseLeave для <see cref="DataAreaBorder"/>.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры</param>
        override protected void Border_MouseLeave( object sender, MouseEventArgs e ) {
            if ( this.IsActive ) {
                this.IsActive = false;
            }
        }

        /// <summary>
        /// Описывает реакцию на событие MouseMove для <see cref="DataAreaBorder"/>.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры</param>
        override protected void Border_MouseMove( object sender, MouseEventArgs e ) {
            bool fl = !this.Child.IsMouseOver
                    && ( WorkPanel.CurrentWorkPanel.IsArrowMoving || WorkPanel.CurrentWorkPanel.IsArrowAdding );
            if ( fl ) {
                this.IsActive = true;
            }
            else {
                this.IsActive = false;
            }
        }

        #endregion
    }
}
