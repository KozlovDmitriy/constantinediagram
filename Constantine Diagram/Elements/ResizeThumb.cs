﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.MathModel;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram.Elements
{
    /// <summary>
    /// Реализует поведения объекта типа Thumb для изменения размера элемента.
    /// </summary>
    public class ResizeThumb : Thumb
    {
        #region Поля

        /// <summary>
        /// Значение Canvas.Left до перетягивания.
        /// </summary>
        private double old_left = 0;
        /// <summary>
        /// Значение Canvas.Top до перетягивания.
        /// </summary>
        private double old_top = 0;
        /// <summary>
        /// Значение ширины элемента до изменения размеров.
        /// </summary>
        private double old_width = 0;
        /// <summary>
        /// Значение высоты элемента до изменения размеров.
        /// </summary>
        private double old_height = 0;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public ResizeThumb() {
            DragDelta += new DragDeltaEventHandler( this.ResizeThumb_DragDelta );
            DragStarted += new DragStartedEventHandler( this.ResizeThumb_DragStarted );
            DragCompleted += new DragCompletedEventHandler( this.ResizeThumb_DragCompleted );
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие DragStarted для <see cref="ResizeThumb"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void ResizeThumb_DragStarted( object sender, DragStartedEventArgs e ) {
            ContentContainer container = this.DataContext as ContentContainer;
            this.old_left = Canvas.GetLeft( container );
            this.old_top = Canvas.GetTop( container );
            this.old_width = container.ActualWidth;
            this.old_height = container.ActualHeight;
        }

        /// <summary>
        /// Описывает реакцию на событие DragDelta для <see cref="ResizeThumb"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void ResizeThumb_DragDelta( object sender, DragDeltaEventArgs e ) {
            Control designerItem = this.DataContext as Control;
            if ( designerItem != null ) {
                double deltaVertical, deltaHorizontal;
                switch ( VerticalAlignment ) {
                    case VerticalAlignment.Bottom:
                        deltaVertical = Math.Min( -e.VerticalChange, designerItem.ActualHeight - designerItem.MinHeight );
                        designerItem.Height -= deltaVertical;
                        break;
                    case VerticalAlignment.Top:
                        deltaVertical = Math.Min( e.VerticalChange, designerItem.ActualHeight - designerItem.MinHeight );
                        Canvas.SetTop( designerItem, Canvas.GetTop( designerItem ) + deltaVertical );
                        designerItem.Height -= deltaVertical;
                        break;
                    default:
                        break;
                }
                switch ( HorizontalAlignment ) {
                    case HorizontalAlignment.Left:
                        deltaHorizontal = Math.Min( e.HorizontalChange, designerItem.ActualWidth - designerItem.MinWidth );
                        Canvas.SetLeft( designerItem, Canvas.GetLeft( designerItem ) + deltaHorizontal );
                        designerItem.Width -= deltaHorizontal;
                        break;
                    case HorizontalAlignment.Right:
                        deltaHorizontal = Math.Min( -e.HorizontalChange, designerItem.ActualWidth - designerItem.MinWidth );
                        designerItem.Width -= deltaHorizontal;
                        break;
                    default:
                        break;
                }
            }
            WorkPanel.CurrentWorkPanel.InvalidateMeasure();
            e.Handled = true;
        }

        /// <summary>
        /// Описывает реакцию на событие DragCompleted для <see cref="ResizeThumb"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void ResizeThumb_DragCompleted( object sender, DragCompletedEventArgs e ) {
            ContentContainer container = this.DataContext as ContentContainer;
            List<IDiagrammElement> list = new List<IDiagrammElement>();
            Helpers.FindAllDiagrammElements( container, list );
            if ( !WorkPanel.CurrentWorkPanel.canLocale( list[0] ) ) {
                Canvas.SetLeft( container, this.old_left );
                Canvas.SetTop( container, this.old_top );
                container.Width = this.old_width;
                container.Height = this.old_height;
                DialogWithUser.showMessage( "Элементы диаграммы не должны пересекаться." );
            }
            else {
                WorkPanel.CurrentWorkPanel.updateConectionsUnderControl( container );
                DialogWithUser.showMessage( "Изменен размер элемента диаграммы." );
            }
        }

        #endregion
    }
}
