﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram.Elements
{
    /// <summary>
    /// Описывает повидение границ элементов типа "Модуль".
    /// </summary>
    public class ModuleBorder : Borders
    {
        #region Типы данных

        /// <summary>
        /// Описывает состояние для <see cref="ModuleBorder"/> как внутреннего [IN] или внешнего [OUT].
        /// </summary>
        public enum InOrOut : uint
        {
            IN = 0,
            OUT = 1
        }

        /// <summary>
        /// Описывает стрелку как объект связи с <see cref="ModuleBorder"/>.
        /// </summary>
        public struct ConnectedArrow
        {
            /// <summary>
            /// Связывваемая стрелка.
            /// </summary>
            public Arrows arrow;
            /// <summary>
            /// Возвращает или задает <see cref="Borders.InOrOutArrow"/> для <see cref="ModuleBorder.ConnectedArrow"/>.
            /// </summary>
            public InOrOutArrow in_or_out_arrow;
            /// <summary>
            /// Возвращает или задает <see cref="ModuleBorder.ConnectOrientation"/> для <see cref="ModuleBorder.ConnectedArrow"/>.
            /// </summary>
            public ConnectOrientation arrow_point_location;
            /// <summary>
            /// Коэффициент смещения точки привязки относительно начала грани.
            /// </summary>
            public double indent_coefficient;
        }

        /// <summary>
        /// Описывает условный элемент как объект связи с <see cref="ModuleBorder"/>.
        /// </summary>
        public struct ConnectedCondition
        {
            /// <summary>
            /// Связывваемый условный элемент.
            /// </summary>
            public Conditions condition;
            /// <summary>
            /// Возвращает или задает <see cref="ModuleBorder.ConnectOrientation"/> для <see cref="ModuleBorder.ConnectedCondition"/>.
            /// </summary>
            public ConnectOrientation point_location;
            /// <summary>
            /// Коэффициент смещения точки привязки относительно начала грани.
            /// </summary>
            public double indent_coefficient;
        }

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает <see cref="ModuleBorder.InOrOut"/> для <see cref="ModuleBorder"/>.
        /// </summary>
        public InOrOut InOrOutBorder { get; set; }

        /// <summary>
        /// Возвращает <see cref="ConnectedCondition"/>, описывающий параметры 
        /// присоединения объекта <see cref="Conditions"/> к объекту <see cref="ModuleBorder"/>.
        /// </summary>
        public ConnectedCondition ConnectCondition {
            get { return this.connected_condition; }
        }

        /// <summary>
        /// Возвращает список присоединенных к <see cref="ModuleBorder"/> стрелок.
        /// </summary>
        public List<ConnectedArrow> ConnectedArrows {
            get { return this.connected_arrows; }
        }

        #endregion

        #region Поля

        /// <summary>
        /// Список всех связей типа <see cref="Arrows"/> с <see cref="ModuleBorder"/>
        /// </summary>
        private List<ConnectedArrow> connected_arrows = new List<ConnectedArrow>();

        /// <summary>
        /// Cвязь типа <see cref="Arrows"/> с <see cref="ModuleBorder"/>
        /// </summary>
        private ConnectedCondition connected_condition;

        #endregion

        #region Функции-члены

        /// <summary>
        /// Возвращает <see cref="ModuleBorder.ConnectOrientation"/> характеризующюю какой из граней принадлежит точка.
        /// </summary>
        /// <param name="point">Анализируемая точка.</param>
        /// <returns><see cref="ModuleBorder.ConnectOrientation"/> характеризующая какой из граней принадлежит точка.</returns>
        private ConnectOrientation getConnectOrientation( Point point ) {
            ConnectOrientation ret = new ConnectOrientation();
            if ( point.X < ( this.Center.X - this.ActualWidth / 2 + this.BorderThickness.Left * 2 ) ) {
                ret = ConnectOrientation.LEFT;
            }
            if ( point.X > ( this.Center.X + this.ActualWidth / 2 - this.BorderThickness.Right * 2 ) ) {
                ret = ConnectOrientation.RIGHT;
            }
            if ( point.Y < ( this.Center.Y - this.ActualHeight / 2 + this.BorderThickness.Top * 2 ) ) {
                ret = ConnectOrientation.TOP;
            }
            if ( point.Y > ( this.Center.Y + this.ActualHeight / 2 - this.BorderThickness.Bottom * 2 ) ) {
                ret = ConnectOrientation.BOTTOM;
            }
            return ret;
        }

        /// <summary>
        /// Позицианирует связь по центру границы элемента.
        /// </summary>
        /// <param name="point">Первоначальная точка связи.</param>
        /// <returns>Новая точка связи</returns>
        private Point getActualBorderConnectPoint( Point point ) {
            double x = point.X;
            double y = point.Y;
            if ( this.getConnectOrientation( point ) == ConnectOrientation.RIGHT ) {
                x = Center.X + this.ActualWidth / 2 - this.BorderThickness.Right;
            }
            if ( this.getConnectOrientation( point ) == ConnectOrientation.LEFT ) {
                x = Center.X - this.ActualWidth / 2 + this.BorderThickness.Left;
            }
            if ( this.getConnectOrientation( point ) == ConnectOrientation.BOTTOM ) {
                y = Center.Y + this.ActualHeight / 2 - this.BorderThickness.Bottom;
            }
            if ( this.getConnectOrientation( point ) == ConnectOrientation.TOP ) {
                y = Center.Y - this.ActualHeight / 2 + this.BorderThickness.Top;
            }
            return new Point( x, y );
        }

        /// <summary>
        /// Для данной точки получает коэффициент смещения от начала грани элемента.
        /// </summary>
        /// <param name="point">Точка.</param>
        /// <param name="location">Характеризует <see cref="ModuleBorder.ConnectOrientation"/> для point.</param>
        /// <returns>Коэффициент смещения.</returns>
        private double getIndentCoefficient( Point point, ConnectOrientation location ) {
            double indent = 0.0;
            if ( location == ConnectOrientation.LEFT || location == ConnectOrientation.RIGHT ) {
                double y = Center.Y - this.ActualHeight / 2;
                indent = Math.Abs( point.Y - y ) / this.ActualHeight;
            }
            if ( location == ConnectOrientation.TOP || location == ConnectOrientation.BOTTOM ) {
                double x = Center.X - this.ActualWidth / 2;
                indent = Math.Abs( point.X - x ) / this.ActualWidth;
            }
            return indent;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Обновляет параметры связи с присоединенным объектом <see cref="Сonditions"/>
        /// </summary>
        public void updateConditionConnections() {
            Point point = Center;
            if ( this.InOrOutBorder == InOrOut.OUT && this.connected_condition.condition != null ) {
                if ( this.connected_condition.point_location == ConnectOrientation.LEFT ) {
                    point = new Point(
                                Center.X - this.ActualWidth / 2 + this.BorderThickness.Left,
                                Center.Y - this.ActualHeight / 2 + this.ActualHeight * this.connected_condition.indent_coefficient
                            );
                }
                if ( this.connected_condition.point_location == ConnectOrientation.RIGHT ) {
                    point = new Point(
                                Center.X + this.ActualWidth / 2 - this.BorderThickness.Right,
                                Center.Y - this.ActualHeight / 2 + this.ActualHeight * this.connected_condition.indent_coefficient
                            );

                }
                if ( this.connected_condition.point_location == ConnectOrientation.TOP ) {
                    point = new Point(
                                Center.X - this.ActualWidth / 2 + this.ActualWidth * this.connected_condition.indent_coefficient,
                                Center.Y - this.ActualHeight / 2 + this.BorderThickness.Top
                            );
                }
                if ( this.connected_condition.point_location == ConnectOrientation.BOTTOM ) {
                    point = new Point(
                                Center.X - this.ActualWidth / 2 + this.ActualWidth * this.connected_condition.indent_coefficient,
                                Center.Y + this.ActualHeight / 2 - this.BorderThickness.Bottom
                            );
                }
                this.connected_condition.condition.ConnectPoint = point;
            }
        }

        /// <summary>
        /// Обновляет все связи с <see cref="ModuleBorder"/>.
        /// </summary>
        override public void updateConnections() {
            Point point = Center;
            updateConditionConnections();
            foreach ( ConnectedArrow connected_arrow in connected_arrows ) {
                if ( connected_arrow.arrow_point_location == ConnectOrientation.LEFT ) {
                    point = new Point(
                            Center.X - this.ActualWidth / 2 + this.BorderThickness.Left,
                            Center.Y - this.ActualHeight / 2 + this.ActualHeight * connected_arrow.indent_coefficient
                        );
                }
                if ( connected_arrow.arrow_point_location == ConnectOrientation.RIGHT ) {
                    point = new Point(
                            Center.X + this.ActualWidth / 2 - this.BorderThickness.Right,
                            Center.Y - this.ActualHeight / 2 + this.ActualHeight * connected_arrow.indent_coefficient
                        );
                }
                if ( connected_arrow.arrow_point_location == ConnectOrientation.TOP ) {
                    point = new Point(
                            Center.X - this.ActualWidth / 2 + this.ActualWidth * connected_arrow.indent_coefficient,
                            Center.Y - this.ActualHeight / 2 + this.BorderThickness.Top
                        );
                }
                if ( connected_arrow.arrow_point_location == ConnectOrientation.BOTTOM ) {
                    point = new Point(
                                Center.X - this.ActualWidth / 2 + this.ActualWidth * connected_arrow.indent_coefficient,
                                Center.Y + this.ActualHeight / 2 - this.BorderThickness.Bottom
                            );
                }
                if ( connected_arrow.in_or_out_arrow == InOrOutArrow.IN ) {
                    connected_arrow.arrow.EndPoint = point;
                }
                else {
                    connected_arrow.arrow.StartPoint = point;
                }
            }
        }

        /// <summary>
        /// Присоединяет <see cref="Conditions"/> к <see cref="ModuleBorder"/>.
        /// </summary>
        /// <param name="condition">Условный элемент.</param>
        /// <returns>true - если условный элемент присоединен.</returns>
        public bool connectCondition( Conditions condition, Point point ) {
            if ( condition != null ) {
                ConnectedCondition connected_condition = new ConnectedCondition();
                connected_condition.condition = condition;
                connected_condition.point_location = this.getConnectOrientation( point );
                condition.ConditionConnectOrientation = connected_condition.point_location;
                //connected_condition.condition.ConnectPoint = getActualBorderConnectPoint( point );
                connected_condition.indent_coefficient =
                    this.getIndentCoefficient( connected_condition.condition.ConnectPoint, connected_condition.point_location );
                this.connected_condition = connected_condition;
                this.updateConnections();
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Присоединяет объект <see cref="Conditions"/> к <see cref="DataAreaBorder"/>.
        /// </summary>
        /// <param name="connected_arrow">Объект <see cref="ConnectedCondition"/>,
        /// описывающий параметры присоединения объекта <see cref="Conditions"/>.</param>
        /// <returns>true - если объект <see cref="Conditions"/> присоединена успешно.</returns>
        public bool connectCondition( ConnectedCondition connected_condition ) {
            if ( connected_condition.condition != null ) {
                this.connected_condition = connected_condition;
                this.updateConnections();
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Отсоединяет элемент типа <see cref="Conditions"/> от <see cref="ModuleBorder"/>.
        /// </summary>
        /// <param name="condition">Условный элемент.</param>
        /// <returns>true - если условный элемент отсоединен.</returns>
        public bool disconnectCondition( Conditions condition ) {
            if ( condition != null && this.connected_condition.condition == condition ) {
                this.connected_condition.condition = null;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Присоединяет стрелку к <see cref="ModuleBorder"/>.
        /// </summary>
        /// <param name="arrow">Стрелка.</param>
        /// <param name="in_or_out_arrow">Описывает <see cref="InOrOutArrow"/> для присоединяемой стрелки.</param>
        /// <returns>true - если стрелка присоединена.</returns>
        override public bool connectArrow( Arrows arrow, InOrOutArrow in_or_out_arrow ) {
            if ( arrow != null && !this.connected_arrows.Any( item => item.arrow == arrow ) ) {
                ConnectedArrow connected_arrow = new ConnectedArrow();
                connected_arrow.arrow = arrow;
                connected_arrow.in_or_out_arrow = in_or_out_arrow;
                if ( in_or_out_arrow == InOrOutArrow.IN ) {
                    arrow.EndPoint = getActualBorderConnectPoint( arrow.EndPoint );
                    connected_arrow.arrow_point_location = this.getConnectOrientation( arrow.EndPoint );
                    connected_arrow.indent_coefficient = this.getIndentCoefficient( arrow.EndPoint, connected_arrow.arrow_point_location );
                }
                else {
                    arrow.StartPoint = getActualBorderConnectPoint( arrow.StartPoint );
                    connected_arrow.arrow_point_location = this.getConnectOrientation( arrow.StartPoint );
                    connected_arrow.indent_coefficient = this.getIndentCoefficient( arrow.StartPoint, connected_arrow.arrow_point_location );
                }
                this.connected_arrows.Add( connected_arrow );
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Присоединяет стрелку к <see cref="ModuleBorder"/>.
        /// </summary>
        /// <param name="connected_arrow"><see cref="ModuleBorder.ConnectedArrow"/> описывающий присоединяемую стрелку.</param>
        /// <returns>true - если стрелка присоединена.</returns>
        public bool connectArrow( ConnectedArrow connected_arrow ) {
            if ( connected_arrow.arrow != null
                && !this.connected_arrows.Any( item => item.arrow == connected_arrow.arrow ) ) {
                this.connected_arrows.Add( connected_arrow );
                this.updateConnections();
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Отсоединяет стрелку от <see cref="ModuleBorder"/>.
        /// </summary>
        /// <param name="arrow">Стрелка.</param>
        /// <param name="in_or_out_arrow">Описывает <see cref="InOrOutArrow"/> для отсоединяемой стрелки.</param>
        /// <returns>true - если стрелка отсоединена.</returns>
        override public bool disconnectArrow( Arrows arrow, InOrOutArrow in_or_out_arrow ) {
            if ( this.connected_arrows.Any( item => item.arrow == arrow && item.in_or_out_arrow == in_or_out_arrow ) ) {
                this.connected_arrows.Remove( this.connected_arrows.First( item => item.arrow == arrow ) );
                return true;
            }
            else {
                return false;
            }
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие MouseLeave для <see cref="ModuleBorder"/>.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры</param>
        override protected void Border_MouseLeave( object sender, MouseEventArgs e ) {
            if ( this.IsActive ) {
                this.IsActive = false;
            }
            if ( this.InOrOutBorder == InOrOut.IN ) {
                this.BorderThickness = border_thickness;
            }
        }

        /// <summary>
        /// Описывает реакцию на событие MouseMove для <see cref="ModuleBorder"/>.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры</param>
        override protected void Border_MouseMove( object sender, MouseEventArgs e ) {
            bool fl = false;
            if ( this.InOrOutBorder == InOrOut.IN ) {
                fl = !this.Child.IsMouseOver && WorkPanel.CurrentWorkPanel.IsEndArrowMoving;
            }
            else {
                fl = !this.Child.IsMouseOver &&
                     (
                        WorkPanel.CurrentWorkPanel.IsArrowMoving ||
                        WorkPanel.CurrentWorkPanel.IsArrowAdding ||
                        WorkPanel.CurrentWorkPanel.IsConditionAdding ||
                        (
                            WorkPanel.CurrentWorkPanel.IsConditionMoving
                        )
                     );
            }
            if ( fl ) {
                this.IsActive = true;
                if ( this.InOrOutBorder == InOrOut.IN ) {
                    this.BorderThickness = border_thickness;
                }
            }
            else {
                this.IsActive = false;
                if ( !this.Child.IsMouseOver && this.InOrOutBorder == InOrOut.IN ) {
                    this.border_thickness = this.BorderThickness;
                    this.BorderThickness = new Thickness( 0 );
                }
            }
        }

        #endregion
    }
}
