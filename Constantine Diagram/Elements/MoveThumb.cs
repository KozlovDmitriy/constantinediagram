﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.MathModel;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram.Elements
{
    /// <summary>
    /// Реализует поведения объекта типа Thumb для перемещения элементов.
    /// </summary>
    public class MoveThumb : Thumb
    {
        #region Поля

        /// <summary>
        /// Значение Canvas.Left до перетягивания.
        /// </summary>
        private double old_left = 0;
        /// <summary>
        /// Значение Canvas.Top до перетягивания.
        /// </summary>
        private double old_top = 0;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public MoveThumb() {
            this.DragDelta += new DragDeltaEventHandler( MoveThumb_DragDelta );
            this.DragStarted += new DragStartedEventHandler( MoveThumb_DragStarted );
            this.DragCompleted += new DragCompletedEventHandler( MoveThumb_DragCompleted );
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие DragStarted для <see cref="MoveThumb"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void MoveThumb_DragStarted( object sender, DragStartedEventArgs e ) {
            ContentContainer container = this.DataContext as ContentContainer;
            old_left = Canvas.GetLeft( container );
            old_top = Canvas.GetTop( container );
        }

        /// <summary>
        /// Описывает реакцию на событие DragDelta для <see cref="MoveThumb"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void MoveThumb_DragDelta( object sender, DragDeltaEventArgs e ) {
            ContentContainer container = this.DataContext as ContentContainer;
            if ( container != null && !container.IsMouseDoubleClick ) {
                double left = Canvas.GetLeft( container );
                double top = Canvas.GetTop( container );
                Canvas.SetLeft( container, left + e.HorizontalChange );
                Canvas.SetTop( container, top + e.VerticalChange );
                WorkPanel.CurrentWorkPanel.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Описывает реакцию на событие DragCompleted для <see cref="MoveThumb"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void MoveThumb_DragCompleted( object sender, DragCompletedEventArgs e ) {
            ContentContainer container = this.DataContext as ContentContainer;
            List<IDiagrammElement> list = new List<IDiagrammElement>();
            Helpers.FindAllDiagrammElements( container, list );
            if ( !WorkPanel.CurrentWorkPanel.canLocale( list[0] ) ) {
                Canvas.SetLeft( container, this.old_left );
                Canvas.SetTop( container, this.old_top );
                DialogWithUser.showMessage( "Элементы диаграммы не должны пересекаться." );
            }
            else {
                WorkPanel.CurrentWorkPanel.updateConectionsUnderControl( container );
                DialogWithUser.showMessage( "Элемент диаграммы перемещен." );
            }
        }

        #endregion
    }
}
