﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram.Elements
{
    /// <summary>
    /// Описывает общее повидение границ элементов диаграммы.
    /// </summary>
    public abstract class Borders : Border
    {
        #region Поля

        /// <summary>
        /// Ширина границы элемента.
        /// </summary>
        protected Thickness border_thickness;
        protected bool is_active = false;

        #endregion

        #region Зависимые свойства

        /// <summary>
        /// Зависимое свойство для задания центра для <see cref="Borders"/>.
        /// </summary>
        public static readonly DependencyProperty CenterProperty =
            DependencyProperty.Register( "Center", typeof( Point ), typeof( Borders ),
                new FrameworkPropertyMetadata( new Point( 0, 0 ), FrameworkPropertyMetadataOptions.AffectsMeasure, centerUpdate ) );
        /// <summary>
        /// Возвращает или задает центр для <see cref="Borders"/>. 
        /// Это свойство зависимости.
        /// </summary>        
        public Point Center {
            get { return ( Point ) GetValue( CenterProperty ); }
            set {
                SetValue( CenterProperty, value );
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Присоединяет стрелку к <see cref="Borders"/>.
        /// Это абстрактная функция.
        /// </summary>
        /// <param name="arrow">Стрелка.</param>
        /// <param name="in_or_out_arrow">Описывает <see cref="InOrOutArrow"/> для присоединяемой стрелки.</param>
        /// <returns>true - если стрелка присоединена.</returns>
        abstract public bool connectArrow( Arrows arrow, InOrOutArrow in_or_out_arrow );
        /// <summary>
        /// Отсоединяет стрелку от <see cref="Borders"/>.
        /// Это абстрактная функция.
        /// </summary>
        /// <param name="arrow">Стрелка.</param>
        /// <param name="in_or_out_arrow">Описывает <see cref="InOrOutArrow"/> для отсоединяемой стрелки.</param>
        /// <returns>true - если стрелка отсоединена.</returns>
        abstract public bool disconnectArrow( Arrows arrow, InOrOutArrow in_or_out_arrow );

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает флаг активности для <see cref="Borders"/>.
        /// </summary>
        public bool IsActive {
            get {
                return this.is_active;
            }
            set {
                is_active = value;
                if ( value ) {
                    this.Style = this.ActiveStyle;
                }
                else {
                    this.Style = this.DefaultStyle;
                }
            }
        }

        /// <summary>
        /// Возвращает или задает "активный" стиль для <see cref="Borders"/>.
        /// </summary>
        public Style ActiveStyle { get; set; }

        /// <summary>
        /// Возвращает или задает стиль по умолчанию стиль для <see cref="Borders"/>.
        /// </summary>
        public Style DefaultStyle { get; set; }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Инициализирует объект <see cref="Borders"/>.
        /// </summary>
        public Borders() {
            this.Initialized += new EventHandler( Borders_Initialized );
            this.MouseMove += new System.Windows.Input.MouseEventHandler( Border_MouseMove );
            this.MouseLeave += new System.Windows.Input.MouseEventHandler( Border_MouseLeave );
        }

        #endregion

        #region Функции-члены

        /// <summary>
        /// Обновляет все связи стрелек с <see cref="Borders"/>.
        /// Это абстрактная функция.
        /// </summary>
        abstract public void updateConnections();

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие инициализации элемента <see cref="Borders"/>.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры</param>
        private void Borders_Initialized( object sender, EventArgs e ) {
            this.border_thickness = ( sender as Borders ).BorderThickness;
        }

        /// <summary>
        /// Описывает реакцию на событие MouseLeave для <see cref="Borders"/>.
        /// Это абстрактная функция.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры</param>
        abstract protected void Border_MouseLeave( object sender, MouseEventArgs e );

        /// <summary>
        /// Описывает реакцию на событие MouseMove для <see cref="Borders"/>.
        /// Это абстрактная функция.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры</param>
        abstract protected void Border_MouseMove( object sender, MouseEventArgs e );

        /// <summary>
        /// Описывает реакцию на событие centerUpdate для <see cref="Borders.Center"/>.
        /// </summary>
        /// <param name="d">Источник</param>
        /// <param name="e">Параметры</param>
        protected static void centerUpdate( DependencyObject d, DependencyPropertyChangedEventArgs e ) {
            ( d as Borders ).updateConnections();
        }

        #endregion
    }
}
