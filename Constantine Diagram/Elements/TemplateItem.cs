﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Constantine_Diagram.Elements
{
    /// <summary>
    /// Реализует элемент списка шаблонов.
    /// </summary>
    class TemplateItem
    {
        #region Независимые свойства

        /// <summary>
        /// Имя шаблона
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// Имя шаблона отображаемое в подсказке.
        /// </summary>
        public string ToolTipName {
            get;
            set;
        }
        /// <summary>
        /// Путь к картинке шаблона.
        /// </summary>
        public string ImagePath {
            get;
            set;
        }

        #endregion
    }
}
