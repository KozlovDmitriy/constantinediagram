﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Constantine_Diagram.Elements;
using Constantine_Diagram.UserControls;
using System.IO;
using Microsoft.Win32;
using System.Windows.Markup;
using Constantine_Diagram.Interfaces;
using System.Security.Cryptography;
using Constantine_Diagram.ProjectHelpers;
using Constantine_Diagram.MathModel;
using System.Runtime.InteropServices;

namespace Constantine_Diagram
{
    /// <summary>
    /// Реализует класс главного окна программы.
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Поля

        /// <summary>
        /// Диалоговое окно сохранения файла.
        /// </summary>
        private SaveFileDialog _saveFileDialog;
        /// <summary>
        /// Диалоговое окно открытия файла.
        /// </summary>
        private OpenFileDialog _openFileDialog;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию для <see cref="MainWindow"/>
        /// </summary>
        public MainWindow() {
            InitializeComponent();
            DialogWithUser.DialogWithUseTextBlock = this.status_bar_text_block;
            _saveFileDialog = new SaveFileDialog();
            _openFileDialog = new OpenFileDialog();
            // Загрузка пользовательских настроек
            LoadSettings();
        }

        #endregion

        #region Функции-члены

        /// <summary>
        /// Сохранение пользовательских настроек
        /// Позиция главного окна и его состояния
        /// </summary>
        private void SaveSettings() {
            Properties.Settings.Default.WindowPosition = this.RestoreBounds;
            Properties.Settings.Default.WindowState = this.WindowState;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Загрузка пользовательских настроек
        /// Позиция главного окна и его состояние
        /// </summary>
        private void LoadSettings() {
            try {
                this.WindowState = Properties.Settings.Default.WindowState;
                Rect bounds = Properties.Settings.Default.WindowPosition;
                this.Top = bounds.Top;
                this.Left = bounds.Left;
                // Восстановить размеры, только если они
                // устанавливались для окна вручную
                if ( this.SizeToContent == SizeToContent.Manual ) {
                    this.Width = bounds.Width;
                    this.Height = bounds.Height;
                }
            }
            catch {
                // если настроек не существует - это первый запуск программы, выставим окно по-середине и поприветствуем пользователя
                this.Top = ( SystemParameters.PrimaryScreenHeight - this.Height ) / 2;
                this.Left = ( SystemParameters.PrimaryScreenWidth - this.Width ) / 2;
                this.status_bar_text_block.Text = "Добро пожаловать! Это ваш первый запуск программы. Чтобы понять как в ней работать, посетите [Помощь -> Справка]";
            }
        }

        /// <summary>
        /// Действие по закрытию окна
        /// Сохраняем пользовательские настройки
        /// </summary>
        /// <param name="e">Параметры.</param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e ) {
            MessageBoxResult message_box_result = MessageBox.Show(
            "Вы выходите из программы!\n" +
            "Хотите сохранить ваш текущий проект?",
            "Приложение будет закрыто",
            MessageBoxButton.YesNoCancel,
            MessageBoxImage.Question );

            if ( message_box_result == MessageBoxResult.Cancel ) {
                e.Cancel = true;
                return;
            }
            else if ( message_box_result == MessageBoxResult.Yes ) {
                Save( this, null );
            }

            SaveSettings();
        }

        /// <summary>
        /// Сохраняет проект в файл.
        /// </summary>
        /// <param name="filename">Путь к файлу.</param>
        /// <param name="password">Пароль шифрования данных.</param>
        private void SaveProjectToFile( string filename, string password = null ) {
            try {
                FileStream stream = new FileStream( filename, FileMode.Create );
                StreamWriter string_writer = new StreamWriter( stream );
                string output = "";
                output += "WorkPanel\r\n";
                output += "LastId : " + WorkPanel.LastId + "\r\n";
                output += "\r\n";
                output += Serialize.serializeParameters();
                output = "Hash : " + MD5Hash.GetHashString( output ) + "\r\n\r\n" + output;
                // Шифруем файл, если было выбрано шифрование
                if ( password != null ) {
                    output = "CRPTD$$" + Aes.Encrypt( output, password );
                }
                string_writer.Write( output );
                string_writer.Close();
                stream.Close();
                DialogWithUser.showMessage( "Проект сохранен" );
                if ( password == null ) {
                    Project.filename = filename;
                }
            }
            catch ( System.Exception e ) {
                if ( e.Message != "Пустое имя пути не допускается." ) {
                    MessageBox.Show( e.Message );
                }
            }
        }

        /// <summary>
        /// Сохраняет проект как шаблон.
        /// </summary>
        /// <param name="filename">Имя шаблона.</param>
        private void SaveProjectToTemplateFile( string filename ) {
            try {
                double wp_width = WorkPanel.CurrentWorkPanel.ActualWidth;
                double wp_height = WorkPanel.CurrentWorkPanel.ActualHeight;
                if ( !System.IO.Directory.Exists( "Templates" ) ) {
                    System.IO.Directory.CreateDirectory( "Templates" );
                }
                FileStream stream = new FileStream( "Templates\\" + filename, FileMode.Create );
                Line diag = WorkPanel.CurrentWorkPanel.getMinRectDiag();
                WorkPanel.CurrentWorkPanel.shiftAllElements( diag.X1, diag.Y1 );
                StreamWriter string_writer = new StreamWriter( stream );
                string output = "";
                output += "Constantine Diagramm Temlate\r\n";
                output += "LastId : " + WorkPanel.LastId + "\r\n";
                output += "Width : " + Math.Abs( diag.X1 - diag.X2 ) + "\r\n";
                output += "Height : " + WorkPanel.CurrentWorkPanel.ActualHeight + "\r\n";
                output += "\r\n";
                output += Serialize.serializeParameters();
                output = "Hash : " + MD5Hash.GetHashString( output ) + "\r\n\r\n" + output;
                string_writer.Write( output );
                string_writer.Close();
                stream.Close();
                WorkPanel.CurrentWorkPanel.exportCanvasToImage( 
                        ( stream.Name.Clone() as string ).Replace( ".cdt", ".png" ), diag 
                    );
                WorkPanel.CurrentWorkPanel.shiftAllElements( -diag.X1, -diag.Y1 );
                DialogWithUser.showMessage( "Новый шаблон сохранен" );
                this.templates_list.UpdateList();
                WorkPanel.CurrentWorkPanel.Width = wp_width;
                WorkPanel.CurrentWorkPanel.Height = wp_height;
            }
            catch ( System.Exception e ) {
                if ( e.Message != "Пустое имя пути не допускается." ) {
                    MessageBox.Show( e.Message );
                }
            }
        }

        /// <summary>
        /// Загружает проект из файла.
        /// </summary>
        /// <param name="filename">Путь к файлу.</param>
        private void OpenProjectFromFile( string filename ) {
            try {
                WorkPanel.CurrentWorkPanel.clear();
                FileStream stream = new FileStream( filename, FileMode.Open );
                StreamReader string_reader = new StreamReader( stream );
                string input = string_reader.ReadToEnd();
                string_reader.Close();
                stream.Close();
                // Расшифровываем файл, если в нем стоит метка шифрования
                if ( input.Substring( 0, 7 ) == "CRPTD$$" ) {
                    bool gotCorrect = false;
                    while ( !gotCorrect ) {
                        CryptWindow cryptWindow = new CryptWindow();
                        if ( cryptWindow.ShowDialog() == true ) {
                            try {
                                input = Aes.Decrypt( input.Substring( 7 ), cryptWindow.getPassword() );
                                gotCorrect = true;
                            }
                            catch {
                                MessageBox.Show( "Вы ввели неверный пароль или файл поврежден", "Неверный пароль или повреждение" );
                                continue;
                            }
                        }
                        else {
                            return;
                        }
                    }
                }
                List<string> input_list = input.Split( new string[] { "\r\n" }, StringSplitOptions.None ).ToList();
                string hash = input_list[0];
                input_list.RemoveAt( 0 );
                input_list.RemoveAt( 0 );
                hash = Serialize.getSerializeStringParam( hash, "Hash : " );
                // проверяем целостность файла
                if ( MD5Hash.GetHashString( String.Join( "\r\n", input_list ) ) == hash ) {
                    if ( input_list[0] == "WorkPanel" ) {
                        input_list.RemoveAt( 0 );
                        WorkPanel.setLastId( Serialize.getSerializeUintParam( input_list[0], "LastId : " ) );
                        input_list.RemoveAt( 0 );
                        input_list.RemoveAt( 0 );
                    }
                    Serialize.deserializeParameters( input_list );
                    Project.filename = filename;
                }
                else {
                    MessageBox.Show( "Входной файл был поврежден или содержит ошибку." );
                }
            }
            catch {
                MessageBox.Show( "Входной файл был поврежден или содержит ошибку." );
            }
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на события Click на элементе btn_DeleteElement.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры</param>
        private void btn_DeleteElement_Click( object sender, RoutedEventArgs e ) {
            WorkPanel.CurrentWorkPanel.Focus();
            WorkPanel.CurrentWorkPanel.IsDeleting = true;
        }

        /// <summary>
        /// Описывает реакцию на события PreviewMouseLeftButtonDown на элементе btn_ModuleElement.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры</param>
        private void btn_ModuleElement_PreviewMouseLeftButtonDown( object sender, MouseButtonEventArgs e ) {
            WorkPanel.CurrentWorkPanel.Focus();
            DialogWithUser.showMessage( "Инициализированно добавление элемента \"Модуль\"" );
            DragDrop.DoDragDrop( btn_ModuleElement, "Module", DragDropEffects.Copy );
        }

        /// <summary>
        /// Описывает реакцию на события PreviewMouseLeftButtonDown на элементе btn_DataArea.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры</param>
        private void btn_DataArea_PreviewMouseLeftButtonDown( object sender, MouseButtonEventArgs e ) {
            WorkPanel.CurrentWorkPanel.Focus();
            DialogWithUser.showMessage( "Инициализированно добавление элемента \"Область данных\"" );
            DragDrop.DoDragDrop( btn_DataArea, "DataArea", DragDropEffects.Copy );
        }

        /// <summary>
        /// Описывает реакцию на события PreviewMouseLeftButtonDown на элементе btn_Comment.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры</param>
        private void btn_Comment_PreviewMouseLeftButtonDown( object sender, MouseButtonEventArgs e ) {
            WorkPanel.CurrentWorkPanel.Focus();
            DialogWithUser.showMessage( "Инициализированно добавление элемента \"Комментарий\"" );
            DragDrop.DoDragDrop( btn_Comment, "Comment", DragDropEffects.Copy );
        }

        /// <summary>
        /// Описывает реакцию на события Click на элементе btn_IfCondition.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры</param>
        private void btn_IfCondition_Click( object sender, RoutedEventArgs e ) {
            WorkPanel.CurrentWorkPanel.Focus();
            DialogWithUser.showMessage( "Инициализированно добавление элемента \"Условный элемент\"" );
            WorkPanel.CurrentWorkPanel.addIfCondition();
        }

        /// <summary>
        /// Описывает реакцию на события Click на элементе btn_IfElseCondition.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры</param>
        private void btn_IfElseCondition_Click( object sender, RoutedEventArgs e ) {
            WorkPanel.CurrentWorkPanel.Focus();
            DialogWithUser.showMessage( "Инициализированно добавление элемента \"Условный элемент\"" );
            WorkPanel.CurrentWorkPanel.addIfElseCondition();
        }

        /// <summary>
        /// Описывает реакцию на события Click на элементе btn_ParallelCallSplyne.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры</param>
        private void btn_ParallelCallSplyne_Click( object sender, RoutedEventArgs e ) {
            WorkPanel.CurrentWorkPanel.Focus();
            DialogWithUser.showMessage( "Инициализированно добавление элемента \"Параллельный вызов\"" );
            WorkPanel.CurrentWorkPanel.addParallelCall( true );
        }

        /// <summary>
        /// Описывает реакцию на события Click на элементе btn_ParallelCallSplyne.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры</param>
        private void btn_ConsecutiveCallSplyne_Click( object sender, RoutedEventArgs e ) {
            WorkPanel.CurrentWorkPanel.Focus();
            DialogWithUser.showMessage( "Инициализированно добавление элемента \"Последовательный вызов\"" );
            WorkPanel.CurrentWorkPanel.addConsecutiveCall( true );
        }

        /// <summary>
        /// Выполняет операцию экспорта диаграммы.
        /// Событие Click на пункте меню "Экспорт".
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Export( object sender, RoutedEventArgs e ) {
            _saveFileDialog.Title = "Сохранить как изображение";
            _saveFileDialog.Filter = "PNG Image|*.png|JPEG Image|*.jpg|BMP Image|*.bmp|GIF Image|*.gif|TIFF Image|*.tif";
            if ( _saveFileDialog.ShowDialog() == true ) {
                try {
                    WorkPanel.CurrentWorkPanel.exportCanvasToImage( _saveFileDialog.FileName );
                    DialogWithUser.showMessage( "Изображение диаграммы сохранено" );
                }
                catch ( System.Exception ex ) {
                    MessageBox.Show( ex.Message );
                }
            }

        }

        /// <summary>
        /// Выполняет операцию сохранения диаграммы как шаблона.
        /// Событие Click на пункте меню "Сохранить проект как шаблон".
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void SaveAsTemplate( object sender, RoutedEventArgs e ) {
            if ( WorkPanel.CurrentWorkPanel.Children.Count == 1 ) {
                MessageBox.Show(
                        "Рабочее поле не содержит элементов.",
                        "Не удалось создать шаблон",
                        MessageBoxButton.OK,
                        MessageBoxImage.Warning
                    );
            }
            else {
                SaveDiagrammTemplateWindow saveDiagrammTemplateWindow = new SaveDiagrammTemplateWindow();
                if ( saveDiagrammTemplateWindow.ShowDialog() == true ) {
                    SaveProjectToTemplateFile( Project.save_template_name + ".cdt" );
                }
            }
        }

        /// <summary>
        /// Выполняет операцию очистки рабочей области.
        /// Событие Click на пункте меню "Очистка".
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void ClearWorkPanel( object sender, RoutedEventArgs e ) {
            MessageBoxResult message_box_result = MessageBox.Show(
                    "Вы действительно хотите очистить рабочую область?\nВСЕ НЕСОХРАНЕННЫЕ ДАННЫЕ БУДУТ УТЕРЯНЫ!",
                    "Подтверждение очистки рабочей области",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question
                );
            if ( message_box_result == MessageBoxResult.Yes ) {
                WorkPanel.CurrentWorkPanel.clear();
                WorkPanel.CurrentWorkPanel.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Вызывает диалоговое окно "О программе"
        /// Событие About на пункте меню "О программе...".
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void About( object sender, RoutedEventArgs e ) {
            MessageBox.Show( "Constantine Diagram © 2013 \n" +
            "Редактор для построения структурных карт Константайна. \n \n" +
            "Разработчики: \n\n" +
            "Мосолов Алексей \n" +
            "Дмитрий Козлов \n" +
            "Бондаренко Виталий",
            "О программе",
            MessageBoxButton.OK, MessageBoxImage.Information );
        }


        /// <summary>
        /// Выполняет операцию шифровки диаграммы.
        /// Событие Click на пункте меню "Шифровать".
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Encrypt( object sender, RoutedEventArgs e ) {
            CryptWindow cryptWindow = new CryptWindow();
            if ( cryptWindow.ShowDialog() == true ) {
                _saveFileDialog.Title = "Зашифровать файл";
                _saveFileDialog.Filter = "CDF File|*.cdf";
                if ( _saveFileDialog.ShowDialog() == true ) {
                    SaveProjectToFile( _saveFileDialog.FileName, cryptWindow.getPassword() );
                    DialogWithUser.showMessage( "Файл зашифрован" );
                }
            }
        }

        /// <summary>
        /// Выполняет операцию заливки файла на rghost.ru.
        /// Событие Click на пункте меню "Залить файл на RGHost.ru".
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Upload( object sender, RoutedEventArgs e ) {
            try {
                Save( null, null );
                if ( Project.filename == "" ) {
                    this.status_bar_text_block.Text = "Прежде чем заливать файл, его необходимо сохранить";
                    return;
                }
                this.status_bar_text_block.Text = "Заливаем файл на RGHost.ru...";
                var result = RGHost.Upload( Project.filename );
                Clipboard.SetData( DataFormats.Text, ( Object ) result );
                this.status_bar_text_block.Text = "Ссылка на ваш файл скопирована в буфер обмена";
            }
            catch {
                this.status_bar_text_block.Text = "Не удалось залить файл. Возможно вы отключены от интернета";
            }
        }

        #endregion

        #region Команды

        /// <summary>
        /// Выполняет команду печати диаграммы.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Print( object sender, ExecutedRoutedEventArgs e ) {
            PrintDialog printDialog = new PrintDialog();
            if ( printDialog.ShowDialog() == true ) {
                printDialog.PrintVisual( WorkPanel.CurrentWorkPanel, "Печать проекта Constantine Diagram" );
            }
        }

        /// <summary>
        /// Выполняет команду создания нового файла с диаграммой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void NewProject( object sender, ExecutedRoutedEventArgs e ) {
            MessageBoxResult message_box_result = MessageBox.Show(
                    "Вы действительно хотите перейти к созданию нового проекта?\nВСЕ НЕСОХРАНЕННЫЕ ДАННЫЕ БУДУТ УТЕРЯНЫ!",
                    "Подтверждение создания нового проекта",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question
                );
            if ( message_box_result == MessageBoxResult.Yes ) {
                WorkPanel.CurrentWorkPanel.clear();
                WorkPanel.CurrentWorkPanel.InvalidateMeasure();
                Project.filename = "";
            }
        }

        /// <summary>
        /// Выполняет команду открытия файла с диаграммой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Open( object sender, ExecutedRoutedEventArgs e ) {
            e.Handled = true;
            MessageBoxResult message_box_result = MessageBox.Show(
                    "Вы действительно хотите открыть другой проект?\nВСЕ НЕСОХРАНЕННЫЕ ДАННЫЕ БУДУТ УТЕРЯНЫ!",
                    "Подтверждение открытия проекта",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question
                );
            if ( message_box_result == MessageBoxResult.Yes ) {
                _openFileDialog.Title = "Открыть проект";
                _openFileDialog.Filter = "CDF File|*.cdf";
                if ( _openFileDialog.ShowDialog() == true ) {
                    OpenProjectFromFile( _openFileDialog.FileName );
                }
            }
        }

        /// <summary>
        /// Выполняет команду сохранения файла с диаграммой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void SaveAs( object sender, ExecutedRoutedEventArgs e ) {
            _saveFileDialog.Title = "Сохранить как";
            _saveFileDialog.Filter = "CDF File|*.cdf";
            if ( _saveFileDialog.ShowDialog() == true ) {
                SaveProjectToFile( _saveFileDialog.FileName );
            }
        }

        /// <summary>
        /// Выполняет команду сохранения файла с диаграммой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Save( object sender, ExecutedRoutedEventArgs e ) {
            if ( Project.filename.Length == 0 ) {
                SaveAs( sender, e );
            }
            else {
                SaveProjectToFile( Project.filename );
            }
        }

        /// <summary>
        /// Выполняет команду выхода из программы.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Exit( object sender, ExecutedRoutedEventArgs e ) {
            this.Close();
        }

        /// <summary>
        /// Выполняет команду показа справки пользователю
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Help( object sender, ExecutedRoutedEventArgs e ) {
            HelpWindow helpWindow = new HelpWindow();
        }

        /// <summary>
        /// Выполняет команду вставки из буфера обмена.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Paste( object sender, ExecutedRoutedEventArgs e ) {
            string input;
            try {
                input = Clipboard.GetText();
            }
            catch ( ExternalException ) {
                DialogWithUser.showMessage( "Невозможно вставить элемент из буфера обмена, конфликт работы с буфером обмена" );
                return;
            }
            List<string> params_list = input.Split( new string[] { "\r\n" }, StringSplitOptions.None ).ToList();
            string hash = params_list[0];
            params_list.RemoveAt( 0 );
            hash = Serialize.getSerializeStringParam( hash, "Hash : " );
            // проверяем целостность данных
            if ( MD5Hash.GetHashString( String.Join( "\r\n", params_list ) ) == hash ) {
                Serialize.deserializeParameters( params_list, true );
            }
        }

        /// <summary>
        /// Проверка на доступность команд.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void CommandCanExecutive( object sender, CanExecuteRoutedEventArgs e ) {
            e.CanExecute = true;
        }

        #endregion
    }
}
