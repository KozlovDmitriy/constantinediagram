﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

/// <summary>
/// Реализует класс заливки файла на RGhost.ru.
/// </summary>
static class RGHost
{
    /// <summary>
    /// Залить файл на сервер.
    /// </summary>
    /// <param name="filepath">Путь к файлу.</param>
    static public string Upload( string filepath ) {
        using ( var wc = new WebClient() ) {
            // запрос на ключ сессии
            wc.Proxy = null;
            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            var reply_session = wc.DownloadString( "http://rghost.net/multiple/upload_host" );
            var reply_array = reply_session.Split( '"' );
            if ( reply_array.Count() < 8 ) {
                throw new System.ArgumentException( "Невозможно получить параметры сессии" );
            }
            var upload_host = reply_array[3];
            var authenticity_token = reply_array[7];

            wc.QueryString.Add( "authenticity_token", authenticity_token );
            wc.QueryString.Add( "filename", filepath );
            // запрос на загрузку файла
            var reply_upload = Encoding.Default.GetString( wc.UploadFile( "http://" + upload_host + "/files", "POST", filepath ) );
            // парсим ссылку на файл в теле ответа
            var regex = @"http://rghost.net/download/(\w|\/|\.)+";
            var href = Regex.Match( reply_upload, regex ).Value;
            if ( href == String.Empty ) {
                throw new System.ArgumentException( "Невозможно получить ссылку" );
            }
            return href;
        }
    }
}

