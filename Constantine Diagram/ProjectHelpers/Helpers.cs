﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Constantine_Diagram.Elements;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram
{
    /// <summary>
    /// Реализует функции расширяющий базовый набор функций работы с деревом элементов WPF.
    /// </summary>
    public class Helpers
    {
        #region Статические методы

        /// <summary>
        /// Ищет предка определенного типа для переданного объекта.
        /// </summary>
        /// <typeparam name="T">Тип искомого элемента.</typeparam>
        /// <param name="current">Объект для начала поиска.</param>
        /// <returns>Ссылка на найденого предка.</returns>
        public static T FindAnchestor<T>( DependencyObject current ) where T : DependencyObject {
            do {
                if ( current is T ) {
                    return ( T ) current;
                }
                current = VisualTreeHelper.GetParent( current );
            }
            while ( current != null );
            return null;
        }

        /// <summary>
        /// Ищет первого потомка переданного элемента над которым находиться указатель мыши.
        /// </summary>
        /// <typeparam name="T">Тип искомого элемента.</typeparam>
        /// <param name="current">Объект для начала поиска.</param>
        /// <returns>Ссылка на найденого потомка.</returns>
        public static T FindChildWithMouse<T>( DependencyObject current ) where T : DependencyObject {
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                if ( child is T ) {
                    if ( ( child as UIElement ).IsMouseOver ) {
                        return child as T;
                    }
                }
            }
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                T ret = FindChildWithMouse<T>( child );
                if ( ret != null ) {
                    return ret;
                }
            }
            return null;
        }

        /// <summary>
        /// Ищет первого потомка переданного элемента непосредственно над которым находиться указатель мыши
        /// (Указатель искомого элемента должен находиться имеенно над элементом, а не над его потомками).
        /// </summary>
        /// <typeparam name="T">Тип искомого элемента.</typeparam>
        /// <param name="current">Объект для начала поиска.</param>
        /// <returns>Ссылка на найденого потомка.</returns>
        public static T PreviewFindChildWithMouse<T>( DependencyObject current ) where T : DependencyObject {
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                bool fl = false;
                for ( int k = 0; k < VisualTreeHelper.GetChildrenCount( child ); ++k ) {
                    DependencyObject grandson = VisualTreeHelper.GetChild( child, k );
                    if ( ( grandson is UIElement ) && ( grandson as UIElement ).IsMouseOver ) {
                        fl = true;
                        break;
                    }
                }
                if ( child is T && !fl ) {
                    if ( ( child is UIElement ) && ( child as UIElement ).IsMouseOver ) {
                        return child as T;
                    }
                }
            }
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                T ret = PreviewFindChildWithMouse<T>( child );
                if ( ret != null ) {
                    return ret;
                }
            }
            return null;
        }

        /// <summary>
        /// Ищет первого потомка определенного типа для переданного элемента.
        /// </summary>
        /// <typeparam name="T">Тип искомого элемента.</typeparam>
        /// <param name="current">Объект для начала поиска.</param>
        /// <returns>Ссылка на найденого потомка.</returns>
        public static T FindChild<T>( DependencyObject current ) where T : DependencyObject {
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                if ( child is T ) {
                    return child as T;
                }
            }
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                T ret = FindChild<T>( child );
                if ( ret != null ) {
                    return ret;
                }
            }
            return null;
        }

        /// <summary>
        /// Возвращает список всех потомков определенного типа для переданного элемента.
        /// </summary>
        /// <typeparam name="T">Тип искомых элементов.</typeparam>
        /// <param name="current">Объект для начала поиска.</param>
        /// <param name="list">Список найденных элементов</param>
        public static void FindAll<T>( DependencyObject current, List<T> list ) where T : DependencyObject {
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                if ( child is T ) {
                    list.Add( child as T );
                }
            }
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                FindAll<T>( child, list );
            }
        }

        /// <summary>
        /// Возвращает список всех потомков типа <see cref="IDiagrammElements"/> для переданного элемента.
        /// </summary>
        /// <param name="current">Объект для начала поиска.</param>
        /// <param name="list">Список найденных элементов</param>
        public static void FindAllDiagrammElements( DependencyObject current, List<IDiagrammElement> list ) {
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                if ( child is IDiagrammElement ) {
                    list.Add( child as IDiagrammElement );
                }
            }
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                FindAllDiagrammElements( child, list );
            }
        }

        /// <summary>
        /// Возвращает список всех потомков типа <see cref="IConnectionElement"/> для переданного элемента.
        /// </summary>
        /// <param name="current">Объект для начала поиска.</param>
        /// <param name="list">Список найденных элементов</param>
        public static void FindAllConnectionElements( DependencyObject current, List<IConnectionElement> list ) {
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                if ( child is IConnectionElement ) {
                    list.Add( child as IConnectionElement );
                }
            }
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                FindAllConnectionElements( child, list );
            }
        }

        /// <summary>
        /// Возвращает список всех потомков типа <see cref="ISerializeble"/> для переданного элемента.
        /// </summary>
        /// <param name="current">Объект для начала поиска.</param>
        /// <param name="list">Список найденных элементов</param>
        public static void FindAllSerializedElements( DependencyObject current, List<ISerializeble> list ) {
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                if ( child is ISerializeble ) {
                    list.Add( child as ISerializeble );
                }
            }
            for ( int i = 0; i < VisualTreeHelper.GetChildrenCount( current ); ++i ) {
                DependencyObject child = VisualTreeHelper.GetChild( current, i );
                FindAllSerializedElements( child, list );
            }
        }

        #endregion
    }
}
