﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Constantine_Diagram
{
    /// <summary>
    /// Организует диалог с пользователем через строку состояния приложения.
    /// </summary>
    public class DialogWithUser
    {
        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает TextBlock для вывода текста сообщений.
        /// </summary>
        public static TextBlock DialogWithUseTextBlock {
            set;
            get;
        }

        #endregion

        #region Статические методы

        /// <summary>
        /// Отображает сообщение пользователю.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        public static void showMessage( string message ) {
            message = DateTime.Now.ToString( "hh:mm:ss" ) + " : " + message;
            DialogWithUser.DialogWithUseTextBlock.Text = message;
        }

        #endregion
    }
}
