﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram
{
    /// <summary>
    /// Реализует отображение прямоугольной области на рабочем поле (удобно при отладке приложения).
    /// </summary>
    internal class RectanglePrintDebug
    {
        #region Поля

        /// <summary>
        /// Центр прямоугольной области.
        /// </summary>
        private Point center;
        /// <summary>
        /// Ширина прямоугольной области.
        /// </summary>
        private double width;
        /// <summary>
        /// Высота прямоугольной области.
        /// </summary>
        private double height;
        /// <summary>
        /// Цвет рамки отображаемой прямоугольной области.
        /// </summary>
        private SolidColorBrush solidColorBrush;

        #endregion

        #region Типы данных

        /// <summary>
        /// Реализует прямоугольную область, отображающуюся в <see cref="RectanglePrintDebug"/>.
        /// </summary>
        private class DebugRectangle : Border
        {
            #region Конструкторы

            /// <summary>
            /// Конструктор по умолчанию для <see cref="DebugRectangle"/>.
            /// </summary>
            private DebugRectangle() { }

            /// <summary>
            /// Инициализирующий конструктор для <see cref="DebugRectangle"/>.
            /// </summary>
            /// <param name="center">Центр прямоугольной области.</param>
            /// <param name="width">Ширина прямоугольной области.</param>
            /// <param name="height">Высота прямоугольной области.</param>
            /// <param name="color">Цвет рамки отображаемой прямоугольной области.</param>
            public DebugRectangle( Point center, double width, double height, SolidColorBrush color ) {
                this.BorderThickness = new Thickness( 2 );
                this.BorderBrush = color;
                Canvas.SetLeft( this, center.X - width / 2 );
                Canvas.SetTop( this, center.Y - height / 2 );
                this.Width = Math.Abs( width );
                this.Height = Math.Abs( height );
                WorkPanel.CurrentWorkPanel.Children.Add( this );
            }

            #endregion
        }

        #endregion

        #region Методы

        /// <summary>
        /// Отображает на рабочем поле прямоугольную область с переданными параметрами.
        /// </summary>
        /// <param name="center">Центр прямоугольной области.</param>
        /// <param name="width">Ширина прямоугольной области.</param>
        /// <param name="height">Высота прямоугольной области.</param>
        /// <param name="color">Цвет рамки отображаемой прямоугольной области.</param>
        public RectanglePrintDebug( Point center, double width, double height, SolidColorBrush color ) {
            new DebugRectangle( center, width, height, color );
        }

        #endregion

        #region Статические методы

        /// <summary>
        /// Удаляет с рабочего поля все элементы типа <see cref="DebugRectangle"/>.
        /// </summary>
        public static void deleteAllDebugRectangles() {
            int i = 0;
            while ( i < WorkPanel.CurrentWorkPanel.Children.Count ) {
                if ( WorkPanel.CurrentWorkPanel.Children[i] is DebugRectangle ) {
                    WorkPanel.CurrentWorkPanel.Children.RemoveAt( i );
                }
                else {
                    ++i;
                }
            }
        }

        #endregion
    }
}
