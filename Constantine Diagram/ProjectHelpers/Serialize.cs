﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Constantine_Diagram.Elements;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram.ProjectHelpers
{
    /// <summary>
    /// Реализует методы сериализации проекта.
    /// </summary>
    public class Serialize
    {
        #region Методы

        /// <summary>
        /// Получает из строки переменную типа double.
        /// </summary>
        /// <param name="str">Строка.</param>
        /// <param name="start_str">Префикс строки.</param>
        /// <returns></returns>
        public static double getSerializeDoubleParam( string str, string start_str ) {
            return str.StartsWith( start_str ) ? Convert.ToDouble( str.Remove( 0, start_str.Length ) ) : 0.0;
        }

        /// <summary>
        /// Получает из строки переменную типа string.
        /// </summary>
        /// <param name="str">Строка.</param>
        /// <param name="start_str">Префикс строки.</param>
        /// <returns></returns>
        public static string getSerializeStringParam( string str, string start_str ) {
            return str.StartsWith( start_str ) ? str.Remove( 0, start_str.Length ) : "";
        }

        /// <summary>
        /// Получает из строки переменную типа uint.
        /// </summary>
        /// <param name="str">Строка.</param>
        /// <param name="start_str">Префикс строки.</param>
        /// <returns></returns>
        public static uint getSerializeUintParam( string str, string start_str ) {
            return str.StartsWith( start_str ) ? Convert.ToUInt32( str.Remove( 0, start_str.Length ) ) : 0;
        }

        /// <summary>
        /// Получает из строки переменную типа bool.
        /// </summary>
        /// <param name="str">Строка.</param>
        /// <param name="start_str">Префикс строки.</param>
        /// <returns></returns>
        public static bool getSerializeBoolParam( string str, string start_str ) {
            return str.StartsWith( start_str ) ? Convert.ToBoolean( str.Remove( 0, start_str.Length ) ) : false;
        }

        /// <summary>
        /// Сериализует текущий проект программы.
        /// </summary>
        /// <returns>Строка сериализации.</returns>
        public static string serializeParameters() {
            string output = "";
            List<Conditions> condition_elements = new List<Conditions>();
            Helpers.FindAll<Conditions>( WorkPanel.CurrentWorkPanel, condition_elements );
            foreach ( var condition_element in condition_elements ) {
                output += condition_element.serialize();
            }
            List<IConnectionElement> connection_elements = new List<IConnectionElement>();
            Helpers.FindAllConnectionElements( WorkPanel.CurrentWorkPanel, connection_elements );
            foreach ( var connection_element in connection_elements ) {
                if ( !( connection_element is ConditionArrows ) ) {
                    output += connection_element.serialize();
                }
            }
            List<IDiagrammElement> diagramm_elements = new List<IDiagrammElement>();
            Helpers.FindAllDiagrammElements( WorkPanel.CurrentWorkPanel, diagramm_elements );
            foreach ( var diagramm_element in diagramm_elements ) {
                if ( Helpers.FindAnchestor<ContentContainer>( diagramm_element as DependencyObject ) != null ) {
                    output += diagramm_element.serialize();
                }
            }
            foreach ( var diagramm_element in diagramm_elements ) {
                if ( diagramm_element is Module ) {
                    output += ( diagramm_element as Module ).serializeConnections();
                }
                if ( diagramm_element is DataArea ) {
                    output += ( diagramm_element as DataArea ).serializeConnections();
                }
            }
            return output;
        }

        /// <summary>
        /// Десириализует данные в объекты и добавляет их в текущий проект.
        /// </summary>
        /// <param name="input_list">Массив сериализованных строк.</param>
        /// <param name="clipboard_operation">true, если сериализованные данные 
        /// были получены путем вставки их из буфера обмена.</param>
        public static void deserializeParameters( List<string> input_list, bool clipboard_operation = false ) {
            while ( input_list.Count > 1 ) {
                string input = input_list[0];
                input_list.RemoveAt( 0 );
                List<string> list = new List<string>() { input };
                string line = input_list[0];
                input_list.RemoveAt( 0 );
                while ( line != "" ) {
                    list.Add( line );
                    line = input_list[0];
                    input_list.RemoveAt( 0 );
                }
                Line diag = WorkPanel.CurrentWorkPanel.getMinRectDiag();
                ContentContainer container = null;
                switch ( input ) {
                    case "Module":
                        container = Module.deserialize( list, clipboard_operation ).getModule();
                        WorkPanel.CurrentWorkPanel.Children.Insert( 0, container );
                        if ( clipboard_operation ) {
                            WorkPanel.CurrentWorkPanel.UpdateLayout();
                            container.Center = new Point(
                                    diag.X1 - container.Width / 2 - 10,
                                    container.Height / 2
                                );
                            WorkPanel.CurrentWorkPanel.InvalidateMeasure();
                        }
                        break;
                    case "DataArea":
                        container = DataArea.deserialize( list, clipboard_operation ).getDataArea();
                        WorkPanel.CurrentWorkPanel.Children.Insert( 0, container );
                        if ( clipboard_operation ) {
                            WorkPanel.CurrentWorkPanel.UpdateLayout();
                            container.Center = new Point(
                                    diag.X1 - container.Width / 2 - 10,
                                    container.Height / 2
                                );
                            WorkPanel.CurrentWorkPanel.InvalidateMeasure();
                        }
                        break;
                    case "Comment":
                        container = Comment.deserialize( list ).getComment();
                        WorkPanel.CurrentWorkPanel.Children.Insert( 0, container );
                        if ( clipboard_operation ) {
                            WorkPanel.CurrentWorkPanel.UpdateLayout();
                            container.Center = new Point(
                                    diag.X1 - container.Width / 2 - 10,
                                    container.Height / 2
                                );
                            WorkPanel.CurrentWorkPanel.InvalidateMeasure();
                        }
                        break;
                    case "Conditions":
                        Conditions condition = Conditions.deserialize( list, clipboard_operation );
                        WorkPanel.CurrentWorkPanel.Children.Insert( 0, condition );
                        if ( clipboard_operation ) {
                            WorkPanel.CurrentWorkPanel.UpdateLayout();
                            Canvas.SetLeft( condition, diag.X1 - condition.Width - 10 );
                            Canvas.SetTop( condition, 0 );
                            condition.update();
                            WorkPanel.CurrentWorkPanel.InvalidateMeasure();
                        }
                        break;
                    case "Arrows":
                        Arrows arrow = Arrows.deserialize( list );
                        WorkPanel.CurrentWorkPanel.Children.Add( arrow );
                        if ( clipboard_operation ) {
                            WorkPanel.CurrentWorkPanel.UpdateLayout();
                            if ( diag.X1 < 60 ) {
                                WorkPanel.CurrentWorkPanel.shiftAllElements( -70 + diag.X1, 0 );
                                diag = WorkPanel.CurrentWorkPanel.getMinRectDiag();
                                arrow.StartPoint = new Point( diag.X1 - 60, 30 );
                                arrow.EndPoint = new Point( diag.X1 - 10, 90 );
                            }
                            else {
                                arrow.StartPoint = new Point( diag.X1 - 60, 30 );
                                arrow.EndPoint = new Point( diag.X1 - 10, 90 );
                            }
                            WorkPanel.CurrentWorkPanel.InvalidateMeasure();
                        }
                        break;
                    case "ConnectCondition":
                        Module.deserializeConnectCondition( list );
                        break;
                    case "ModuleOutBorderConnnectedArrows":
                        Module.deserializeOutBorderConnnectedArrows( list, clipboard_operation );
                        break;
                    case "DataAreaOutBorderConnnectedArrows":
                        DataArea.deserializeOutBorderConnnectedArrows( list );
                        break;
                    case "InBorderConnnectedArrows":
                        Module.deserializeInBorderConnnectedArrows( list );
                        break;
                }
            }
        }

        #endregion
    }
}
