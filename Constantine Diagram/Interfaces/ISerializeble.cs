﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Constantine_Diagram.Interfaces
{
    public interface ISerializeble : IInterface
    {
        uint Id { get; set; }
        string serialize();
    }
}
