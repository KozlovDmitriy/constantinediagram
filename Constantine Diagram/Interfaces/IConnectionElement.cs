﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Constantine_Diagram.Interfaces
{
    public interface IConnectionElement : IInterface
    {
        List<Rect> getGeometries();
        void Update();
        string serialize();
    }
}
