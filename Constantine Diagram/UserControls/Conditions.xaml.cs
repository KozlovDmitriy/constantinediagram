﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Constantine_Diagram.Elements;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.ProjectHelpers;
using Constantine_Diagram.MathModel;
using System.Runtime.InteropServices;

namespace Constantine_Diagram.UserControls
{
    /// <summary>
    /// Реализует условные элементы диаграммы.
    /// </summary>
    public partial class Conditions : UserControl, ISerializeble
    {
        #region Константы

        /// <summary>
        /// | X2 - X1 | для стрелок принадлежащих <see cref="Conditions"/>.
        /// </summary>
        private const double ARROW_WIDTH = 70;
        /// <summary>
        /// | Y2 - Y1 | для стрелок принадлежащих <see cref="Conditions"/>.
        /// </summary>
        private const double ARROW_HEIGHT = 70;
        /// <summary>
        /// Ширина объектов типа <see cref="Module"/> принадлежащих <see cref="Conditions"/>.
        /// </summary>
        private const double MODULE_WIDTH = 94;
        /// <summary>
        /// Высота объектов типа <see cref="Module"/> принадлежащих <see cref="Conditions"/>.
        /// </summary>
        private const double MODULE_HEIGHT = 64;
        /// <summary>
        /// Ширина объектов типа <see cref="ConditionRomb"/> принадлежащих <see cref="Conditions"/>.
        /// </summary>
        private const double ROMB_WIDTH = 20;
        /// <summary>
        /// Высота объектов типа <see cref="ConditionRomb"/> принадлежащих <see cref="Conditions"/>.
        /// </summary>
        private const double ROMB_HEIGHT = 20;

        #endregion

        #region Поля

        /// <summary>
        /// Флаг "активности" для <see cref="Conditions"/>.
        /// </summary>
        private bool active = false;

        /// <summary>
        /// Флаг, говорящий о том, существует ли в данном объекте <see cref="Conditions"/> ветка "Иначе".
        /// </summary>
        private bool if_else;

        /// <summary>
        /// Точка соединения условного элемента с объектом соединения.
        /// </summary>
        private Point connect_point;

        /// <summary>
        /// <see cref="ConditionRomb"/> принадлежащий текущему <see cref="Conditions"/>.
        /// </summary>
        private ConditionRomb romb;

        /// <summary>
        /// <see cref="ConditionArrows"/> для ветки "Если".
        /// </summary>
        private ConditionArrows if_arrow;

        /// <summary>
        /// <see cref="ConditionArrows"/> для ветки "Иначе".
        /// </summary>
        private ConditionArrows else_arrow;

        /// <summary>
        /// <see cref="Module"/> для ветки "Если".
        /// </summary>
        private Module if_module;

        /// <summary>
        /// <see cref="Module"/> для ветки "Иначе".
        /// </summary>
        private Module else_module;

        /// <summary>
        /// <see cref="ConnectOrientation"/> для текущего объекта <see cref="Conditions"/>. 
        /// </summary>
        private ConnectOrientation condition_connect_orientation;

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает флаг "активности" для <see cref="ContentContainer"/>.
        /// </summary>
        public bool Active {
            get { return active; }
            set {
                if ( value ) {
                    this.active = true;
                    this.condition_border.BorderThickness = new Thickness( 1 );
                }
                else {
                    this.active = false;
                    this.condition_border.BorderThickness = new Thickness( 0 );
                }
            }
        }

        /// <summary>
        /// Возвращает или задает id для <see cref="Conditions"/>
        /// </summary>
        public uint Id {
            get;
            set;
        }

        /// <summary>
        /// Возвращает <see cref="ConditionArrows"/> для ветки "Если".
        /// </summary>
        public ConditionArrows IfArrow {
            get { return this.if_arrow; }
        }

        /// <summary>
        /// Возвращает или задает комментарий для стрелки "Если".
        /// </summary>
        public string IfArrowText {
            get { return this.IfArrow.Text; }
            set { this.IfArrow.Text = value; }
        }

        /// <summary>
        /// Возвращает модуль "Если".
        /// </summary>
        public Module IfModule {
            get { return this.if_module; }
        }

        /// <summary>
        /// Возвращает или задает комментарий для модуля "Если".
        /// </summary>
        public string IfModuleText {
            get { return this.if_module.Text; }
            set { this.if_module.Text = value; }
        }

        /// <summary>
        /// Возвращает <see cref="ConditionArrows"/> для ветки "Иначе".
        /// </summary>
        public ConditionArrows ElseArrow {
            get { return this.else_arrow; }
        }

        /// <summary>
        /// Возвращает или задает комментарий для стрелки "Иначе".
        /// </summary>
        public string ElseArrowText {
            get { return this.ElseArrow.Text; }
            set { this.ElseArrow.Text = value; }
        }

        /// <summary>
        /// Возвращает модуль "Иначе".
        /// </summary>
        public Module ElseModule {
            get { return this.else_module; }
        }

        /// <summary>
        /// Возвращает или задает комментарий для модуля "Иначе".
        /// </summary>
        public string ElseModuleText {
            get { return this.else_module.Text; }
            set { this.else_module.Text = value; }
        }

        /// <summary>
        /// Возвращает или задает <see cref="ConnectOrientation"/> для текущего объекта <see cref="Conditions"/>.
        /// </summary>
        public ConnectOrientation ConditionConnectOrientation {
            get { return this.condition_connect_orientation; }
            set {
                this.condition_connect_orientation = value;
                this.romb.RombConnectOrientation = value;
                this.setConditionsSize();
                this.setConditionsPosition();
                this.romb.Center = getRombCenter();
                this.setIfModuleOrientation();
                this.connectArrowToModule( this.if_arrow, this.if_module );
                if ( this.else_module != null ) {
                    this.setElseModuleOrientation();
                    this.connectArrowToModule( this.else_arrow, this.else_module );
                }
            }
        }

        /// <summary>
        /// Возвращает или задает точку соединения условного элемента с объектом соединения.
        /// </summary>
        public Point ConnectPoint {
            get { return this.connect_point; }
            set {
                this.connect_point = value;
                this.setConditionsPosition();
                if ( this.romb != null ) {
                    this.romb.Center = this.getRombCenter();
                }
                if ( this.if_module != null ) {
                    this.if_module.Center = this.getModuleCenter( this.if_module );
                    this.if_module.updateConditionConnections();
                }
                if ( this.else_module != null ) {
                    this.else_module.Center = this.getModuleCenter( this.else_module );
                    this.else_module.updateConditionConnections();
                }
                WorkPanel.CurrentWorkPanel.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Возвращает или задает флаг, говорящий о том, существует ли в данном объекте 
        /// <see cref="Conditions"/> ветка "Иначе".
        /// </summary>
        public bool IfElse {
            get { return this.if_else; }
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        private Conditions() {
            InitializeComponent();
        }

        /// <summary>
        /// Инициализирующий конструктор.
        /// </summary>
        /// <param name="orientation">Задает <see cref="Conditions.condition_connect_orientation"/>.</param>
        /// <param name="connect_point">Задает <see cref="Conditions.connect_point"/>.</param>
        /// <param name="if_else">Задает <see cref="Conditions.if_else"/>.</param>
        private Conditions( ConnectOrientation orientation, Point connect_point, bool if_else ) {
            InitializeComponent();
            this.condition_connect_orientation = orientation;
            this.if_else = if_else;
            this.setConditionsSize();
            this.ConnectPoint = connect_point;
            this.romb = new ConditionRomb( orientation, this );
            this.condition_grid.Children.Add( romb );
            this.romb.Measure( new Size( double.PositiveInfinity, double.PositiveInfinity ) );
            this.romb.Arrange( new Rect( this.romb.DesiredSize ) );
            this.romb.Center = getRombCenter();
            this.addIfModuleInCondition();
            this.addElseModuleInCondition();
            this.addIfArrowInCondition();
            this.addElseArrowInCondition();
            this.Id = WorkPanel.getNextId();
        }

        #endregion

        #region Функции-члены

        /// <summary>
        /// Определяет положение текущего <see cref="Conditions"/> на рабочем поле.
        /// </summary>
        private void setConditionsPosition() {
            switch ( this.ConditionConnectOrientation ) {
                case ConnectOrientation.LEFT:
                    Canvas.SetLeft( this, this.connect_point.X - this.Width );
                    Canvas.SetTop( this, this.connect_point.Y - this.Height / 2 );
                    break;
                case ConnectOrientation.RIGHT:
                    Canvas.SetLeft( this, this.connect_point.X );
                    Canvas.SetTop( this, this.connect_point.Y - this.Height / 2 );
                    break;
                case ConnectOrientation.TOP:
                    Canvas.SetLeft( this, this.connect_point.X - this.Width / 2 );
                    Canvas.SetTop( this, this.connect_point.Y - this.Height );
                    break;
                case ConnectOrientation.BOTTOM:
                    Canvas.SetLeft( this, this.connect_point.X - this.Width / 2 );
                    Canvas.SetTop( this, this.connect_point.Y );
                    break;
            }
        }

        /// <summary>
        /// Определяет размеры текущего <see cref="Conditions"/>.
        /// </summary>
        private void setConditionsSize() {
            if ( this.condition_connect_orientation == ConnectOrientation.BOTTOM
                 || this.condition_connect_orientation == ConnectOrientation.TOP ) {
                if ( this.if_else ) {
                    this.Width = 2 * Conditions.ARROW_WIDTH + Conditions.MODULE_WIDTH;
                }
                else {
                    this.Width = Conditions.MODULE_WIDTH;
                }
                this.Height = Conditions.ROMB_HEIGHT + Conditions.ARROW_HEIGHT + Conditions.MODULE_HEIGHT - 3.5;
            }
            else {
                this.Width = Conditions.ROMB_WIDTH + Conditions.ARROW_WIDTH + Conditions.MODULE_WIDTH - 3.5;
                if ( this.if_else ) {
                    this.Height = 2 * Conditions.ARROW_HEIGHT + Conditions.MODULE_HEIGHT;
                }
                else {
                    this.Height = Conditions.MODULE_HEIGHT;
                }
            }
        }

        /// <summary>
        /// Добавляет <see cref="Conditions.if_module"/> к текущему объекту <see cref="Conditions"/>.
        /// </summary>
        private void addIfModuleInCondition() {
            this.if_module = new Module();
            this.if_module.Width = Conditions.MODULE_WIDTH;
            this.if_module.Height = Conditions.MODULE_HEIGHT;
            this.setIfModuleOrientation();
            this.if_module.Text = "IF Модуль";
            this.condition_grid.Children.Add( if_module );
            this.if_module.Measure( new Size( double.PositiveInfinity, double.PositiveInfinity ) );
            this.if_module.Arrange( new Rect( this.if_module.DesiredSize ) );
            this.if_module.Center = getModuleCenter( this.if_module );
        }

        /// <summary>
        /// Добавляет <see cref="Conditions.else_module"/> к текущему объекту <see cref="Conditions"/>.
        /// </summary>
        private void addElseModuleInCondition() {
            if ( if_else ) {
                this.else_module = new Module();
                this.else_module.Width = Conditions.MODULE_WIDTH;
                this.else_module.Height = Conditions.MODULE_HEIGHT;
                this.setElseModuleOrientation();
                this.else_module.Text = "ELSE Модуль";
                this.condition_grid.Children.Add( else_module );
                this.else_module.Measure( new Size( double.PositiveInfinity, double.PositiveInfinity ) );
                this.else_module.Arrange( new Rect( this.else_module.DesiredSize ) );
                this.else_module.Center = getModuleCenter( this.else_module );
            }
        }

        /// <summary>
        /// Определяет положение присоединения начальных точек стрелок к <see cref="ConditionRomb"/> 
        /// текущего объекта <see cref="Conditions"/>.
        /// </summary>
        /// <returns>Расчитанный <see cref="ConnectOrientation"/>.</returns>
        private ConnectOrientation getArrowConnectOrientation() {
            ConnectOrientation ret = new ConnectOrientation();
            switch ( this.ConditionConnectOrientation ) {
                case ConnectOrientation.LEFT:
                    ret = ConnectOrientation.RIGHT;
                    break;
                case ConnectOrientation.RIGHT:
                    ret = ConnectOrientation.LEFT;
                    break;
                case ConnectOrientation.TOP:
                    ret = ConnectOrientation.BOTTOM;
                    break;
                case ConnectOrientation.BOTTOM:
                    ret = ConnectOrientation.TOP;
                    break;
            }
            return ret;
        }

        /// <summary>
        /// Получает координаты центра <see cref="ConditionRomb"/> принадлежащего данному <see cref="Conditions"/>.
        /// </summary>
        /// <returns>Точка центра.</returns>
        private Point getRombCenter() {
            double x = Canvas.GetLeft( this );
            double y = Canvas.GetTop( this );
            switch ( this.romb.HorizontalAlignment ) {
                case System.Windows.HorizontalAlignment.Left:
                    x += this.romb.Width / 2;
                    break;
                case System.Windows.HorizontalAlignment.Right:
                    x += this.Width - this.romb.Width / 2;
                    break;
                case System.Windows.HorizontalAlignment.Center:
                    x += this.Width / 2;
                    break;
            }
            switch ( this.romb.VerticalAlignment ) {
                case System.Windows.VerticalAlignment.Top:
                    y += this.romb.Height / 2;
                    break;
                case System.Windows.VerticalAlignment.Bottom:
                    y += this.Height - this.romb.Height / 2;
                    break;
                case System.Windows.VerticalAlignment.Center:
                    y += this.Height / 2;
                    break;
            }
            return new Point( x, y );
        }

        /// <summary>
        /// Присоединяет стрелку к модулю.
        /// </summary>
        /// <param name="arrow">Стрелка.</param>
        /// <param name="module">Модуль.</param>
        private void connectArrowToModule( Arrows arrow, Module module ) {
            Elements.ModuleBorder.ConnectedArrow connected_arrow = new Elements.ModuleBorder.ConnectedArrow();
            connected_arrow.arrow = arrow;
            connected_arrow.arrow_point_location = getArrowConnectOrientation();
            connected_arrow.in_or_out_arrow = InOrOutArrow.IN;
            connected_arrow.indent_coefficient = 0.5;
            WorkPanel.CurrentWorkPanel.DeleteAllEndArrowConnections( arrow );
            module.OutBorder.disconnectArrow( arrow, InOrOutArrow.IN );
            module.OutBorder.connectArrow( connected_arrow );
        }

        /// <summary>
        /// Добавляет <see cref="Conditions.if_arrow"/> к текущему объекту <see cref="Conditions"/>.
        /// </summary>
        private void addIfArrowInCondition() {
            this.if_arrow = new ConditionArrows( new Point( 0, 0 ), new Point( 0, 0 ), this );
            this.if_arrow.IsEdit = false;
            this.romb.IfArrow = if_arrow;
            this.connectArrowToModule( this.if_arrow, this.if_module );
            WorkPanel.CurrentWorkPanel.Children.Add( if_arrow );
        }

        /// <summary>
        /// Добавляет <see cref="Conditions.else_arrow"/> к текущему объекту <see cref="Conditions"/>.
        /// </summary>
        private void addElseArrowInCondition() {
            if ( if_else ) {
                this.else_arrow = new ConditionArrows( new Point( 0, 0 ), new Point( 0, 0 ), this );
                this.else_arrow.IsEdit = false;
                this.romb.ElseArrow = else_arrow;
                this.connectArrowToModule( this.else_arrow, this.else_module );
                WorkPanel.CurrentWorkPanel.Children.Add( else_arrow );
            }
        }

        /// <summary>
        /// Определяет положение <see cref="Conditions.if_module"/> внутри текущего
        /// объекта <see cref="Conditions"/>.
        /// </summary>
        private void setIfModuleOrientation() {
            if ( this.if_else ) {
                switch ( this.ConditionConnectOrientation ) {
                    case ConnectOrientation.LEFT:
                        this.if_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                        this.if_module.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                        break;
                    case ConnectOrientation.RIGHT:
                        this.if_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                        this.if_module.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                        break;
                    case ConnectOrientation.TOP:
                        this.if_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                        this.if_module.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                        break;
                    case ConnectOrientation.BOTTOM:
                        this.if_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                        this.if_module.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                        break;
                }
            }
            else {
                switch ( this.ConditionConnectOrientation ) {
                    case ConnectOrientation.LEFT:
                        this.if_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                        this.if_module.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                        break;
                    case ConnectOrientation.RIGHT:
                        this.if_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                        this.if_module.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                        break;
                    case ConnectOrientation.TOP:
                        this.if_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                        this.if_module.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                        break;
                    case ConnectOrientation.BOTTOM:
                        this.if_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                        this.if_module.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                        break;
                }
            }
        }

        /// <summary>
        /// Определяет положение <see cref="Conditions.else_module"/> внутри текущего 
        /// объекта <see cref="Conditions"/>.
        /// </summary>
        private void setElseModuleOrientation() {
            switch ( this.ConditionConnectOrientation ) {
                case ConnectOrientation.LEFT:
                    this.else_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                    this.else_module.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                    break;
                case ConnectOrientation.RIGHT:
                    this.else_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                    this.else_module.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                    break;
                case ConnectOrientation.TOP:
                    this.else_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                    this.else_module.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                    break;
                case ConnectOrientation.BOTTOM:
                    this.else_module.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                    this.else_module.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                    break;
            }
        }

        /// <summary>
        /// Получает координаты центра одного из модулей принадлежащих данному <see cref="Conditions"/>.
        /// </summary>
        /// <param name="module">Модуль.</param>
        /// <returns>Точка центра.</returns>
        private Point getModuleCenter( Module module ) {
            double x = Canvas.GetLeft( this );
            double y = Canvas.GetTop( this );
            switch ( module.HorizontalAlignment ) {
                case System.Windows.HorizontalAlignment.Left:
                    x += module.Width / 2;
                    break;
                case System.Windows.HorizontalAlignment.Right:
                    x += this.Width - module.Width / 2;
                    break;
                case System.Windows.HorizontalAlignment.Center:
                    x += this.Width / 2;
                    break;
            }
            switch ( module.VerticalAlignment ) {
                case System.Windows.VerticalAlignment.Top:
                    y += module.Height / 2;
                    break;
                case System.Windows.VerticalAlignment.Bottom:
                    y += this.Height - module.Height / 2;
                    break;
                case System.Windows.VerticalAlignment.Center:
                    y += this.Height / 2;
                    break;
            }
            return new Point( x, y );
        }

        /// <summary>
        /// Удаляет условный элемент.
        /// </summary>
        private void DeleteCondition() {
            WorkPanel.CurrentWorkPanel.deleteElement( this );
            WorkPanel.CurrentWorkPanel.DeleteConditionsConnection( this );
            WorkPanel.CurrentWorkPanel.deleteElement( this.if_arrow );
            if ( this.if_else ) {
                WorkPanel.CurrentWorkPanel.deleteElement( this.else_arrow );
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Сериализует объект типа <see cref="Conditions"/>.
        /// </summary>
        /// <returns>Строка с сериализованными данными.</returns>
        public string serialize() {
            string ret = "Conditions\r\n";
            ret += "Id : " + Id + "\r\n";
            ret += "ConditionConnectOrientation : " + ( uint ) ConditionConnectOrientation + "\r\n";
            ret += "ConnectPoint.X : " + ConnectPoint.X + "\r\n";
            ret += "ConnectPoint.Y : " + ConnectPoint.Y + "\r\n";
            ret += "IfElse : " + IfElse + "\r\n";
            ret += "IfArrowId : " + IfArrow.Id + "\r\n";
            ret += "IfArrowText : " + IfArrowText + "\r\n";
            ret += "IfModuleId : " + IfModule.Id + "\r\n";
            ret += "IfModuleText : " + IfModuleText + "\r\n";
            if ( IfElse ) {
                ret += "ElseArrowId : " + ElseArrow.Id + "\r\n";
                ret += "ElseArrowText : " + ElseArrowText + "\r\n";
                ret += "ElseModuleId : " + ElseModule.Id + "\r\n";
                ret += "ElseModuleText : " + ElseModuleText + "\r\n";
            }
            ret += "\r\n";
            return ret;
        }

        /// <summary>
        /// Обновляет расположение всех элементов принадлежащих текущему <see cref="Conditions"/>.
        /// </summary>
        public void update() {
            double x = Canvas.GetLeft( this );
            double y = Canvas.GetTop( this );
            switch ( this.romb.HorizontalAlignment ) {
                case System.Windows.HorizontalAlignment.Left:
                    x += 0;
                    break;
                case System.Windows.HorizontalAlignment.Right:
                    x += this.Width;
                    break;
                case System.Windows.HorizontalAlignment.Center:
                    x += this.Width / 2;
                    break;
            }
            switch ( this.romb.VerticalAlignment ) {
                case System.Windows.VerticalAlignment.Top:
                    y += 0;
                    break;
                case System.Windows.VerticalAlignment.Bottom:
                    y += this.Height;
                    break;
                case System.Windows.VerticalAlignment.Center:
                    y += this.Height / 2;
                    break;
            }
            this.ConnectPoint = new Point( x, y );
        }

        /// <summary>
        /// Копирует <see cref="Conditions"/> в буфер обмена.
        /// </summary>
        public void CopyConditionToClipboard() {
            string input = this.serialize();
            input = "Hash : " + MD5Hash.GetHashString( input ) + "\r\n" + input;
            try {
                Clipboard.SetText( input );
            }
            catch ( ExternalException ) {
                DialogWithUser.showMessage( "Невозможно копировать элемент в буфер обмена, конфликт работы с буфером обмена" );
            }
        }

        #endregion

        #region Статические поля

        public static List<uint> ArrowId = new List<uint>();
        public static List<uint> ModuleId = new List<uint>();

        #endregion

        #region Статические методы

        /// <summary>
        /// Десириализует объект типа <see cref="Conditions"/>.
        /// </summary>
        /// <param name="serialize_string">Сериализованные данные.</param>
        /// <returns>Десириализованный объект типа <see cref="Arrows"/>.</returns>
        public static Conditions deserialize( List<string> serialize_string, bool clipboard_operation = false ) {
            ArrowId.Clear();
            ModuleId.Clear();
            string str = serialize_string[0];
            serialize_string.RemoveAt( 0 );
            Conditions conditions = null;
            if ( str == "Conditions" ) {
                uint id = Serialize.getSerializeUintParam( serialize_string[0], "Id : " );
                if ( clipboard_operation ) {
                    id = WorkPanel.getNextId();
                }
                serialize_string.RemoveAt( 0 );
                ConnectOrientation orientation = ( ConnectOrientation ) Serialize.getSerializeUintParam(
                        serialize_string[0],
                        "ConditionConnectOrientation : "
                    );
                serialize_string.RemoveAt( 0 );
                Point connect_point = new Point();
                connect_point.X = Serialize.getSerializeDoubleParam( serialize_string[0], "ConnectPoint.X : " );
                serialize_string.RemoveAt( 0 );
                connect_point.Y = Serialize.getSerializeDoubleParam( serialize_string[0], "ConnectPoint.Y : " );
                serialize_string.RemoveAt( 0 );
                bool if_else = Serialize.getSerializeBoolParam( serialize_string[0], "IfElse : " );
                serialize_string.RemoveAt( 0 );
                conditions = new Conditions( orientation, connect_point, if_else );
                conditions.Id = id;
                conditions.IfArrow.Id = Serialize.getSerializeUintParam( serialize_string[0], "IfArrowId : " );
                if ( clipboard_operation ) {
                    ArrowId.Add( WorkPanel.getNextId() );
                    conditions.IfArrow.Id = ArrowId.Last();
                }
                serialize_string.RemoveAt( 0 );
                conditions.IfArrowText = Serialize.getSerializeStringParam( serialize_string[0], "IfArrowText : " );
                serialize_string.RemoveAt( 0 );
                conditions.IfModule.Id = Serialize.getSerializeUintParam( serialize_string[0], "IfModuleId : " );
                if ( clipboard_operation ) {
                    ModuleId.Add( WorkPanel.getNextId() );
                    conditions.IfModule.Id = ModuleId.Last();
                }
                serialize_string.RemoveAt( 0 );
                conditions.IfModuleText = Serialize.getSerializeStringParam( serialize_string[0], "IfModuleText : " );
                serialize_string.RemoveAt( 0 );
                if ( conditions.IfElse ) {
                    conditions.ElseArrow.Id = Serialize.getSerializeUintParam( serialize_string[0], "ElseArrowId : " );
                    if ( clipboard_operation ) {
                        ArrowId.Add( WorkPanel.getNextId() );
                        conditions.ElseArrow.Id = ArrowId.Last();
                    }
                    serialize_string.RemoveAt( 0 );
                    conditions.ElseArrowText = Serialize.getSerializeStringParam( serialize_string[0], "ElseArrowText : " );
                    serialize_string.RemoveAt( 0 );
                    conditions.ElseModule.Id = Serialize.getSerializeUintParam( serialize_string[0], "ElseModuleId : " );
                    if ( clipboard_operation ) {
                        ModuleId.Add( WorkPanel.getNextId() );
                        conditions.ElseModule.Id = ModuleId.Last();
                    }
                    serialize_string.RemoveAt( 0 );
                    conditions.ElseModuleText = Serialize.getSerializeStringParam( serialize_string[0], "ElseModuleText : " );
                    serialize_string.RemoveAt( 0 );
                }
            }
            return conditions;
        }

        /// <summary>
        /// Возвращает условный элемент у которого id соответствует переданному. 
        /// </summary>
        /// <param name="id">Id условного элемента.</param>
        /// <returns>Найденный условный элемент.</returns>
        internal static Conditions findConditionById( uint id ) {
            List<Conditions> list = new List<Conditions>();
            Helpers.FindAll<Conditions>( WorkPanel.CurrentWorkPanel, list );
            return list.Find( it => it.Id == id );
        }

        /// <summary>
        /// Создает <see cref="Conditions"/> типа "Если".
        /// </summary>
        /// <param name="orientation">Ориентация элемента.</param>
        /// <param name="connect_point">Точка привязка.</param>
        /// <returns>Созданный объект <see cref="Conditions"/>.</returns>
        public static Conditions IfCondition( ConnectOrientation orientation, Point connect_point ) {
            return new Conditions( orientation, connect_point, false );
        }

        /// <summary>
        /// Создает <see cref="Conditions"/> типа "Если-Иначе".
        /// </summary>
        /// <param name="orientation">Ориентация элемента.</param>
        /// <param name="connect_point">Точка привязка.</param>
        /// <returns>Созданный объект <see cref="Conditions"/>.</returns>
        public static Conditions IfElseCondition( ConnectOrientation orientation, Point connect_point ) {
            return new Conditions( orientation, connect_point, true );
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда удаления.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Delete( object sender, ExecutedRoutedEventArgs e ) {
            this.DeleteCondition();
        }

        /// <summary>
        /// Команда копирования.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Copy( object sender, ExecutedRoutedEventArgs e ) {
            CopyConditionToClipboard();
        }

        /// <summary>
        /// Команда вырезания.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Cut( object sender, ExecutedRoutedEventArgs e ) {
            CopyConditionToClipboard();
            this.DeleteCondition();
        }

        /// <summary>
        /// Проверка на доступность команд.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void CommandCanExecutive( object sender, CanExecuteRoutedEventArgs e ) {
            e.CanExecute = this.Active;
        }

        #endregion
    }
}
