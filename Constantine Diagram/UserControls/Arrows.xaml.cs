﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Constantine_Diagram.Elements;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.MathModel;
using Constantine_Diagram.ProjectHelpers;
using System.Runtime.InteropServices;

namespace Constantine_Diagram.UserControls
{
    /// <summary>
    /// Реализует стрелки типов "Последовательный вызов" и "Параллельный вызов"
    /// </summary>
    public partial class Arrows : UserControl, IConnectionElement, ISerializeble
    {
        #region Зависимые свойства

        /// <summary>
        /// Зависимое свойство для задания начальной точки для <see cref="Arrows"/>.
        /// </summary>
        public static readonly DependencyProperty StartPointProperty =
            DependencyProperty.Register( "StartPoint", typeof( Point ), typeof( Arrows ),
                new FrameworkPropertyMetadata( new Point( 0, 0 ), FrameworkPropertyMetadataOptions.AffectsMeasure, dpupdate ) );
        /// <summary>
        /// Возвращает или задает начальную точку для <see cref="Arrows"/>. 
        /// Это свойство зависимости.
        /// </summary>
        public Point StartPoint {
            get { return ( Point ) GetValue( StartPointProperty ); }
            set {
                SetValue( StartPointProperty, value );
                Update();
            }
        }

        /// <summary>
        /// Зависимое свойство для задания конечной точки для <see cref="Arrows"/>.
        /// </summary>
        public static readonly DependencyProperty EndPointProperty =
            DependencyProperty.Register( "EndPoint", typeof( Point ), typeof( Arrows ),
                new FrameworkPropertyMetadata( new Point( 0, 0 ), FrameworkPropertyMetadataOptions.AffectsMeasure, dpupdate ) );
        /// <summary>
        /// Возвращает или задает конечную точку для <see cref="Arrows"/>.
        /// Это свойство зависимости.
        /// </summary>    
        public Point EndPoint {
            get { return ( Point ) GetValue( EndPointProperty ); }
            set {
                SetValue( EndPointProperty, value );
                Update();
            }
        }

        #endregion

        #region Поля

        /// <summary>
        /// Флаг характеризующий возможность редактирования <see cref="Arrows"/>.
        /// </summary>
        private bool is_edit = true;

        /// <summary>
        /// Флаг характеризующий выделен ли в данный момент объект <see cref="Arrows"/>.
        /// </summary>
        bool is_active = false;

        /// <summary>
        /// Текстблок комментария.
        /// </summary>
        private TextBlock text_block;

        /// <summary>
        /// Флаг характеризующий является ли стрелка ломанной.
        /// </summary>
        private bool is_polyline = false;

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает Id стрелки.
        /// </summary>
        public uint Id {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает флаг характеризующий возможность редактирования <see cref="Arrows"/>.
        /// </summary>
        public bool IsEdit {
            get {
                return this.is_edit;
            }
            set {
                this.is_edit = value;
            }
        }

        /// <summary>
        /// Возвращает или задает флаг характеризующий является ли <see cref="Arrows"/> ломаной.
        /// </summary>
        public bool IsPolyline {
            get { return this.is_polyline; }
            set {
                this.is_polyline = value;
                this.Update();
            }
        }

        /// <summary>
        /// Возвращает длинну <see cref="Arrows"/>.
        /// </summary>
        public double Length {
            get {
                double length = 0;
                for ( int i = 1; i < line.Points.Count; ++i ) {
                    length += getSegmentLength( line.Points[i], line.Points[i - 1] );
                }
                return length;
            }
        }

        /// <summary>
        /// Задает свойство StrokeDashArray для линии стрелки.
        /// </summary>
        private DoubleCollection ArrowDashArray {
            set { this.line.StrokeDashArray = value; }
            get { return new DoubleCollection( this.line.StrokeDashArray ); }
        }

        /// <summary>
        /// Возвращает или задает текст содержащийся внутри <see cref="Comment"/>
        /// </summary>
        public string Text {
            get { return this.text_box.Text; }
            set { this.text_box.Text = value; }
        }

        /// <summary>
        /// Возвращает или задает флаг, характеризующий является ли стрелка пунктирной.
        /// </summary>
        public bool IsDashed {
            get;
            private set;
        }

        /// <summary>
        /// Возвращает или задает флаг, характеризующий выделен ли в данный момент объект <see cref="Arrows"/>.
        /// </summary>
        bool Active {
            get {
                return this.is_active;
            }
            set {
                if ( value ) {
                    this.is_active = true;
                    middle_arrow.Template = cArrow.Resources["tLineThumbFocused"] as ControlTemplate;
                    start_arrow.Template = end_arrow.Template = cArrow.Resources["tLineButtonFocused"] as ControlTemplate;
                }
                else {
                    this.is_active = false;
                    middle_arrow.Template = cArrow.Resources["tLineThumb"] as ControlTemplate;
                    start_arrow.Template = end_arrow.Template = cArrow.Resources["tLineButton"] as ControlTemplate;
                }
            }
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        protected Arrows() {
            InitThis();
        }

        /// <summary>
        /// Конструктор с указанием начальной и конечной точек.
        /// </summary>
        /// <param name="start_point">Начальная точка.</param>
        /// <param name="EndPoint">Конечная точка.</param>
        /// <param name="is_polyline">Может ли стрелка искривляться.</param>
        private Arrows( Point start_point, Point end_point, bool is_polyline ) {
            InitThis();
            this.IsPolyline = is_polyline;
            this.StartPoint = start_point;
            this.EndPoint = end_point;
        }

        #endregion

        #region Функции-члены

        /// <summary>
        /// Получает длинну отрезка.
        /// </summary>
        /// <param name="first">Начальная точка отрезка.</param>
        /// <param name="second">Конечная точка отрезка.</param>
        /// <returns>Длинна отрезка.</returns>
        private double getSegmentLength( Point first, Point second ) {
            return Math.Sqrt(
                            Math.Pow( first.X - second.X, 2.0 ) +
                            Math.Pow( first.Y - second.Y, 2.0 )
                        );
        }

        /// <summary>
        /// Обновляет позицию центральной точки стрелки.
        /// </summary>
        private void updateMiddleArrowPosition() {
            if ( !this.IsPolyline ) {
                this.middle_arrow.Visibility = System.Windows.Visibility.Visible;
                Canvas.SetLeft( middle_arrow, ( EndPoint.X + StartPoint.X ) / 2 - 3.5 );
                Canvas.SetTop( middle_arrow, ( EndPoint.Y + StartPoint.Y ) / 2 - 3.5 );
            }
            else {
                this.middle_arrow.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        /// <summary>
        /// Инициализация стрелки.
        /// </summary>
        private void InitThis() {
            InitializeComponent();
            this.Focusable = true;
            this.LostFocus += new RoutedEventHandler( arrow_LostFocus );
            middle_arrow.DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler( middle_arrow_DragDelta );
            middle_arrow.DragCompleted += new DragCompletedEventHandler( middle_arrow_DragCompleted );
        }

        /// <summary>
        /// Обновляет расположение комментария над стрелкой.
        /// </summary>
        private void updateTextBox() {
            double length = this.Length / 2;
            double segment = 0;
            int i = 0;
            while ( length > 0 ) {
                ++i;
                segment = this.getSegmentLength( line.Points[i], line.Points[i - 1] );
                if ( segment < length ) {
                    length -= segment;
                }
                else {
                    length = 0;
                }
            }
            Point middle_point = new Point(
                    ( line.Points[i].X + line.Points[i - 1].X ) / 2,
                    ( line.Points[i].Y + line.Points[i - 1].Y ) / 2
                );
            double angle = Math.Atan2(
                    ( line.Points[i].Y - line.Points[i - 1].Y ),
                    ( line.Points[i].X - line.Points[i - 1].X )
                );
            Canvas.SetLeft( this.text_box, middle_point.X - text_box.Width / 2 );
            Canvas.SetTop( this.text_box, middle_point.Y - text_box.Height / 2 );
            double grad_angle = angle / Math.PI * 180;
            if ( Math.Abs( grad_angle ) > 90 ) {
                grad_angle -= 180;
            }
            RotateTransform rt = new RotateTransform( grad_angle, this.text_box.Width / 2, this.text_box.Height / 2 );
            this.text_box.RenderTransform = rt;
            Canvas.SetLeft( this.text_box, middle_point.X - text_box.Width / 2 );
            Canvas.SetTop( this.text_box, middle_point.Y - text_box.Height / 2 );
        }

        #endregion

        #region Статические методы

        /// <summary>
        /// Возвращает стрелку у которой id соответствует переданному. 
        /// </summary>
        /// <param name="id">Id стрелки.</param>
        /// <returns>Найденная стрелка.</returns>
        internal static Arrows findArrowById( uint id ) {
            List<Arrows> list = new List<Arrows>();
            Helpers.FindAll<Arrows>( WorkPanel.CurrentWorkPanel, list );
            return list.Find( it => it.Id == id );
        }

        /// <summary>
        /// Десириализует объект типа <see cref="Arrows"/>.
        /// </summary>
        /// <param name="serialize_string">Сериализованные данные.</param>
        /// <returns>Десириализованный объект типа <see cref="Arrows"/>.</returns>
        public static Arrows deserialize( List<string> serialize_string, bool clipboard_operation = false ) {
            string str = serialize_string[0];
            serialize_string.RemoveAt( 0 );
            Arrows arrow = null;
            if ( str == "Arrows" ) {
                uint id = Serialize.getSerializeUintParam( serialize_string[0], "Id : " );
                if ( clipboard_operation ) {
                    id = WorkPanel.getNextId();
                }
                serialize_string.RemoveAt( 0 );
                Point start = new Point();
                start.X = Serialize.getSerializeDoubleParam( serialize_string[0], "StartPoint.X : " );
                serialize_string.RemoveAt( 0 );
                start.Y = Serialize.getSerializeDoubleParam( serialize_string[0], "StartPoint.Y : " );
                serialize_string.RemoveAt( 0 );
                Point end = new Point();
                end.X = Serialize.getSerializeDoubleParam( serialize_string[0], "EndPoint.X : " );
                serialize_string.RemoveAt( 0 );
                end.Y = Serialize.getSerializeDoubleParam( serialize_string[0], "EndPoint.Y : " );
                serialize_string.RemoveAt( 0 );
                bool is_polyline = Serialize.getSerializeBoolParam( serialize_string[0], "IsPolyline : " );
                serialize_string.RemoveAt( 0 );
                bool is_dashed = Serialize.getSerializeBoolParam( serialize_string[0], "IsDashed : " );
                serialize_string.RemoveAt( 0 );
                if ( is_dashed ) {
                    arrow = Arrows.ParallelCall( start, end, is_polyline );
                }
                else {
                    arrow = Arrows.ConsecutiveCall( start, end, is_polyline );
                }
                arrow.Id = id;
                arrow.Text = Serialize.getSerializeStringParam( serialize_string[0], "Text : " );
                serialize_string.RemoveAt( 0 );
            }
            return arrow;
        }

        /// <summary>
        /// Возвращает объект <see cref="Arrows"/> типа "Параллельный вызов".
        /// </summary>
        /// <param name="StartPoint">Начальная точка.</param>
        /// <param name="EndPoint">Конечная точка.</param>
        /// <returns>Объект стрелки.</returns>
        public static Arrows ParallelCall( Point StartPoint, Point EndPoint, bool is_polyline = false ) {
            Arrows arrow = new Arrows( StartPoint, EndPoint, is_polyline );
            arrow.IsDashed = true;
            arrow.ArrowDashArray = new DoubleCollection( new double[] { 4, 2 } );
            arrow.Id = WorkPanel.getNextId();
            return arrow;
        }

        /// <summary>
        /// Возвращает объект <see cref="Arrows"/> типа "Последовательный вызов".
        /// </summary>
        /// <param name="StartPoint">Начальная точка.</param>
        /// <param name="EndPoint">Конечная точка.</param>
        /// <returns>Объект стрелки.</returns>
        public static Arrows ConsecutiveCall( Point StartPoint, Point EndPoint, bool is_polyline = false ) {
            Arrows arrow = new Arrows( StartPoint, EndPoint, is_polyline );
            arrow.IsDashed = false;
            arrow.Id = WorkPanel.getNextId();
            return arrow;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Сериализует объект типа <see cref="Arrows"/>.
        /// </summary>
        /// <returns>Строка с сериализованными данными.</returns>
        public string serialize() {
            string ret = "Arrows\r\n";
            ret += "Id : " + Id + "\r\n";
            ret += "StartPoint.X : " + StartPoint.X + "\r\n";
            ret += "StartPoint.Y : " + StartPoint.Y + "\r\n";
            ret += "EndPoint.X : " + EndPoint.X + "\r\n";
            ret += "EndPoint.Y : " + EndPoint.Y + "\r\n";
            ret += "IsPolyline : " + IsPolyline + "\r\n";
            ret += "IsDashed : " + IsDashed + "\r\n";
            ret += "Text : " + Text + "\r\n";
            ret += "\r\n";
            return ret;
        }

        /// <summary>
        /// Получает угол наклона конца стрелки.
        /// </summary>
        /// <returns>Угол в радианах.</returns>
        public double getEndArrowAngle() {
            if ( this.IsPolyline ) {
                return Math.Atan2(
                    ( line.Points[line.Points.Count - 1].Y - line.Points[line.Points.Count - 2].Y ),
                    ( line.Points[line.Points.Count - 1].X - line.Points[line.Points.Count - 2].X )
                );
            }
            else {
                return Math.Atan2(
                    ( EndPoint.Y - StartPoint.Y ),
                    ( EndPoint.X - StartPoint.X )
                );
            }

        }

        /// <summary>
        /// Обновляет координаты стрелки.
        /// </summary>
        public void Update() {
            this.line.Points.Clear();
            if ( this.IsPolyline ) {
                if ( this.StartPoint.X != 0 && this.StartPoint.Y != 0
                     && this.EndPoint.X != 0 && this.EndPoint.Y != 0 ) {
                    List<Point> points = AstarAlgoritm.FindPath( this.StartPoint, this.EndPoint );
                    if ( points != null ) {
                        line.Points = new PointCollection( points );
                    }
                    else {
                        line.Points = new PointCollection();
                    }
                }
            }
            if ( line.Points.Count == 0 ) {
                line.Points.Add( this.StartPoint );
            }
            if ( line.Points.Count == 1 ) {
                line.Points.Add( this.EndPoint );
            }
            Canvas.SetLeft( start_arrow, StartPoint.X - 3.5 );
            Canvas.SetTop( start_arrow, StartPoint.Y - 3.5 );
            Canvas.SetLeft( end_arrow, this.EndPoint.X - 3.5 );
            Canvas.SetTop( end_arrow, this.EndPoint.Y - 3.5 );
            updateMiddleArrowPosition();
            double angle = 0;
            Point end_point = new Point();
            PointCollection head_points = new PointCollection();
            if ( this.IsPolyline ) {
                angle = getEndArrowAngle();
                end_point = new Point(
                    line.Points[line.Points.Count - 1].X - 15 * Math.Cos( angle ),
                    line.Points[line.Points.Count - 1].Y - 15 * Math.Sin( angle )
                );
                head_points.Add( new Point(
                    end_point.X - 4 * Math.Sin( angle ),
                    end_point.Y + 4 * Math.Cos( angle ) )
                );
                head_points.Add( new Point(
                    end_point.X + 4 * Math.Sin( angle ),
                    end_point.Y - 4 * Math.Cos( angle ) )
                );
                head_points.Add( new Point(
                    end_point.X + 15 * Math.Cos( angle ),
                    end_point.Y + 15 * Math.Sin( angle ) )
                );
            }
            else {
                line.Points[0] = StartPoint;
                angle = getEndArrowAngle();
                end_point = new Point(
                    EndPoint.X - 15 * Math.Cos( angle ),
                    EndPoint.Y - 15 * Math.Sin( angle )
                );
                head_points.Add( new Point(
                    end_point.X - 4 * Math.Sin( angle ),
                    end_point.Y + 4 * Math.Cos( angle ) )
                );
                head_points.Add( new Point(
                    end_point.X + 4 * Math.Sin( angle ),
                    end_point.Y - 4 * Math.Cos( angle ) )
                );
                head_points.Add( new Point(
                    end_point.X + 15 * Math.Cos( angle ),
                    end_point.Y + 15 * Math.Sin( angle ) )
                );
            }
            line.Points[line.Points.Count - 1] = end_point;
            head.Points = head_points;
            this.updateTextBox();
        }

        /// <summary>
        /// Возвращает список геометрий в которых находиться стрелка.
        /// </summary>
        /// <returns>Список геометрий.</returns>
        public List<Rect> getGeometries() {
            Rect rect = new Rect();
            List<Rect> ret = new List<Rect>();
            if ( line.Points.Count > 2 ) {
                int i = 1;
                while ( i < line.Points.Count ) {
                    if ( Math.Abs( line.Points[i].X - line.Points[i - 1].X ) < 0.2 ) {
                        rect = new Rect(
                            line.Points[i - 1].X - 3.5,
                            Math.Min( line.Points[i - 1].Y, line.Points[i].Y ),
                            7,
                            Math.Abs( line.Points[i].Y - line.Points[i - 1].Y )
                        );
                    }
                    if ( Math.Abs( line.Points[i].Y - line.Points[i - 1].Y ) < 0.2 ) {
                        rect = new Rect(
                            Math.Min( line.Points[i - 1].X, line.Points[i].X ),
                            line.Points[i - 1].Y - 3.5,
                            Math.Abs( line.Points[i].X - line.Points[i - 1].X ),
                            7
                        );
                    }
                    ret.Add( rect );
                    ++i;
                }
            }
            return ret;
        }

        /// <summary>
        /// Дает фокус полю ввода текста в <see cref="Comment"/>.
        /// </summary>
        public void focusTextBox() {
            Dispatcher.BeginInvoke( DispatcherPriority.Input,
                                    new Action( delegate() {
                                        this.text_box.Focus();           // Set Logical Focumodules
                                        Keyboard.Focus( this.text_box ); // Set Keyboard Focus
                                    } ) );
        }

        /// <summary>
        /// Копирует <see cref="Arrows"/> в буфер обмена.
        /// </summary>
        public void CopyArrowToClipboard() {
            string input = this.serialize();
            input = "Hash : " + MD5Hash.GetHashString( input ) + "\r\n" + input;
            try {
                Clipboard.SetText( input );
            }
            catch ( ExternalException ) {
                DialogWithUser.showMessage( "Невозможно копировать элемент в буфер обмена, конфликт работы с буфером обмена" );
            }
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие dpupdate для <see cref="Arrows.StartArrowProperty"/> и <see cref="Arrows.EndArrowProperty"/>
        /// </summary>
        /// <param name="d">Источник.</param>
        /// <param name="e">Параметры.</param>
        private static void dpupdate( DependencyObject d, DependencyPropertyChangedEventArgs e ) {
            ( d as Arrows ).Update();
        }

        /// <summary>
        /// Описывает реакцию на событие PreviewMouseLeftButtonDown произошедшее над концом стрелки.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры.</param>
        private void end_arrow_PreviewMouseLeftButtonDown( object sender, MouseButtonEventArgs e ) {
            WorkPanel.CurrentWorkPanel.doEndArrowMoving( this );
            e.Handled = true;
        }

        /// <summary>
        /// Описывает реакцию на событие PreviewMouseLeftButtonDown произошедшее над началом стрелки.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры.</param>
        private void start_arrow_PreviewMouseLeftButtonDown( object sender, MouseButtonEventArgs e ) {
            WorkPanel.CurrentWorkPanel.doStartArrowMoving( this );
            e.Handled = true;
        }

        /// <summary>
        /// Описывает реакцию на событие DragDelta произошедшее над серединой стрелки.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры.</param>
        private void middle_arrow_DragDelta( object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e ) {
            this.StartPoint = new Point( StartPoint.X + e.HorizontalChange, StartPoint.Y + e.VerticalChange );
            this.EndPoint = new Point( EndPoint.X + e.HorizontalChange, EndPoint.Y + e.VerticalChange );
        }

        /// <summary>
        /// Описывает реакцию на событие DragCompleted произошедшее над серединой стрелки.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры.</param>
        private void middle_arrow_DragCompleted( object sender, DragCompletedEventArgs e ) {
            WorkPanel.CurrentWorkPanel.DeleteAllStartArrowConnections( this );
            WorkPanel.CurrentWorkPanel.DeleteAllEndArrowConnections( this );
        }

        /// <summary>
        /// Описывает реакцию на событие Click для <see cref="Arrows"/>.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры.</param>
        private void bArrow_Click( object sender, RoutedEventArgs e ) {
            if ( is_edit ) {
                FocusManager.SetFocusedElement( this, null );
                this.Active = true;
            }
        }

        /// <summary>
        /// Описывает реакцию на событие LostFocus для <see cref="Arrows"/>.
        /// </summary>
        /// <param name="sender">Источник</param>
        /// <param name="e">Параметры.</param>
        private void arrow_LostFocus( object sender, RoutedEventArgs e ) {
            this.Active = false;
        }

        /// <summary>
        /// Реализует реакцию на сСобытие LostKeyboardFocus для комментария над стрелкой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void text_box_LostKeyboardFocus( object sender, KeyboardFocusChangedEventArgs e ) {
            this.text_box.Focusable = false;
        }

        /// <summary>
        /// Реализует реакцию на событие GotFocus для комментария над стрелкой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void text_box_GotFocus( object sender, RoutedEventArgs e ) {
            RotateTransform rt = new RotateTransform( 0, this.text_box.Width / 2, this.text_box.Height / 2 );
            this.text_box.RenderTransform = rt;
        }

        /// <summary>
        /// Реализует реакцию на событие MouseDoubleClick для <see cref="Arrows"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void bArrow_MouseDoubleClick( object sender, MouseButtonEventArgs e ) {
            this.text_box.Focusable = true;
            this.focusTextBox();
        }

        /// <summary>
        /// Реализует реакцию на событие LostFocus для комментария над стрелкой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void text_box_LostFocus( object sender, RoutedEventArgs e ) {
            this.updateTextBox();
        }

        /// <summary>
        /// Реализует реакцию на событие Initialized для подсказки над стрелкой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void tool_tip_Initialized( object sender, EventArgs e ) {
            text_block = sender as TextBlock;
        }

        /// <summary>
        /// Реализует реакцию на событие PreviewMouseMove для объекта <see cref="Arrows"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Arrows_PreviewMouseMove( object sender, MouseEventArgs e ) {
            if ( this.Text != "" ) {
                tool_tip.Visibility = System.Windows.Visibility.Visible;
                text_block.Text = this.Text;
            }
            else {
                tool_tip.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда удаления.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Delete( object sender, ExecutedRoutedEventArgs e ) {
            WorkPanel.CurrentWorkPanel.deleteElement( this );
            WorkPanel.CurrentWorkPanel.DeleteAllStartArrowConnections( this );
            WorkPanel.CurrentWorkPanel.DeleteAllEndArrowConnections( this );
        }

        /// <summary>
        /// Команда копирования.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Copy( object sender, ExecutedRoutedEventArgs e ) {
            CopyArrowToClipboard();
        }

        /// <summary>
        /// Команда вырезания.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Cut( object sender, ExecutedRoutedEventArgs e ) {
            CopyArrowToClipboard();
            Delete( sender, e );
        }

        /// <summary>
        /// Проверка на доступность команд.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void CommandCanExecutive( object sender, CanExecuteRoutedEventArgs e ) {
            e.CanExecute = this.Active;
        }

        #endregion
    }
}
