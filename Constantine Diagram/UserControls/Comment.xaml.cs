﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Constantine_Diagram.Elements;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.ProjectHelpers;

namespace Constantine_Diagram.UserControls
{
    /// <summary>
    /// Реализует элемнт типа "Комментарий"
    /// </summary>
    public partial class Comment : UserControl, IDiagrammElement, ISerializeble
    {
        #region Поля

        /// <summary>
        /// Объект <see cref="ContentContainer"/> в котором расположен текущий объект <see cref="Comment"/>
        /// </summary>
        private ContentContainer container;

        /// <summary>
        /// Текстблок комментария.
        /// </summary>
        private TextBlock text_block;

        #endregion

        #region Зависимые свойства

        /// <summary>
        /// Зависимое свойство для задания центра для <see cref="Comment"/>.
        /// </summary>
        public static readonly DependencyProperty CenterPointProperty = DependencyProperty.Register( "Center", typeof( Point ), typeof( Comment ), new FrameworkPropertyMetadata( new Point( 0, 0 ), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault ) );
        /// <summary>
        /// Возвращает или задает центр для <see cref="Comment"/>. 
        /// Это свойство зависимости.
        /// </summary>  
        public Point Center {
            get {
                return this.container.Center;
            }
            set {
                SetValue( CenterPointProperty, value );
                container.Center = value;
            }
        }

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает текст содержащийся внутри <see cref="Comment"/>
        /// </summary>
        public string Text {
            get { return this.text_box.Text; }
            set { this.text_box.Text = value; }
        }

        /// <summary>
        /// Возвращает или задает ширину <see cref="ContentContainer"/> в котором находиться комментарий.
        /// </summary>
        public double ContainerWidth {
            get { return container.ActualWidth; }
            set { container.Width = value; }
        }

        /// <summary>
        /// Возвращает или задает высоту <see cref="ContentContainer"/> в котором находиться комментарий.
        /// </summary>
        public double ContainerHeight {
            get { return container.ActualHeight; }
            set { container.Height = value; }
        }

        /// <summary>
        /// Возвращает или задает Id объекта <see cref="Comment"/>.
        /// </summary>
        public uint Id {
            get;
            set;
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Инициализирует объект <see cref="Comment"/>.
        /// </summary>
        private Comment() {
            InitializeComponent();
        }

        /// <summary>
        /// Инициализирует объект <see cref="Comment"/> и центрирует его в заданной точке.
        /// </summary>
        /// <param name="center">Центр объекта <see cref="Comment"/>.</param>
        public Comment( Point center ) {
            InitializeComponent();
            container = new ContentContainer( this, new Point( center.X, center.Y ) );
            container.MinHeight += 20;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Позвращает готовый для отрисовки объект.
        /// </summary>
        /// <returns>Объект <see cref="Comment"/> типа UIElement.</returns>
        public ContentContainer getComment() {
            return this.container;
        }

        /// <summary>
        /// Дает фокус полю ввода текста в <see cref="Comment"/>.
        /// </summary>
        public void focusTextBox() {
            ( this.text_box as InternalDiagramElementTextBox ).focus();
        }

        /// <summary>
        /// Сериализует объект типа <see cref="Comment"/>.
        /// </summary>
        /// <returns>Строка с сериализованными данными.</returns>
        public string serialize() {
            string ret = "Comment\r\n";
            ret += "Center.X : " + Center.X + "\r\n";
            ret += "Center.Y : " + Center.Y + "\r\n";
            ret += "ContainerWidth : " + ContainerWidth + "\r\n";
            ret += "ContainerHeight : " + ContainerHeight + "\r\n";
            ret += "Text : " + Text + "\r\n";
            ret += "\r\n";
            return ret;
        }

        #endregion

        #region Статические методы

        /// <summary>
        /// Десириализует объект типа <see cref="Comment"/>.
        /// </summary>
        /// <param name="serialize_string">Сериализованные данные.</param>
        /// <returns>Десириализованный объект типа <see cref="Arrows"/>.</returns>
        public static Comment deserialize( List<string> serialize_string ) {
            string str = serialize_string[0];
            serialize_string.RemoveAt( 0 );
            Comment comment = null;
            if ( str == "Comment" ) {
                Point center = new Point();
                center.X = Serialize.getSerializeDoubleParam( serialize_string[0], "Center.X : " );
                serialize_string.RemoveAt( 0 );
                center.Y = Serialize.getSerializeDoubleParam( serialize_string[0], "Center.Y : " );
                serialize_string.RemoveAt( 0 );
                comment = new Comment( center );
                double width = Serialize.getSerializeDoubleParam( serialize_string[0], "ContainerWidth : " );
                comment.ContainerWidth = width;
                serialize_string.RemoveAt( 0 );
                double height = Serialize.getSerializeDoubleParam( serialize_string[0], "ContainerHeight : " );
                comment.ContainerHeight = height;
                serialize_string.RemoveAt( 0 );
                comment.Text = Serialize.getSerializeStringParam( serialize_string[0], "Text : " );
                serialize_string.RemoveAt( 0 );
                comment.getComment().Center = center;
            }
            return comment;
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие MouseDoubleClick для <see cref="Comment"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void comment_button_MouseDoubleClick( object sender, MouseButtonEventArgs e ) {
            comment.Template = this.Resources["tCommentElementTextFocused"] as ControlTemplate;
            focusTextBox();
        }

        /// <summary>
        /// Описывает реакцию на событие LostKeyboardFocus для объекта типа TextBox расположенного внутри <see cref="Comment"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void text_box_LostKeyboardFocus( object sender, KeyboardFocusChangedEventArgs e ) {
            comment.Template = this.Resources["tCommentElement"] as ControlTemplate;
            container.Active = false;
        }

        /// <summary>
        /// Описывает реакцию на событие GotKeyboardFocus для объекта типа TextBox расположенного внутри <see cref="Comment"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void text_box_GotKeyboardFocus( object sender, KeyboardFocusChangedEventArgs e ) {
            container.Active = false;
        }

        /// <summary>
        /// Описывает реакцию на событие PreviewMouseMove для <see cref="Comment"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Comment_PreviewMouseMove( object sender, MouseEventArgs e ) {
            if ( this.Text != "" ) {
                tool_tip.Visibility = System.Windows.Visibility.Visible;
                text_block.Text = this.Text;
            }
            else {
                tool_tip.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        /// <summary>
        /// Описывает реакцию на событие Initialized для ToolTip к <see cref="Comment"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void tool_tip_Initialized( object sender, EventArgs e ) {
            this.text_block = sender as TextBlock;
        }

        #endregion
    }
}
