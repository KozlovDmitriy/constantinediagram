﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Constantine_Diagram.Elements;

namespace Constantine_Diagram.UserControls
{

    /// <summary>
    /// Реализует список шаблонов для диаграммы.
    /// </summary>
    public partial class TemplatesListBox : UserControl
    {
        #region Поля

        /// <summary>
        /// Коллекция элементов для спика шаблонов.
        /// </summary>
        private ObservableCollection<TemplateItem> template_items = null;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public TemplatesListBox() {
            InitializeComponent();
        }

        #endregion

        #region Методы

        /// <summary>
        /// Обновляет список шаблонов.
        /// </summary>
        public void UpdateList() {
            if ( !Directory.Exists( "Templates" ) ) {
                Directory.CreateDirectory( "Templates" );
            }
            this.template_items.Clear();
            string[] cdt_files = Directory.GetFiles( "Templates", "*.cdt", System.IO.SearchOption.TopDirectoryOnly );
            foreach ( string name in cdt_files ) {
                this.template_items.Add( new TemplateItem() {
                    Name = name.Remove( 0, "Templates\\".Length ).Replace( ".cdt", "" ),
                    ToolTipName = "Шаблон : " + name.Remove( 0, "Templates\\".Length ).Replace( ".cdt", "" ),
                    ImagePath = new System.IO.FileInfo( name.Replace( ".cdt", ".png" ) ).FullName
                } );
            }
        }

        #endregion

        #region События

        /// <summary>
        /// Перегруженная реакция на событие OnInitialized для <see cref="TemplatesListBox"/>.
        /// </summary>
        /// <param name="e">Параметры.</param>
        protected override void OnInitialized( EventArgs e ) {
            base.OnInitialized( e );
            this.template_items = new ObservableCollection<TemplateItem>();
            this.list_box.ItemsSource = template_items;
            this.UpdateList();
        }

        /// <summary>
        /// Добавление шаблона из файла.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void AddTemplateFromFile( object sender, RoutedEventArgs e ) {
            if ( list_box.SelectedItem != null ) {
                string filename = "Templates\\" + ( list_box.SelectedItem as TemplateItem ).Name + ".cdt";
                try {
                    FileStream work_stream = new FileStream( filename, FileMode.Open );
                    WorkPanel.CurrentWorkPanel.AddTemplateToProject( work_stream );
                    DialogWithUser.showMessage( "Выбранный шаблон добавлен к проекту" );
                }
                catch ( System.Exception exp ) {
                    if ( exp.Message != "Пустое имя пути не допускается." ) {
                        MessageBox.Show( exp.Message );
                    }
                }
            }
        }

        /// <summary>
        /// Удаление текущего выделенного шаблона.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void DeleteSelectedTemplate( object sender, RoutedEventArgs e ) {
            if ( list_box.SelectedItem != null ) {
                MessageBoxResult message_box_result = MessageBox.Show(
                    "Вы действительно хотите удалить шаблон \"" + ( list_box.SelectedItem as TemplateItem ).Name + "\"?",
                    "Подтверждение удаления шаблона",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question
                );
                if ( message_box_result == MessageBoxResult.Yes ) {
                    string filename = "Templates\\" + ( list_box.SelectedItem as TemplateItem ).Name + ".cdt";
                    try {
                        template_items.RemoveAt( list_box.SelectedIndex );
                        FileInfo template_file = new FileInfo( filename );
                        template_file.Delete();
                    }
                    catch ( System.Exception exp ) {
                        MessageBox.Show( exp.Message );
                    }
                }
            }
        }

        #endregion
    }
}
