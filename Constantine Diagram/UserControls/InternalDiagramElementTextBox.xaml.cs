﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Constantine_Diagram.UserControls
{
    /// <summary>
    /// Реализует блок ввода текста для элементов диаграммы.
    /// </summary>
    public partial class InternalDiagramElementTextBox : TextBox
    {
        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public InternalDiagramElementTextBox() {
            InitializeComponent();
        }

        #endregion

        #region Методы

        /// <summary>
        /// Передает фокус полю ввода текста в <see cref="InternalDiagramElementTextBox"/>.
        /// </summary>
        public void focus() {
            Dispatcher.BeginInvoke(
                DispatcherPriority.Input,
                new Action( delegate() {
                this.Focus();           // Set Logical Focus
                Keyboard.Focus( this ); // Set Keyboard Focus
            } )
            );
        }

        #endregion

        #region События

        /// <summary>
        /// Реализует реакцию на событие LostKeyboardFocus для элемента <see cref="InternalDiagramElementTextBox"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void text_LostKeyboardFocus( object sender, KeyboardFocusChangedEventArgs e ) {
            DialogWithUser.showMessage( "Закончено редактирование комментария элемента диаграммы." );
        }

        #endregion
    }
}
