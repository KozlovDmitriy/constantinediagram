﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Constantine_Diagram.Elements;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.ProjectHelpers;

namespace Constantine_Diagram.UserControls
{
    /// <summary>
    /// Реализует элемент типа "Модуль"
    /// </summary>
    public partial class Module : UserControl, IDiagrammElement, ISerializeble
    {
        #region Поля

        /// <summary>
        /// Объект <see cref="ContentContainer"/> в котором расположен текущий объект <see cref="Module"/>
        /// </summary>
        private ContentContainer container;

        /// <summary>
        /// Текстблок подсказки.
        /// </summary>
        private TextBlock text_block;

        /// <summary>
        /// Список присоединенных стрелок к внутреннему бордеру модуля.
        /// </summary>
        private List<ModuleBorder.ConnectedArrow> in_border_connect_arrows = null;

        #endregion

        #region Зависимые свойства

        /// <summary>
        /// Зависимое свойство задающее центр для <see cref="Module"/>.
        /// </summary>
        public static readonly DependencyProperty CenterPointProperty = DependencyProperty.Register( "Center", typeof( Point ), typeof( Module ), new FrameworkPropertyMetadata( new Point( 0, 0 ), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, centerUpdate ) );

        /// <summary>
        /// Возвращает или задает центр для <see cref="Module"/>. 
        /// Это свойство зависимости.
        /// </summary> 
        public Point Center {
            get {
                if ( container != null ) {
                    return this.container.Center;
                }
                else {
                    return this.OutBorder.Center;
                }
            }
            set {
                SetValue( CenterPointProperty, value );
                if ( container != null ) {
                    container.Center = value;
                }
                if ( this.OutBorder != null ) {
                    this.OutBorder.Center = value;
                }
                if ( this.InBorder != null ) {
                    this.InBorder.Center = value;
                }
            }
        }

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает id данного <see cref="Module"/>.
        /// </summary>
        public uint Id {
            get;
            set;
        }

        /// <summary>
        /// Внешний бордер.
        /// </summary>
        public ModuleBorder OutBorder { get; private set; }

        /// <summary>
        /// Внутренний бордер.
        /// </summary>
        public ModuleBorder InBorder { get; private set; }

        /// <summary>
        /// Возвращает или задает текст содержащийся внутри <see cref="Module"/>
        /// </summary>
        public string Text {
            get { return this.text_box.Text; }
            set { this.text_box.Text = value; }
        }

        /// <summary>
        /// Возвращает или задает ширину <see cref="ContentContainer"/> в котором находиться модуль.
        /// </summary>
        public double ContainerWidth {
            get { return container.ActualWidth; }
            set { container.Width = value; }
        }

        /// <summary>
        /// Возвращает или задает высоту <see cref="ContentContainer"/> в котором находиться модуль.
        /// </summary>
        public double ContainerHeight {
            get { return container.ActualHeight; }
            set { container.Height = value; }
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Инициализирует объект <see cref="Module"/>.
        /// </summary>
        public Module() {
            InitializeComponent();
            this.Id = WorkPanel.getNextId();
        }

        /// <summary>
        /// Инициализирует объект <see cref="Module"/> и центрирует его в заданной точке.
        /// </summary>
        /// <param name="center">Центр объекта <see cref="Module"/>.</param>
        public Module( Point center ) {
            container = new ContentContainer( this, new Point( center.X, center.Y ) );
            InitializeComponent();
            this.Id = WorkPanel.getNextId();
        }

        #endregion

        #region Методы

        /// <summary>
        /// Обновляет параметры связи с присоединенным объектом <see cref="Сonditions"/>
        /// </summary>
        public void updateConditionConnections() {
            if ( this.OutBorder != null ) {
                this.OutBorder.updateConditionConnections();
            }
        }

        /// <summary>
        /// Возвращает готовый для отрисовки объект модуля.
        /// </summary>
        /// <returns>Объект <see cref="Module"/> обернутого в <see cref="ContentContainer"/>.</returns>
        public ContentContainer getModule() {
            return this.container;
        }

        /// <summary>
        /// Дает фокус полю ввода текста в <see cref="Module"/>.
        /// </summary>
        public void focusTextBox() {
            ( this.text_box as InternalDiagramElementTextBox ).focus();
        }

        /// <summary>
        /// Сериализует объект <see cref="Module"/>.
        /// </summary>
        /// <returns>Текст сериализации.</returns>
        public string serialize() {
            string ret = "Module\r\n";
            ret += "Id : " + Id + "\r\n";
            ret += "Center.X : " + Center.X + "\r\n";
            ret += "Center.Y : " + Center.Y + "\r\n";
            ret += "ContainerWidth : " + ContainerWidth + "\r\n";
            ret += "ContainerHeight : " + ContainerHeight + "\r\n";
            ret += "Text : " + Text + "\r\n";
            ret += "\r\n";
            return ret;
        }

        /// <summary>
        /// Сериализует все связи с существующими объектами диаграммы для <see cref="Module"/>.
        /// </summary>
        /// <returns>Строка с сериализованными данными.</returns>
        public string serializeConnections() {
            string ret = "";
            if ( this.OutBorder.ConnectCondition.condition != null ) {
                ret += "ConnectCondition\r\n";
                ret += "ModuleId : " + Id + "\r\n";
                ret += "ConnectConditionID : " + this.OutBorder.ConnectCondition.condition.Id + "\r\n";
                ret += "ConnectConditionIndentCoefficient : " + this.OutBorder.ConnectCondition.indent_coefficient + "\r\n";
                ret += "ConnectConditionPointLocation : " + ( uint ) this.OutBorder.ConnectCondition.point_location + "\r\n";
                ret += "\r\n";
            }
            foreach ( var connect in this.OutBorder.ConnectedArrows ) {
                ret += "ModuleOutBorderConnnectedArrows\r\n";
                ret += "ModuleId : " + Id + "\r\n";
                ret += "ConnectArrowID : " + connect.arrow.Id + "\r\n";
                ret += "ConnectArrowPointLocation : " + ( uint ) connect.arrow_point_location + "\r\n";
                ret += "ConnectArrowInOrOut : " + ( uint ) connect.in_or_out_arrow + "\r\n";
                ret += "ConnectArrowIndentCoefficient : " + connect.indent_coefficient + "\r\n";
                ret += "\r\n";
            }
            foreach ( var connect in this.InBorder.ConnectedArrows ) {
                ret += "InBorderConnnectedArrows\r\n";
                ret += "ModuleId : " + Id + "\r\n";
                ret += "ConnectArrowID : " + connect.arrow.Id + "\r\n";
                ret += "ConnectArrowPointLocation : " + ( uint ) connect.arrow_point_location + "\r\n";
                ret += "ConnectArrowInOrOut : " + ( uint ) connect.in_or_out_arrow + "\r\n";
                ret += "ConnectArrowIndentCoefficient : " + connect.indent_coefficient + "\r\n";
                ret += "\r\n";
            }
            return ret;
        }

        #endregion

        #region Статические методы

        /// <summary>
        /// Десириализует объект типа <see cref="Module"/>.
        /// </summary>
        /// <param name="serialize_string">Сериализованные данные.</param>
        /// <returns>Десириализованный объект типа <see cref="Module"/>.</returns>
        public static Module deserialize( List<string> serialize_string, bool clipboard_operation = false ) {
            string str = serialize_string[0];
            serialize_string.RemoveAt( 0 );
            Module module = null;
            if ( str == "Module" ) {
                uint id = Serialize.getSerializeUintParam( serialize_string[0], "Id : " );
                if ( clipboard_operation ) {
                    id = WorkPanel.getNextId();
                }
                serialize_string.RemoveAt( 0 );
                Point center = new Point();
                center.X = Serialize.getSerializeDoubleParam( serialize_string[0], "Center.X : " );
                serialize_string.RemoveAt( 0 );
                center.Y = Serialize.getSerializeDoubleParam( serialize_string[0], "Center.Y : " );
                serialize_string.RemoveAt( 0 );
                module = new Module( center );
                module.Id = id;
                double width = Serialize.getSerializeDoubleParam( serialize_string[0], "ContainerWidth : " );
                module.ContainerWidth = width;
                serialize_string.RemoveAt( 0 );
                double height = Serialize.getSerializeDoubleParam( serialize_string[0], "ContainerHeight : " );
                module.ContainerHeight = height;
                serialize_string.RemoveAt( 0 );
                module.Text = Serialize.getSerializeStringParam( serialize_string[0], "Text : " );                
                serialize_string.RemoveAt( 0 );
                module.getModule().Center = center;
            }
            return module;
        }

        /// <summary>
        /// Десериализует все связи с <see cref="Conditions"/> диаграммы для <see cref="Module"/>.
        /// </summary>
        /// <param name="serialize_string">Сериализованные данные.</param>
        public static void deserializeConnectCondition( List<string> serialize_string ) {
            string str = serialize_string[0];
            serialize_string.RemoveAt( 0 );
            Module module = null;
            if ( str == "ConnectCondition" ) {
                uint module_id = Serialize.getSerializeUintParam( serialize_string[0], "ModuleId : " );
                serialize_string.RemoveAt( 0 );
                module = Module.FindById( module_id );
                uint id = Serialize.getSerializeUintParam( serialize_string[0], "ConnectConditionID : " );
                serialize_string.RemoveAt( 0 );
                ModuleBorder.ConnectedCondition connect_condition = new ModuleBorder.ConnectedCondition();
                connect_condition.condition = Conditions.findConditionById( id );
                connect_condition.indent_coefficient =
                    Serialize.getSerializeDoubleParam( serialize_string[0], "ConnectConditionIndentCoefficient : " );
                serialize_string.RemoveAt( 0 );
                connect_condition.point_location = ( ConnectOrientation ) Serialize.getSerializeUintParam(
                        serialize_string[0],
                        "ConnectConditionPointLocation : "
                    );
                serialize_string.RemoveAt( 0 );
                module.OutBorder.connectCondition( connect_condition );
            }
        }

        /// <summary>
        /// Десериализует все связи с <see cref="Arrows"/> диаграммы для внешнего бордера элемента <see cref="Module"/>.
        /// </summary>
        /// <param name="serialize_string">Сериализованные данные.</param>
        public static void deserializeOutBorderConnnectedArrows( List<string> serialize_string, bool clipboard_operation = false ) {
            string str = serialize_string[0];
            serialize_string.RemoveAt( 0 );
            Module module = null;
            if ( str == "ModuleOutBorderConnnectedArrows" ) {
                uint module_id = Serialize.getSerializeUintParam( serialize_string[0], "ModuleId : " );
                if ( clipboard_operation ) {
                    module_id = Conditions.ModuleId[0];
                    Conditions.ModuleId.RemoveAt( 0 );
                }
                serialize_string.RemoveAt( 0 );
                module = Module.FindById( module_id );
                uint id = Serialize.getSerializeUintParam( serialize_string[0], "ConnectArrowID : " );
                if ( clipboard_operation ) {
                    id = Conditions.ArrowId[0];
                    Conditions.ArrowId.RemoveAt( 0 );
                }
                serialize_string.RemoveAt( 0 );
                ModuleBorder.ConnectedArrow connect_arrow = new ModuleBorder.ConnectedArrow();
                connect_arrow.arrow = Arrows.findArrowById( id );
                connect_arrow.arrow_point_location = ( ConnectOrientation ) Serialize.getSerializeUintParam(
                        serialize_string[0],
                        "ConnectArrowPointLocation : "
                    );
                serialize_string.RemoveAt( 0 );
                connect_arrow.in_or_out_arrow = ( InOrOutArrow ) Serialize.getSerializeUintParam(
                        serialize_string[0],
                        "ConnectArrowInOrOut : "
                    );
                serialize_string.RemoveAt( 0 );
                connect_arrow.indent_coefficient =
                    Serialize.getSerializeDoubleParam( serialize_string[0], "ConnectArrowIndentCoefficient : " );
                serialize_string.RemoveAt( 0 );
                module.OutBorder.connectArrow( connect_arrow );
            }
        }

        /// <summary>
        /// Десериализует все связи с <see cref="Arrows"/> диаграммы для внутреннего бордера элемента <see cref="Module"/>.
        /// </summary>
        /// <param name="serialize_string">Сериализованные данные.</param>
        public static void deserializeInBorderConnnectedArrows( List<string> serialize_string ) {
            string str = serialize_string[0];
            serialize_string.RemoveAt( 0 );
            Module module = null;
            if ( str == "InBorderConnnectedArrows" ) {
                uint module_id = Serialize.getSerializeUintParam( serialize_string[0], "ModuleId : " );
                serialize_string.RemoveAt( 0 );
                module = Module.FindById( module_id );
                if ( module.in_border_connect_arrows == null ) {
                    module.in_border_connect_arrows = new List<ModuleBorder.ConnectedArrow>();
                }
                uint id = Serialize.getSerializeUintParam( serialize_string[0], "ConnectArrowID : " );
                serialize_string.RemoveAt( 0 );
                ModuleBorder.ConnectedArrow connect_arrow = new ModuleBorder.ConnectedArrow();
                connect_arrow.arrow = Arrows.findArrowById( id );
                connect_arrow.arrow_point_location = ( ConnectOrientation ) Serialize.getSerializeUintParam(
                        serialize_string[0],
                        "ConnectArrowPointLocation : "
                    );
                serialize_string.RemoveAt( 0 );
                connect_arrow.in_or_out_arrow = ( InOrOutArrow ) Serialize.getSerializeUintParam(
                        serialize_string[0],
                        "ConnectArrowInOrOut : "
                    );
                serialize_string.RemoveAt( 0 );
                connect_arrow.indent_coefficient =
                    Serialize.getSerializeDoubleParam( serialize_string[0], "ConnectArrowIndentCoefficient : " );
                serialize_string.RemoveAt( 0 );
                module.in_border_connect_arrows.Add( connect_arrow );
            }
        }

        /// <summary>
        /// Возвращает модуль у которого id соответствует переданному. 
        /// </summary>
        /// <param name="id">Id искомого модуля.</param>
        /// <returns>Найденный модуль.</returns>
        private static Module FindById( uint id ) {
            foreach ( var child in WorkPanel.CurrentWorkPanel.Children ) {
                if ( child is ContentContainer &&
                     ( child as ContentContainer ).Content is Module &&
                     ( ( child as ContentContainer ).Content as Module ).Id == id ) {
                    return ( child as ContentContainer ).Content as Module;
                }
                if ( child is Conditions ) {
                    if ( ( child as Conditions ).IfModule.Id == id ) {
                        return ( child as Conditions ).IfModule;
                    }
                    if ( ( child as Conditions ).IfElse && ( child as Conditions ).ElseModule.Id == id ) {
                        return ( child as Conditions ).ElseModule;
                    }
                }
            }
            return null;
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие PreviewMouseMove для <see cref="Module"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Module_PreviewMouseMove( object sender, MouseEventArgs e ) {
            if ( this.Text != "" ) {
                tool_tip.Visibility = System.Windows.Visibility.Visible;
                text_block.Text = this.Text;
            }
            else {
                tool_tip.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        /// <summary>
        /// Реализует реакцию на событие Initialized для подсказки над элементом модуля.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void tool_tip_Initialized( object sender, EventArgs e ) {
            text_block = sender as TextBlock;
        }

        /// <summary>
        /// Описывает реакцию на событие изменения значения <see cref="Module.Center"/>.
        /// </summary>
        /// <param name="d">Источник.</param>
        /// <param name="e">Параметры.</param>
        private static void centerUpdate( DependencyObject d, DependencyPropertyChangedEventArgs e ) { }

        /// <summary>
        /// Описывает реакцию на событие MouseDoubleClick для <see cref="Module"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void module_button_MouseDoubleClick( object sender, MouseButtonEventArgs e ) {
            module.Template = this.Resources["tModuleElementTextFocused"] as ControlTemplate;
            focusTextBox();
        }

        /// <summary>
        /// Описывает реакцию на событие LostKeyboardFocus для объекта типа TextBox расположенного внутри <see cref="Module"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void text_box_LostKeyboardFocus( object sender, KeyboardFocusChangedEventArgs e ) {
            this.module.Template = this.Resources["tModuleElement"] as ControlTemplate;
            if ( this.container != null ) {
                this.container.Active = false;
            }
        }

        /// <summary>
        /// Описывает реакцию на событие GotKeyboardFocus для объекта типа TextBox расположенного внутри <see cref="Module"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void text_box_GotKeyboardFocus( object sender, KeyboardFocusChangedEventArgs e ) {
            if ( this.container != null ) {
                container.Active = false;
            }
        }

        #endregion

        #region Внешний бордер

        /// <summary>
        /// Описывает реакцию на событие Initialized для внешнего бордера.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void out_border_Initialized( object sender, EventArgs e ) {
            this.OutBorder = sender as ModuleBorder;
            this.OutBorder.InOrOutBorder = ModuleBorder.InOrOut.OUT;
            if ( this.container != null ) {
                this.OutBorder.SetBinding( Borders.CenterProperty, this.container.CreateConnectorBinding() );
            }
            this.OutBorder.ActiveStyle = this.Resources["sBorderActive"] as Style;
            this.OutBorder.DefaultStyle = this.Resources["sBorderDefault"] as Style;
        }

        #endregion

        #region Внутренний бордер

        /// <summary>
        /// Описывает реакцию на событие Initialized для внутреннего бордера.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void in_border_Initialized( object sender, EventArgs e ) {
            this.InBorder = sender as ModuleBorder;
            this.InBorder.InOrOutBorder = ModuleBorder.InOrOut.IN;
            if ( this.container != null ) {
                this.InBorder.SetBinding( Borders.CenterProperty, this.container.CreateConnectorBinding() );
            }
            this.InBorder.ActiveStyle = this.Resources["sBorderActive"] as Style;
            this.InBorder.DefaultStyle = this.Resources["sBorderDefault"] as Style;
            if ( this.in_border_connect_arrows != null ) {
                foreach ( var it in this.in_border_connect_arrows ) {
                    this.InBorder.connectArrow( it );
                }
            }
        }

        #endregion
    }
}
