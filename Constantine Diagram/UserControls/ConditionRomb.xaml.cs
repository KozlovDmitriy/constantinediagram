﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Constantine_Diagram.Elements;

namespace Constantine_Diagram.UserControls
{
    /// <summary>
    /// Реализует ромб содержащийся в элементе <see cref="Conditions"/>.
    /// </summary>
    public partial class ConditionRomb : UserControl
    {
        #region Зависимые свойства

        /// <summary>
        /// Зависимое свойство задающее центр для <see cref="ConditionRomb"/>.
        /// </summary>
        public static readonly DependencyProperty CenterProperty = DependencyProperty.Register(
                    "Center",
                    typeof( Point ),
                    typeof( ConditionRomb ),
                    new FrameworkPropertyMetadata(
                            new Point( 0, 0 ),
                            FrameworkPropertyMetadataOptions.AffectsMeasure,
                            centerUpdate
                        )
                );
        /// <summary>
        /// Возвращает или задает центр для <see cref="ConditionRomb"/>. 
        /// Это свойство зависимости.
        /// </summary>  
        public Point Center {
            get { return ( Point ) GetValue( CenterProperty ); }
            set {
                SetValue( CenterProperty, value );
            }
        }

        #endregion

        #region Поля

        /// <summary>
        /// <see cref="ConnectOrientation"/> для <see cref="ConditionRomb"/>.
        /// </summary>
        private ConnectOrientation connect_orientation = ConnectOrientation.BOTTOM;
        /// <summary>
        /// Точка из которой исходят стрелки элемента <see cref="Conditions"/>.
        /// </summary>
        private Point start_point_to_arrows;
        /// <summary>
        /// Стрелка типа If.
        /// </summary>
        private Arrows if_arrow;
        /// <summary>
        /// Стрелка типа Else.
        /// </summary>
        private Arrows else_arrow;
        /// <summary>
        /// Объект <see cref="Conditions"/> которому принадлежит данный <see cref="ConditionRomb"/>.
        /// </summary>
        private Conditions condition;

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает <see cref="ConditionRomb.connect_orientation"/>.
        /// </summary>
        public ConnectOrientation RombConnectOrientation {
            get { return this.connect_orientation; }
            set {
                this.connect_orientation = value;
                updateStartPointArrow( value );
                if ( value == ConnectOrientation.LEFT ) {
                    this.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                    this.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                }
                if ( value == ConnectOrientation.RIGHT ) {
                    this.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                    this.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                }
                if ( value == ConnectOrientation.TOP ) {
                    this.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                    this.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                }
                if ( value == ConnectOrientation.BOTTOM ) {
                    this.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                    this.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                }
            }
        }

        /// <summary>
        /// Возвращает или задает <see cref="ConditionRomb.if_arrow"/>.
        /// </summary>
        public Arrows IfArrow {
            get { return if_arrow; }
            set {
                if ( value != null ) {
                    this.if_arrow = value;
                    this.if_arrow.StartPoint = start_point_to_arrows;
                }
            }
        }

        /// <summary>
        /// Возвращает или задает <see cref="ConditionRomb.else_arrow"/>.
        /// </summary>
        public Arrows ElseArrow {
            get { return else_arrow; }
            set {
                if ( value != null ) {
                    this.else_arrow = value;
                    this.else_arrow.StartPoint = start_point_to_arrows;
                }
            }
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public ConditionRomb() {
            InitializeComponent();
        }

        /// <summary>
        /// Инициализирующий конструктор.
        /// </summary>
        /// <param name="orientation"><see cref="ConnectOrientation"/> для <see cref="ConditionRomb"/>.</param>
        /// <param name="condition">Объект <see cref="Conditions"/> которому 
        /// принадлежит данный <see cref="ConditionRomb"/>.</param>
        public ConditionRomb( ConnectOrientation orientation, Conditions condition ) {
            InitializeComponent();
            this.RombConnectOrientation = orientation;
            this.condition = condition;
        }

        #endregion

        #region Функции-члены

        /// <summary>
        /// Обновляет <see cref="ConditionRomb.start_point_to_arrows"/> относительно ориентации <see cref="ConditionRomb"/>.
        /// </summary>
        /// <param name="orientation"></param>
        private void updateStartPointArrow( ConnectOrientation orientation ) {
            if ( orientation == ConnectOrientation.LEFT ) {
                this.start_point_to_arrows = new Point( Center.X - this.Width / 2, Center.Y );
            }
            if ( orientation == ConnectOrientation.RIGHT ) {
                this.start_point_to_arrows = new Point( Center.X + this.Width / 2, Center.Y );
            }
            if ( orientation == ConnectOrientation.TOP ) {
                this.start_point_to_arrows = new Point( Center.X, Center.Y - this.Height / 2 );
            }
            if ( orientation == ConnectOrientation.BOTTOM ) {
                this.start_point_to_arrows = new Point( Center.X, Center.Y + this.Height / 2 );
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Обновляет <see cref="ConditionRomb"/>.
        /// </summary>
        public void update() {
            this.updateStartPointArrow( this.RombConnectOrientation );
            if ( this.if_arrow != null ) {
                this.if_arrow.StartPoint = start_point_to_arrows;
            }
            if ( this.else_arrow != null ) {
                else_arrow.StartPoint = start_point_to_arrows;
            }
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие изменения значения <see cref="ConditionRomb.Center"/>.
        /// </summary>
        /// <param name="d">Источник.</param>
        /// <param name="e">Парметры.</param>
        private static void centerUpdate( DependencyObject d, DependencyPropertyChangedEventArgs e ) {
            ( d as ConditionRomb ).update();
        }

        /// <summary>
        /// Обработка события PreviewMouseLeftButtonDown для <see cref="ConditionRomb"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void romb_PreviewMouseLeftButtonDown( object sender, MouseButtonEventArgs e ) {
            this.romb.Focus();
            if ( this.condition != null && this.condition.Active ) {
                WorkPanel.CurrentWorkPanel.Focus();
                WorkPanel.CurrentWorkPanel.startMovingCondition( Helpers.FindAnchestor<Conditions>( this ) );
            }
            else {
                this.condition.Active = true;
            }
            e.Handled = true;
        }

        /// <summary>
        /// Обработка события LostFocus для <see cref="ConditionRomb"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void romb_LostFocus( object sender, RoutedEventArgs e ) {
            this.condition.Active = false;
        }

        #endregion
    }
}
