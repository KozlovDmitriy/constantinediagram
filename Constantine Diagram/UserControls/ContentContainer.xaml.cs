﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.MathModel;
using System.Runtime.InteropServices;

namespace Constantine_Diagram.UserControls
{
    /// <summary>
    /// Реализует контейнер для элементов диаграммы.
    /// </summary>
    public partial class ContentContainer : ContentControl
    {
        #region Зависимые свойства

        /// <summary>
        /// Зависимое свойство задающее центр для <see cref="ContentContainer"/>.
        /// </summary>
        public static readonly DependencyProperty CenterPointProperty = DependencyProperty.Register( "Center", typeof( Point ), typeof( ContentContainer ), new FrameworkPropertyMetadata( new Point( 0, 0 ), FrameworkPropertyMetadataOptions.AffectsMeasure ) );
        /// <summary>
        /// Возвращает или задает центр для <see cref="ContentContainer"/>. 
        /// Это свойство зависимости.
        /// </summary>  
        public Point Center {
            get {
                //return (Point) GetValue( CenterPointProperty ); 
                return new Point(
                        Canvas.GetLeft( this ) + this.ActualWidth / 2,
                        Canvas.GetTop( this ) + this.ActualHeight / 2
                    );
            }
            set {
                if ( value != this.Center ) {
                    SetValue( CenterPointProperty, value );
                    Canvas.SetLeft( this, value.X - this.Width / 2 );
                    Canvas.SetTop( this, value.Y - this.Height / 2 );

                }
            }
        }

        #endregion

        #region Поля

        /// <summary>
        /// Флаг "активности" для <see cref="ContentContainer"/>.
        /// </summary>
        private bool active = false;

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает флаг, указывающий на то было ли последним событием событие MouseDoubleClick.
        /// </summary>
        public bool IsMouseDoubleClick {
            get;
            private set;
        }

        /// <summary>
        /// Возвращает или задает флаг "активности" для <see cref="ContentContainer"/>.
        /// </summary>
        public bool Active {
            get { return active; }
            set {
                if ( value ) {
                    this.active = true;
                    this.Template = this.Resources["tContentControllerActive"] as ControlTemplate;
                }
                else {
                    this.active = false;
                    this.Template = this.Resources["tContentController"] as ControlTemplate;
                }
            }
        }

        #endregion

        #region Функции-члены

        /// <summary>
        /// Инициализирует объект <see cref="ContentContainer"/>.
        /// </summary>
        private void InitThis() {
            InitializeComponent();
            this.Width = 94;
            this.Height = 64;
            this.MinWidth = 94;
            this.MinHeight = 64;
            this.SetBinding( ContentContainer.CenterPointProperty, this.CreateConnectorBinding() );
            Binding bind = new Binding();
            bind.Mode = BindingMode.TwoWay;
            bind.Source = this;
            bind.Path = new PropertyPath( Canvas.LeftProperty );
            this.GotFocus += new RoutedEventHandler( ContentContainer_GotFocus );
            this.LostFocus += new RoutedEventHandler( ContentContainer_LostFocus );
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Инициализирует объект <see cref="ContentContainer"/>.
        /// </summary>
        public ContentContainer() {
            InitThis();
            Canvas.SetLeft( this, 200 );
            Canvas.SetTop( this, 200 );
            this.Center = new Point( 240, 225 );
        }

        /// <summary>
        /// Инициализирует объект <see cref="ContentContainer"/> и добавляет в него контент.
        /// </summary>
        /// <param name="content">Добавляемый контент контейнера.</param>
        public ContentContainer( object content ) {
            InitThis();
            this.Content = content;
            Canvas.SetLeft( this, 200 );
            Canvas.SetTop( this, 200 );
            this.Center = new Point( 240, 225 );
        }

        /// <summary>
        /// Инициализирует объект <see cref="ContentContainer"/>, центрируя его в заданной точке, и добавляет в него контент.
        /// </summary>
        /// <param name="content">Добавляемый контент контейнера.</param>
        /// <param name="center">Центральная точка.</param>
        public ContentContainer( object content, Point center ) {
            InitThis();
            this.Content = content;
            Canvas.SetLeft( this, center.X - this.Width / 2 );
            Canvas.SetTop( this, center.Y - this.Height / 2 );
            this.Center = center;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Создает бинд для присоединения других объектов к элементам диаграммы.
        /// </summary>
        /// <returns>Бинд.</returns>
        public MultiBinding CreateConnectorBinding() {
            MultiBinding multiBinding = new MultiBinding();
            multiBinding.NotifyOnSourceUpdated = true;
            multiBinding.NotifyOnTargetUpdated = true;
            multiBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            multiBinding.Converter = new ConnectorBindingConverter();
            Binding binding = new Binding();
            binding.Source = this;
            binding.Path = new PropertyPath( Canvas.LeftProperty );
            multiBinding.Bindings.Add( binding );
            binding = new Binding();
            binding.Source = this;
            binding.Path = new PropertyPath( Canvas.TopProperty );
            multiBinding.Bindings.Add( binding );
            binding = new Binding();
            binding.Source = this;
            binding.Path = new PropertyPath( FrameworkElement.ActualWidthProperty );
            multiBinding.Bindings.Add( binding );
            binding = new Binding();
            binding.Source = this;
            binding.Path = new PropertyPath( FrameworkElement.ActualHeightProperty );
            multiBinding.Bindings.Add( binding );
            return multiBinding;
        }

        /// <summary>
        /// Копирует <see cref="ContentContainer"/> в буфер обмена.
        /// </summary>
        public void CopyContainerToClipboard() {
            string input = ( ( ISerializeble ) this.Content ).serialize();
            input = "Hash : " + MD5Hash.GetHashString( input ) + "\r\n" + input;
            try {
                Clipboard.SetText( input );
            }
            catch ( ExternalException ) {
                DialogWithUser.showMessage( "Невозможно копировать элемент в буфер обмена, конфликт работы с буфером обмена" );
            }
        }

        #endregion

        #region События

        /// <summary>
        /// Обработка события MouseDoubleClick для <see cref="ContentContainer"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void move_thumb_MouseDoubleClick( object sender, MouseButtonEventArgs e ) {
            List<Button> list = new List<Button>();
            Helpers.FindAll<Button>( this as DependencyObject, list );
            foreach ( Button element in list ) {
                if ( element != ( sender as Button ) ) {
                    element.RaiseEvent( e );
                }
            }
            IsMouseDoubleClick = true;
        }

        /// <summary>
        /// Обработка события LostFocus для <see cref="ContentContainer"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void ContentContainer_LostFocus( object sender, RoutedEventArgs e ) {
            this.Active = false;
            IsMouseDoubleClick = false;
        }

        /// <summary>
        /// Обработка события GotFocus для <see cref="ContentContainer"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void ContentContainer_GotFocus( object sender, RoutedEventArgs e ) {
            this.Active = true;
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда удаления.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Delete( object sender, ExecutedRoutedEventArgs e ) {
            WorkPanel.CurrentWorkPanel.deleteElement( this );
        }

        /// <summary>
        /// Команда копирования.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Copy( object sender, ExecutedRoutedEventArgs e ) {
            CopyContainerToClipboard();
        }

        /// <summary>
        /// Команда вырезания.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Cut( object sender, ExecutedRoutedEventArgs e ) {
            CopyContainerToClipboard();
            WorkPanel.CurrentWorkPanel.deleteElement( this );
        }

        /// <summary>
        /// Проверка на доступность команд.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void CommandCanExecutive( object sender, CanExecuteRoutedEventArgs e ) {
            e.CanExecute = this.Active;
        }

        #endregion
    }
}
