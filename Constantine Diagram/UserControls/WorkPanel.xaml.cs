﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Constantine_Diagram.Elements;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.MathModel;
using Constantine_Diagram.ProjectHelpers;

namespace Constantine_Diagram.UserControls
{
    /// <summary>
    /// Реализует поведение рабочей области приложения.
    /// </summary>
    public partial class WorkPanel : Canvas
    {
        #region Статические свойства

        /// <summary> 
        /// Ссылка на последний созданный объект рабочего поля. 
        /// </summary>
        public static WorkPanel CurrentWorkPanel { get; private set; }

        /// <summary>
        /// Последний выделенный Id.
        /// </summary>
        public static uint LastId {
            get;
            private set;
        }

        #endregion

        #region Поля

        /// <summary>
        /// Флаг, говорящий о том, опущенна ли левай кнопка мыши над рабочем полем.
        /// </summary>
        private bool is_mouse_left_button_down = false;       

        #endregion

        #region Конструкторы

        /// <summary>
        /// Инициализирует объект <see cref="WorkPanel"/>.
        /// </summary>
        public WorkPanel() {
            InitializeComponent();
            WorkPanel.CurrentWorkPanel = this;
            this.Drop += new DragEventHandler( WorKPanel_Drop );
            this.GotFocus += new RoutedEventHandler( WorkPanel_GotFocus );
            this.PreviewMouseLeftButtonDown += new MouseButtonEventHandler( WorkPanel_PreviewMouseLeftButtonDown );
            this.PreviewMouseMove += new MouseEventHandler( WorkPanel_PreviewMouseMove );
            this.PreviewMouseLeftButtonUp += new MouseButtonEventHandler( WorkPanel_PreviewMouseLeftButtonUp );
        }

        #endregion

        #region Удаление элементов

        /// <summary>
        /// Флаг переводящий <see cref="WorkPanel"/> в режим удаления элементов.
        /// </summary>
        private bool is_deleting = false;

        /// <summary>
        /// Возвращает или задает флаг переводящий <see cref="WorkPanel"/> в режим удаления элементов.
        /// </summary>
        public bool IsDeleting {
            get { return this.is_deleting; }
            set {
                this.is_deleting = value;
                if ( value ) {
                    this.Cursor = Cursors.Cross;
                    btn_WorkPanel.CaptureMouse();
                } else {
                    this.Cursor = Cursors.Arrow;
                    btn_WorkPanel.ReleaseMouseCapture();
                }
            }
        }

        /// <summary>
        /// Удаляет все привязки к начальной точке переданной стрелке.
        /// </summary>
        /// <param name="arrow">Стрелка.</param>
        public void DeleteConditionsConnection( Conditions condition ) {
            List<ModuleBorder> list = new List<ModuleBorder>();
            Helpers.FindAll<ModuleBorder>( this, list );
            foreach ( ModuleBorder border in list ) {
                border.disconnectCondition( condition );
            }
        }

        /// <summary>
        /// Удаляет все привязки к начальной точке переданной стрелке.
        /// </summary>
        /// <param name="arrow">Стрелка.</param>
        public void DeleteAllStartArrowConnections( Arrows arrow ) {
            List<Borders> list = new List<Borders>();
            Helpers.FindAll<Borders>( this, list );
            foreach ( Borders border in list ) {
                border.disconnectArrow( arrow, InOrOutArrow.OUT );
            }
        }

        /// <summary>
        /// Удаляет все привязки к конечной точке переданной стрелке.
        /// </summary>
        /// <param name="arrow">Стрелка.</param>
        public void DeleteAllEndArrowConnections( Arrows arrow ) {
            List<Borders> list = new List<Borders>();
            Helpers.FindAll<Borders>( this, list );
            foreach ( Borders border in list ) {
                border.disconnectArrow( arrow, InOrOutArrow.IN );
            }
        }

        /// <summary>
        /// Удаляет элемент с рабочего поля, если переданный элемент является 
        /// потомком первого порядка для рабочего поля.
        /// </summary>
        /// <param name="element">Удаляемый элемент.</param>
        /// <returns>true - если элемент найден и удален.</returns>
        public bool deleteElement( UIElement element ) {
            if ( this.Children.Contains( element ) ) {
                this.Children.Remove( element );
                return true;
            }
            return false;
        }

        #endregion

        #region Работа с условными элементами

        /// <summary>
        ///  Флаг, говорящий о том, емеет ли добавляечый элемент ветку "Иначе".
        /// </summary>
        private bool is_if_else = false;

        /// <summary>
        /// Флаг, говорящий о том, добавляется ли в данный момент условный элемент на рабочее поле.
        /// </summary>
        private bool is_condition_adding = false;
        /// <summary>
        /// Возвращает флаг, говорящий о том, добавляется ли в данный момент условный элемент на рабочее поле.
        /// </summary>
        public bool IsConditionAdding {
            get { return is_condition_adding; }
        }

        /// <summary>
        /// Флаг, говорящий о том, перемещается ли в данный момент условный элемент по рабочему полю.
        /// </summary>
        private bool is_condition_moving = false;
        /// <summary>
        /// Возвращает флаг, говорящий о том, перемещается ли в данный момент условный элемент по рабочему полю.
        /// </summary>
        public bool IsConditionMoving {
            get { return this.is_condition_moving; }
        }

        /// <summary>
        /// Задает смещение указателя мыши при перемещении условного элемента. 
        /// </summary>
        private Point moving_offset = new Point();

        /// <summary>
        /// Перемещаемый в данный момент условный элемент.
        /// </summary>
        private Conditions condition_element;
        /// <summary>
        /// Возвращает, перемещаемый в данный момент условный элемент.
        /// </summary>
        public Conditions MovingCondition {
            get {
                return this.condition_element;
            }
        }

        /// <summary>
        /// Инициализирует добавление <see cref="Conditions"/> типа "Условный элемент IF" на рабочую панель.
        /// </summary>
        public void addIfCondition() {
            this.is_arrow_adding = false;
            this.adding_arrow = null;
            this.is_condition_adding = true;
            this.is_if_else = false;
        }

        /// <summary>
        /// Инициализирует добавление <see cref="Conditions"/> типа "Условный элемент IF-ELSE" на рабочую панель.
        /// </summary>
        public void addIfElseCondition() {
            this.is_arrow_adding = false;
            this.adding_arrow = null;
            this.is_condition_adding = true;
            this.is_if_else = true;
        }

        /// <summary>
        /// Инициализирует передвижение условного элемента по рабочему полю.
        /// </summary>
        /// <param name="condition">Условный элемент.</param>
        public void startMovingCondition( Conditions condition ) {
            this.is_mouse_left_button_down = true;
            if ( condition != null ) {
                this.condition_element = condition;
                this.is_condition_moving = true;
            }
            switch ( condition.ConditionConnectOrientation ) {
                case ConnectOrientation.LEFT:
                    moving_offset = new Point( -2, 0 );
                    break;
                case ConnectOrientation.RIGHT:
                    moving_offset = new Point( 2, 0 );
                    break;
                case ConnectOrientation.TOP:
                    moving_offset = new Point( 0, -2 );
                    break;
                case ConnectOrientation.BOTTOM:
                    moving_offset = new Point( 0, 2 );
                    break;
            }
        }

        /// <summary>
        /// Завершает передвижение условного элемента по рабочему полю.
        /// </summary>
        /// <param name="finish_moving_point"></param>
        private void finishMovingCondition( Point finish_moving_point ) {
            if ( this.is_condition_moving && this.is_mouse_left_button_down && this.condition_element != null ) {
                this.DeleteConditionsConnection( this.condition_element );
                this.condition_element.ConnectPoint = finish_moving_point;
                ModuleBorder border = Helpers.PreviewFindChildWithMouse<ModuleBorder>( this );
                if ( border != null && border.IsActive ) {
                    border.connectCondition( this.condition_element, finish_moving_point );
                }
                this.is_condition_moving = false;
                this.condition_element = null;
            }
        }

        #endregion

        #region Работа со стрелками

        private bool is_polyline = false;

        /// <summary>
        /// Флаг, говорящий о том, перемещается ли в данный момент начальная точка какой-либо стрелки по рабочему полю.
        /// </summary>
        private bool is_start_arrow_moving = false;
        /// <summary>
        /// Возвращает флаг, говорящий о том, перемещается ли в данный момент начальная точка какой-либо 
        /// стрелки по рабочему полю.
        /// </summary>
        public bool IsStartArrowMoving {
            get { return this.is_start_arrow_moving; }
        }

        /// <summary>
        /// Флаг, говорящий о том, перемещается ли в данный момент конечная точка какой-либо стрелки по рабочему полю.
        /// </summary>
        private bool is_end_arrow_moving = false;
        /// <summary>
        /// Возвращает или задает флаг, говорящий о том, перемещается ли в данный момент конечная точка какой-либо 
        /// стрелки по рабочему полю.
        /// </summary>
        public bool IsEndArrowMoving {
            get { return this.is_end_arrow_moving; }
        }

        /// <summary>
        /// Возвращает или задает флаг, говорящий о том, перемещается ли в данный момент конечная или начальная точка 
        /// какой-либо стрелки по рабочему полю.
        /// </summary>
        public bool IsArrowMoving {
            get { return this.is_start_arrow_moving || this.is_end_arrow_moving; }
        }

        /// <summary>
        /// Флаг, говорящий о том, происходит ли в данный момент добавление новой стрелки на рабочую область.
        /// </summary>
        private bool is_arrow_adding = false;
        // <summary>
        /// Возвращает или задает флаг, говорящий о том, происходит ли в данный момент добавление новой стрелки 
        /// на рабочую панель.
        /// </summary>
        public bool IsArrowAdding {
            get {
                return this.is_arrow_adding;
            }
        }

        /// <summary>
        /// Добавляемая в данный момент на рабочую панель стрелка.
        /// </summary>
        private Arrows adding_arrow;
        /// <summary>
        /// Возвращает добавляемую в данный момент на рабочую панель стрелку.
        /// </summary>
        public Arrows AddingArrow {
            get {
                return this.adding_arrow;
            }
        }

        /// <summary>
        /// Принудительно прекращает добавление стрелки на рабочую панель.
        /// </summary>
        public void stopArrowAdding() {
            this.is_arrow_adding = false;
            this.adding_arrow = null;
            if ( this.IsMouseCaptured ) {
                this.ReleaseMouseCapture();
            }
        }

        /// <summary>
        /// Инициализирует добавление <see cref="Arrows"/> типа "Последовательный вызов" на рабочую панель.
        /// </summary>
        public void addConsecutiveCall( bool is_polyline = false ) {
            this.is_polyline = is_polyline;
            this.is_condition_adding = false;
            this.condition_element = null;
            is_arrow_adding = true;
            adding_arrow = Arrows.ConsecutiveCall( new Point( 0, 0 ), new Point( 0, 0 ), false );
        }

        /// <summary>
        /// Инициализирует добавление <see cref="Arrows"/> типа "Параллельный вызов" на рабочую панель.
        /// </summary>
        public void addParallelCall( bool is_polyline = false ) {
            this.is_polyline = is_polyline;
            this.is_condition_adding = false;
            this.condition_element = null;
            this.is_arrow_adding = true;
            this.adding_arrow = Arrows.ParallelCall( new Point( 0, 0 ), new Point( 0, 0 ), false );
        }

        /// <summary>
        /// Инициализирует перемещение начальной точки передонной стрелки.
        /// </summary>
        /// <param name="arrow">Перемещаемая стрелка.</param>
        public void doStartArrowMoving( Arrows arrow ) {
            this.is_polyline = arrow.IsPolyline;
            arrow.IsPolyline = false;
            this.is_arrow_adding = false;
            this.is_mouse_left_button_down = true;
            this.is_start_arrow_moving = true;
            this.adding_arrow = arrow;
        }

        /// <summary>
        /// Инициализирует перемещение конечной точки передонной стрелки.
        /// </summary>
        /// <param name="arrow">Перемещаемая стрелка.</param>
        public void doEndArrowMoving( Arrows arrow ) {
            this.is_polyline = arrow.IsPolyline;
            arrow.IsPolyline = false;
            this.is_arrow_adding = false;
            this.is_mouse_left_button_down = true;
            this.is_end_arrow_moving = true;
            this.adding_arrow = arrow;
        }

        #endregion

        #region Контроль пересечения объектов

        public bool canLocale( IDiagrammElement uielement ) {
            Rect uielement_rect = new Rect(
                uielement.Center.X - ( uielement as Control ).ActualWidth / 2,
                uielement.Center.Y - ( uielement as Control ).ActualHeight / 2,
                ( uielement as Control ).ActualWidth,
                ( uielement as Control ).ActualHeight
            );
            List<IDiagrammElement> elements = new List<IDiagrammElement>();
            Helpers.FindAllDiagrammElements( WorkPanel.CurrentWorkPanel, elements );
            foreach ( IDiagrammElement element in elements ) {
                if ( element != uielement ) {
                    Rect rect = new Rect(
                        element.Center.X - ( element as UserControl ).ActualWidth / 2,
                        element.Center.Y - ( element as UserControl ).ActualHeight / 2,
                        ( element as UserControl ).ActualWidth,
                        ( element as UserControl ).ActualHeight
                    );
                    if ( rect.IntersectsWith( uielement_rect ) ) {
                        return false;
                    }
                }
            }
            return true;
        }

        public void updateConectionsUnderControl(Control element) {
            List<IConnectionElement> connection_list = new List<IConnectionElement>();
            Helpers.FindAllConnectionElements( WorkPanel.CurrentWorkPanel, connection_list );
            Rect container_rect = new Rect(
                    Canvas.GetLeft( element ),
                    Canvas.GetTop( element ),
                    element.ActualWidth,
                    element.ActualHeight
                );
            List<Rect> connection_element_geometries = null;
            foreach ( IConnectionElement connection_element in connection_list ) {
                connection_element_geometries = connection_element.getGeometries();
                bool is_empty = new CanvasCell(
                        container_rect.X,
                        container_rect.Y,
                        container_rect.Width,
                        container_rect.Height,
                        connection_element_geometries
                    ).Empty;
                if ( !is_empty || connection_element_geometries.Count == 0 ) {
                    connection_element.Update();
                }
            }
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие PreviewMouseLeftButtonDown для <see cref="WorkPanel"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void WorkPanel_PreviewMouseLeftButtonDown( object sender, MouseButtonEventArgs e ) {
            if ( !this.is_mouse_left_button_down ) {
                if ( this.is_condition_adding ) {
                    if ( this.is_if_else ) {
                        this.condition_element = Conditions.IfElseCondition( ConnectOrientation.BOTTOM, e.GetPosition( this ) );
                    } else {
                        this.condition_element = Conditions.IfCondition( ConnectOrientation.BOTTOM, e.GetPosition( this ) );
                    }
                    this.Children.Insert( 0, this.condition_element );
                    ModuleBorder border = Helpers.FindChildWithMouse<ModuleBorder>( this );
                    if ( border != null && border.IsActive ) {
                        border.connectCondition( this.condition_element, e.GetPosition( this ) );
                    }
                } else {
                    this.is_condition_adding = false;
                    this.condition_element = null;
                }
                if ( this.is_arrow_adding && this.adding_arrow != null ) {                    
                    this.adding_arrow.StartPoint = e.GetPosition( this );
                    this.adding_arrow.EndPoint = e.GetPosition( this );
                    this.is_end_arrow_moving = true;
                    this.Children.Insert( this.Children.Count, adding_arrow );
                    Borders border = Helpers.FindChildWithMouse<Borders>( this );
                    if ( border != null ) {
                        border.connectArrow( this.adding_arrow, InOrOutArrow.OUT );
                    }
                } else {
                    this.is_arrow_adding = false;
                    this.adding_arrow = null;
                }
            }
            this.is_mouse_left_button_down = true;
        }

        /// <summary>
        /// Описывает реакцию на событие PreviewMouseMove для <see cref="WorkPanel"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void WorkPanel_PreviewMouseMove( object sender, MouseEventArgs e ) {
            if ( e.LeftButton == MouseButtonState.Pressed ) {
                if ( this.is_condition_moving && this.condition_element != null ) {
                    this.condition_element.ConnectPoint = new Point(
                                e.GetPosition( this ).X + this.moving_offset.X,
                                e.GetPosition( this ).Y + this.moving_offset.Y
                            );
                }
                if ( this.adding_arrow != null ) {
                    double angle = Math.Atan2( ( adding_arrow.EndPoint.Y - adding_arrow.StartPoint.Y ),
                            ( adding_arrow.EndPoint.X - adding_arrow.StartPoint.X ) );
                    if ( this.is_arrow_adding ) {                        
                        this.adding_arrow.EndPoint = new Point( e.GetPosition( this ).X - 4 * Math.Cos( angle ),
                            e.GetPosition( this ).Y - 4 * Math.Sin( angle ) );
                    }
                    if ( this.is_start_arrow_moving ) {                        
                        this.adding_arrow.StartPoint = new Point( e.GetPosition( this ).X + 4 * Math.Cos( angle ),
                            e.GetPosition( this ).Y + 4 * Math.Sin( angle ) );
                    }
                    if ( this.is_end_arrow_moving ) {
                        this.adding_arrow.EndPoint = new Point( e.GetPosition( this ).X - 4 * Math.Cos( angle ),
                            e.GetPosition( this ).Y - 4 * Math.Sin( angle ) );
                    }
                }
            }
        }

        /// <summary>
        /// Описывает реакцию на событие PreviewMouseLeftButtonUp для <see cref="WorkPanel"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void WorkPanel_PreviewMouseLeftButtonUp( object sender, MouseButtonEventArgs e ) {
            if ( this.is_start_arrow_moving && this.adding_arrow != null && this.is_mouse_left_button_down ) {
                this.DeleteAllStartArrowConnections( this.adding_arrow );
                Borders border = Helpers.PreviewFindChildWithMouse<Borders>( this );
                if ( border != null && border.IsActive ) {
                    border.connectArrow( this.adding_arrow, InOrOutArrow.OUT );
                }
                this.adding_arrow.IsPolyline = this.is_polyline;
            }
            if ( this.is_end_arrow_moving && this.adding_arrow != null && this.is_mouse_left_button_down ) {
                this.DeleteAllEndArrowConnections( this.adding_arrow );
                Borders border = Helpers.PreviewFindChildWithMouse<Borders>( this );
                if ( border != null && border.IsActive ) {
                    border.connectArrow( this.adding_arrow, InOrOutArrow.IN );
                }
                this.adding_arrow.IsPolyline = this.is_polyline;
            }
            if ( this.is_arrow_adding && this.adding_arrow != null && this.is_mouse_left_button_down ) {
                Borders border = Helpers.PreviewFindChildWithMouse<Borders>( this );
                if ( border != null && border.IsActive ) {
                    border.connectArrow( this.adding_arrow, InOrOutArrow.IN );
                }
                this.adding_arrow.IsPolyline = this.is_polyline;
            }
            this.is_start_arrow_moving = this.is_end_arrow_moving = this.is_arrow_adding = false;
            this.adding_arrow = null;
            this.finishMovingCondition( e.GetPosition( this ) );
            this.is_condition_adding = false;
            this.condition_element = null;
            this.is_mouse_left_button_down = false;
        }

        /// <summary>
        /// Описывает реакцию на событие GotFocus для <see cref="WorkPanel"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void WorkPanel_GotFocus( object sender, RoutedEventArgs e ) {
            FocusManager.SetFocusedElement( this, null );
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Описывает реакцию на событие Drop для <see cref="WorkPanel"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void WorKPanel_Drop( object sender, DragEventArgs e ) {
            if ( e.Data.GetData( System.Windows.DataFormats.StringFormat ) as string == "Module" ) {
                Module module = new Module( e.GetPosition( this ) );
                module.Text = "Модуль";
                module.getModule().Measure( new Size( double.PositiveInfinity, double.PositiveInfinity ) );
                module.getModule().Arrange( new Rect( module.getModule().DesiredSize ) );
                if ( this.canLocale( module ) ) {
                    this.Children.Insert( 0, module.getModule() );
                    this.updateConectionsUnderControl( module.getModule() );
                } else {
                    DialogWithUser.showMessage( "Элементы диаграммы не должны пересекаться." );
                }
            }
            if ( e.Data.GetData( System.Windows.DataFormats.StringFormat ) as string == "DataArea" ) {
                DataArea data_area = new DataArea( e.GetPosition( this ) );
                data_area.Text = "Область данных";
                data_area.getDataArea().Measure( new Size( double.PositiveInfinity, double.PositiveInfinity ) );
                data_area.getDataArea().Arrange( new Rect( data_area.getDataArea().DesiredSize ) );
                if ( this.canLocale( data_area ) ) {
                    this.Children.Insert( 0, data_area.getDataArea() );
                    this.updateConectionsUnderControl( data_area.getDataArea() );
                } else {
                    DialogWithUser.showMessage( "Элементы диаграммы не должны пересекаться." );
                }
            }
            if ( e.Data.GetData( System.Windows.DataFormats.StringFormat ) as string == "Comment" ) {
                Comment comment = new Comment( e.GetPosition( this ) );
                comment.Text = "Новый комментарий";
                comment.getComment().Measure( new Size( double.PositiveInfinity, double.PositiveInfinity ) );
                comment.getComment().Arrange( new Rect( comment.getComment().DesiredSize ) );
                if ( this.canLocale( comment ) ) {
                    this.Children.Insert( 0, comment.getComment() );
                    this.updateConectionsUnderControl( comment.getComment() );
                } else {
                    DialogWithUser.showMessage( "Элементы диаграммы не должны пересекаться." );
                }
            }
        }

        /// <summary>
        /// Описывает реакцию на событие Click для <see cref="WorkPanel"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void WorkPanel_Click( object sender, RoutedEventArgs e ) {
            if ( this.is_deleting ) {
                int i = 0;
                while ( i < this.Children.Count && !this.Children[i].IsMouseOver ) ++i;
                if ( i < this.Children.Count && this.Children[i] is Arrows ) {
                    DeleteAllStartArrowConnections( this.Children[i] as Arrows );
                    DeleteAllEndArrowConnections( this.Children[i] as Arrows );
                }
                if ( i < this.Children.Count && ( this.Children[i] is Conditions || this.Children[i] is ConditionArrows ) ) {
                    Conditions condition = null;
                    if ( this.Children[i] is Conditions ) {
                        condition = this.Children[i] as Conditions;
                    } else {
                        condition = ( this.Children[i] as ConditionArrows ).Condition;
                    }
                    Arrows if_arrow = condition.IfArrow;
                    Arrows else_arrow = condition.ElseArrow;
                    if ( i < this.Children.Count ) this.Children.RemoveAt( i );
                    this.deleteElement( if_arrow );
                    if ( else_arrow != null ) {
                        this.deleteElement( else_arrow );
                    }
                    this.deleteElement( condition );
                } else {
                    if ( i < this.Children.Count ) this.Children.RemoveAt( i );
                }
                this.IsDeleting = false;
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Увеличивает id всех элементов диаграммы на рабочем поле на переданное значение.
        /// </summary>
        /// <param name="inc_value">Значение на которое необходимо увеличить id элементов.</param>
        public void incAllElemetsId(uint inc_value) {
            List<ISerializeble> list = new List<ISerializeble>();
            Helpers.FindAllSerializedElements( this, list );
            foreach ( ISerializeble element in list ) {
                element.Id += inc_value;
            }
        } 

        /// <summary>
        /// Добавляет шаблон к проекту.
        /// </summary>
        /// <param name="stream">Поток из которого читаются данные шаблона.</param>
        public void AddTemplateToProject( FileStream stream ) {
            StreamReader string_reader = new StreamReader( stream );
            string input = string_reader.ReadToEnd();
            string_reader.Close();
            stream.Close();
            List<string> input_list = input.Split( new string[] { "\r\n" }, StringSplitOptions.None ).ToList();
            string hash = input_list[0];
            input_list.RemoveAt( 0 );
            input_list.RemoveAt( 0 );
            hash = Serialize.getSerializeStringParam( hash, "Hash : " );
            // Проверяем целостность файла
            if ( MD5Hash.GetHashString( String.Join( "\r\n", input_list ) ) == hash ) {
                if ( input_list[0] == "Constantine Diagramm Temlate" ) {
                    input_list.RemoveAt( 0 );
                    uint temp_id = Serialize.getSerializeUintParam( input_list[0], "LastId : " );
                    WorkPanel.CurrentWorkPanel.incAllElemetsId( temp_id + 10);
                    WorkPanel.LastId += temp_id + 10;
                    input_list.RemoveAt( 0 );
                    double template_width = Serialize.getSerializeDoubleParam( input_list[0], "Width : " );
                    input_list.RemoveAt( 0 );
                    double template_height = Serialize.getSerializeDoubleParam( input_list[0], "Height : " );
                    input_list.RemoveAt( 0 );
                    Line diag = WorkPanel.CurrentWorkPanel.getMinRectDiag();
                    double shift_left = ( ( template_width - diag.X1 ) > 0 ) ? ( template_width - diag.X1 ) : 0;
                    WorkPanel.CurrentWorkPanel.shiftAllElements( -shift_left, 0 );
                    input_list.RemoveAt( 0 );
                }
                Serialize.deserializeParameters( input_list );
            } else {
                MessageBox.Show( "Входный файл был поврежден или содержит ошибку." );
            }
        }

        /// <summary>
        /// Сдвигает все элементы на рабочей панели.
        /// </summary>
        /// <param name="left_shift">Сдвиг влево.</param>
        /// <param name="top_shift">Сдвиг вверх.</param>
        public void shiftAllElements( double left_shift, double top_shift ) {
            foreach ( UIElement element in this.InternalChildren ) {
                if ( element is Arrows && !(element is ConditionArrows) ) {
                    ( element as Arrows ).StartPoint = new Point(
                            ( element as Arrows ).StartPoint.X - left_shift,
                            ( element as Arrows ).StartPoint.Y - top_shift
                        );
                    ( element as Arrows ).EndPoint = new Point(
                            ( element as Arrows ).EndPoint.X - left_shift,
                            ( element as Arrows ).EndPoint.Y - top_shift
                        );
                }
            }
            double left = 0;
            double top = 0;
            foreach ( UIElement element in this.InternalChildren ) {
                if ( element is Conditions ) {
                    ( element as Conditions ).ConnectPoint = new Point(
                            ( element as Conditions ).ConnectPoint.X - left_shift,
                            ( element as Conditions ).ConnectPoint.Y - top_shift
                        );
                }
            }
            foreach ( UIElement element in this.InternalChildren ) {
                if ( element is Conditions ) {
                    continue;
                } else {
                    left = Canvas.GetLeft( element );
                    if ( !double.IsNaN( left ) ) {
                        if ( element is ContentContainer ) {
                            ( element as ContentContainer ).Center = new Point(
                                    ( element as ContentContainer ).Center.X - left_shift,
                                    ( element as ContentContainer ).Center.Y
                                );
                        } else {
                            Canvas.SetLeft( element, left - left_shift );
                        }
                    }
                    top = Canvas.GetTop( element );
                    if ( !double.IsNaN( top ) ) {
                        if ( element is ContentContainer ) {
                            ( element as ContentContainer ).Center = new Point(
                                     ( element as ContentContainer ).Center.X,
                                     ( element as ContentContainer ).Center.Y - top_shift
                                 );
                        } else {
                            Canvas.SetTop( element, top - top_shift );
                        }
                    }
                }
            }
            foreach ( UIElement element in this.InternalChildren ) {
                 if ( element is ContentContainer ) {
                    if ( ( element as ContentContainer ).Content is Module ) {
                        ( ( element as ContentContainer ).Content as Module ).updateConditionConnections();
                    }
                }
            }
        }

        /// <summary>
        /// Получает диагональ прямоугольника минимальной площади 
        /// в который умещаются все элементы на рабочем поле.
        /// </summary>
        /// <returns>Вычисленная диагональ.</returns>
        public Line getMinRectDiag() {
            Line ret = new Line();
            ret.X1 = this.ActualWidth;
            ret.Y1 = this.ActualHeight;
            ret.X2 = 0;
            ret.Y2 = 0;
            foreach ( UIElement element in this.InternalChildren ) {
                if ( element == btn_WorkPanel ) {
                    continue;
                } else {
                    // Связи
                    if ( element is IConnectionElement ) {
                        foreach ( Rect rect in ( element as IConnectionElement ).getGeometries() ) {
                            /*new RectanglePrintDebug( new Point( ( rect.X + rect.Width - rect.X ) / 2, ( rect.Height - rect.Y ) / 2 ),
                                        rect.Width, rect.Height, Brushes.Red );*/
                            ret.X1 = ( rect.X < ret.X1 ) ? rect.X - 5 : ret.X1;
                            ret.Y1 = ( rect.Y < ret.Y1 ) ? rect.Y - 5 : ret.Y1;
                            ret.X2 = ( rect.X + rect.Width > ret.X2 ) ? rect.X + rect.Width + 5 : ret.X2;
                            ret.Y2 = ( rect.Y + rect.Height > ret.Y2 ) ? rect.Y + rect.Height + 5 : ret.Y2;
                        }
                        // Элементы диаграммы
                    } else {
                        ret.X1 = ( Canvas.GetLeft( element ) < ret.X1 ) ? Canvas.GetLeft( element ) : ret.X1;
                        ret.Y1 = ( Canvas.GetTop( element ) < ret.Y1 ) ? Canvas.GetTop( element ) : ret.Y1;
                        ret.X2 = ( Canvas.GetLeft( element ) + element.DesiredSize.Width > ret.X2 ) ?
                                   Canvas.GetLeft( element ) + element.DesiredSize.Width : ret.X2;
                        ret.Y2 = ( Canvas.GetTop( element ) + element.DesiredSize.Height > ret.Y2 ) ?
                                   Canvas.GetTop( element ) + element.DesiredSize.Height : ret.Y2;
                    }
                }
            }
            ret.X1 = ( ret.X2 < ret.X1 ) ? ret.X2 : ret.X1;
            ret.Y1 = ( ret.Y2 < ret.Y1 ) ? ret.Y2 : ret.Y1;
            /*new RectanglePrintDebug( new Point( ( ret.X2 - ret.X1 ) / 2, ( ret.Y2 - ret.Y1 ) / 2 ),
                ret.X2 - ret.X1, ret.Y2 - ret.Y1, Brushes.Red );*/
            return ret;
        }

        /// <summary>
        /// Экспортирует содержимое рабочего поля в картинку.
        /// </summary>
        /// <param name="outStream">Выходной файловый поток.</param>
        public void exportCanvasToImage( string filename, Line size_diag = null ) {
            FileStream outStream = new FileStream( filename, FileMode.Create );
            this.Focus();            
            Transform transform = this.LayoutTransform;
            this.LayoutTransform = null;
            Size size = new Size();
            if ( size_diag == null ) {
                size = new Size( this.ActualWidth, this.ActualHeight );
            } else {
                size = new Size (
                        Math.Abs( size_diag.X2 - size_diag.X1 ),
                        Math.Abs( size_diag.Y2 - size_diag.Y1 ) 
                    );
            }
            this.Measure( size );
            this.Arrange( new Rect( size ) );
            this.UpdateLayout();
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap(
                                                        (int) size.Width,
                                                        (int) size.Height,
                                                        96d,
                                                        96d,
                                                        PixelFormats.Pbgra32
                                                    );
            renderBitmap.Render( this );
            BitmapEncoder encoder;
            switch ( outStream.Name.Substring( outStream.Name.LastIndexOf( '.' ) ) ) {
                case "jpg":
                    encoder = new JpegBitmapEncoder();
                    break;
                case "png":
                    encoder = new PngBitmapEncoder();
                    break;
                case "gif":
                    encoder = new GifBitmapEncoder();
                    break;
                case "bmp":
                    encoder = new BmpBitmapEncoder();
                    break;
                default:
                    encoder = new TiffBitmapEncoder();
                    break;
            }
            encoder.Frames.Add( BitmapFrame.Create( renderBitmap ) );
            encoder.Save( outStream );
            outStream.Close();
            this.LayoutTransform = transform;
        }

        /// <summary>
        /// Очищает рабочу область <see cref="WorkPanel"/>.
        /// </summary>
        public void clear() {
            WorkPanel.LastId = 0;
            for ( int i = this.Children.Count - 1; i >= 0; --i ) {
                if ( this.Children[i] != btn_WorkPanel ) {
                    this.Children.RemoveAt( i );
                }
            }
        }

        #endregion

        #region Статические методы

        /// <summary>
        /// Получает новый id для элемента.
        /// </summary>
        /// <returns>Id/</returns>
        public static uint getNextId() {
            ++WorkPanel.LastId;
            return WorkPanel.LastId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public static void setLastId(uint id) {
            WorkPanel.LastId = id;
        }

        #endregion

        #region Перегруженные функции-члены

        /// <summary>
        /// Перегруженный метод MeasureOverride для <see cref="WorkPanel"/>.
        /// </summary>
        protected override Size MeasureOverride( Size constraint ) {
            Size size = new Size();
            double min_left = double.PositiveInfinity;
            double min_top = double.PositiveInfinity;
            foreach ( UIElement element in this.InternalChildren ) {
                if ( element == btn_WorkPanel ) {
                    continue;
                } else {
                    double left = 0;
                    double top = 0;
                    // Связи
                    if ( element is IConnectionElement ) {
                        List<Rect> rects = (element as IConnectionElement).getGeometries();
                        foreach ( Rect rect in rects ) {
                            left = rect.X;
                            if ( left < min_left ) {
                                min_left = left;
                            }
                            top = rect.Y;
                            if ( top < min_top ) {
                                min_top = top;
                            }
                            size.Width = Math.Max( size.Width, Math.Abs( left ) + rect.Width );
                            size.Height = Math.Max( size.Height, Math.Abs( top ) + rect.Height );
                        }
                    // Элементы диаграммы
                    } else {
                        left = Canvas.GetLeft( element );
                        top = Canvas.GetTop( element );
                        left = double.IsNaN( left ) ? 0 : left;
                        if ( left < min_left ) {
                            min_left = left;
                        }
                        top = double.IsNaN( top ) ? 0 : top;
                        if ( top < min_top ) {
                            min_top = top;
                        }
                        element.Measure( constraint );
                        Size desiredSize = element.DesiredSize;
                        if ( !double.IsNaN( desiredSize.Width ) && !double.IsNaN( desiredSize.Height ) ) {
                            size.Width = Math.Max( size.Width, Math.Abs(left) + desiredSize.Width );
                            size.Height = Math.Max( size.Height, Math.Abs(top) + desiredSize.Height );
                        }
                    }
                }
            }
            if ( ( this.Parent as ScrollViewer ).ActualWidth < size.Width || min_left < 0 ) {
                if ( min_left != double.PositiveInfinity ) {
                    size.Width -= min_left;
                }
                this.shiftAllElements( min_left, 0 );
            }
            if ( ( this.Parent as ScrollViewer ).ActualHeight < size.Height || min_top < 0 ) {
                if ( min_top != double.PositiveInfinity ) {
                    size.Height -= min_top;
                }
                this.shiftAllElements( 0, min_top );
            }
            return size;
        }

        #endregion               
    }
}
