﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Constantine_Diagram.Elements;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.ProjectHelpers;

namespace Constantine_Diagram.UserControls
{
    /// <summary>
    /// Реализует элемент типа "Область данных"
    /// </summary>
    public partial class DataArea : UserControl, IDiagrammElement, ISerializeble
    {
        #region Поля

        /// <summary>
        /// Комментарий к элементу <see cref="DataArea"/>.
        /// </summary>
        private TextBlock text_block;

        /// <summary>
        /// Объект <see cref="ContentContainer"/> в котором расположен текущий объект <see cref="DataArea"/>
        /// </summary>
        private ContentContainer container;

        #endregion

        #region Зависимые свойства

        /// <summary>
        /// Зависимое свойство задающее центр для <see cref="DataArea"/>.
        /// </summary>
        public static readonly DependencyProperty CenterPointProperty = DependencyProperty.Register( "Center", typeof( Point ), typeof( DataArea ), new FrameworkPropertyMetadata( new Point( 0, 0 ), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, centerUpdate ) );

        /// <summary>
        /// Возвращает или задает центр для <see cref="DataArea"/>. 
        /// Это свойство зависимости.
        /// </summary> 
        public Point Center {
            get {
                if ( container != null ) {
                    return this.container.Center;
                }
                else {
                    return this.OutBorder.Center;
                }
            }
            set {
                SetValue( CenterPointProperty, value );
                if ( container != null ) {
                    container.Center = value;
                }
                if ( this.OutBorder != null ) {
                    this.OutBorder.Center = value;
                }
            }
        }

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает id данного <see cref="DataArea"/>.
        /// </summary>
        public uint Id {
            get;
            set;
        }

        /// <summary>
        /// Внешний бордер.
        /// </summary>
        public DataAreaBorder OutBorder { get; private set; }

        /// <summary>
        /// Возвращает или задает текст содержащийся внутри <see cref="DataArea"/>.
        /// </summary>
        public string Text {
            get { return this.text_box.Text; }
            set { this.text_box.Text = value; }
        }

        /// <summary>
        /// Возвращает или задает ширину <see cref="ContentContainer"/> в котором находиться область данных.
        /// </summary>
        public double ContainerWidth {
            get { return container.ActualWidth; }
            set { container.Width = value; }
        }

        /// <summary>
        /// Возвращает или задает высоту <see cref="ContentContainer"/> в котором находиться область данных.
        /// </summary>
        public double ContainerHeight {
            get { return container.ActualHeight; }
            set { container.Height = value; }
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Инициализирует объект <see cref="DataArea"/>.
        /// </summary>
        private DataArea() {
            InitializeComponent();
            this.Id = WorkPanel.getNextId();
        }

        /// <summary>
        /// Инициализирует объект <see cref="DataArea"/> и центрирует его в заданной точке.
        /// </summary>
        /// <param name="center">Центр объекта <see cref="DataArea"/>.</param>
        public DataArea( Point center ) {
            container = new ContentContainer( this, new Point( center.X, center.Y ) );
            container.Width += 20;
            container.Height += 20;
            InitializeComponent();
            this.Id = WorkPanel.getNextId();
            this.PreviewMouseMove += new MouseEventHandler( DataArea_PreviewMouseMove );
        }

        #endregion

        #region Методы

        /// <summary>
        /// Позвращает готовый для отрисовки объект области данных.
        /// </summary>
        /// <returns>Объект <see cref="DataArea"/> типа UIElement.</returns>
        public ContentContainer getDataArea() {
            return this.container;
        }

        /// <summary>
        /// Дает фокус полю ввода текста в <see cref="DataArea"/>.
        /// </summary>
        public void focusTextBox() {
            ( this.text_box as InternalDiagramElementTextBox ).focus();
        }

        /// <summary>
        /// Сериализует объект типа <see cref="DataArea"/>.
        /// </summary>
        /// <returns>Строка с сериализованными данными.</returns>
        public string serialize() {
            string ret = "DataArea\r\n";
            ret += "Id : " + Id + "\r\n";
            ret += "Center.X : " + Center.X + "\r\n";
            ret += "Center.Y : " + Center.Y + "\r\n";
            ret += "ContainerWidth : " + ContainerWidth + "\r\n";
            ret += "ContainerHeight : " + ContainerHeight + "\r\n";
            ret += "Text : " + Text + "\r\n";
            ret += "\r\n";
            return ret;
        }

        /// <summary>
        /// Сериализует все связи с существующими объектами диаграммы для <see cref="DataArea"/>.
        /// </summary>
        /// <returns>Строка с сериализованными данными.</returns>
        public string serializeConnections() {
            string ret = "";
            foreach ( var connect in this.OutBorder.ConnectedArrows ) {
                ret += "DataAreaOutBorderConnnectedArrows\r\n";
                ret += "DataAreaId : " + Id + "\r\n";
                ret += "ConnectArrowID : " + connect.arrow.Id + "\r\n";
                ret += "ConnectArrowInOrOut : " + ( uint ) connect.in_or_out_arrow + "\r\n";
                ret += "ConnectArrowAngle : " + connect.angle + "\r\n";
                ret += "\r\n";
            }
            return ret;
        }

        #endregion

        #region Статические методы

        /// <summary>
        /// Десириализует объект типа <see cref="DataArea"/>.
        /// </summary>
        /// <param name="serialize_string">Сериализованные данные.</param>
        /// <returns>Десириализованный объект типа <see cref="DataArea"/>.</returns>
        public static DataArea deserialize( List<string> serialize_string, bool clipboard_operation = false ) {
            string str = serialize_string[0];
            serialize_string.RemoveAt( 0 );
            DataArea data_area = null;
            if ( str == "DataArea" ) {
                uint id = Serialize.getSerializeUintParam( serialize_string[0], "Id : " );
                if ( clipboard_operation ) {
                    id = WorkPanel.getNextId();
                }
                serialize_string.RemoveAt( 0 );
                Point center = new Point();
                center.X = Serialize.getSerializeDoubleParam( serialize_string[0], "Center.X : " );
                serialize_string.RemoveAt( 0 );
                center.Y = Serialize.getSerializeDoubleParam( serialize_string[0], "Center.Y : " );
                serialize_string.RemoveAt( 0 );
                data_area = new DataArea( center );
                data_area.Id = id;
                double width = Serialize.getSerializeDoubleParam( serialize_string[0], "ContainerWidth : " );
                data_area.ContainerWidth = width;
                serialize_string.RemoveAt( 0 );
                double height = Serialize.getSerializeDoubleParam( serialize_string[0], "ContainerHeight : " );
                data_area.ContainerHeight = height;
                serialize_string.RemoveAt( 0 );
                data_area.Text = Serialize.getSerializeStringParam( serialize_string[0], "Text : " );
                serialize_string.RemoveAt( 0 );
                data_area.getDataArea().Center = center;
                //Canvas.SetLeft( data_area, center.X - width / 2 );
                //Canvas.SetTop( data_area, center.Y - height / 2 );
            }
            return data_area;
        }

        /// <summary>
        /// Десериализует все связи с существующими объектами диаграммы для <see cref="DataArea"/>.
        /// </summary>
        /// <param name="serialize_string">Сериализованные данные.</param>
        public static void deserializeOutBorderConnnectedArrows( List<string> serialize_string ) {
            string str = serialize_string[0];
            serialize_string.RemoveAt( 0 );
            DataArea data_area = null;
            if ( str == "DataAreaOutBorderConnnectedArrows" ) {
                uint data_area_id = Serialize.getSerializeUintParam( serialize_string[0], "DataAreaId : " );
                serialize_string.RemoveAt( 0 );
                data_area = DataArea.FindById( data_area_id );
                uint id = Serialize.getSerializeUintParam( serialize_string[0], "ConnectArrowID : " );
                serialize_string.RemoveAt( 0 );
                DataAreaBorder.ConnectedArrow connect_arrow = new DataAreaBorder.ConnectedArrow();
                connect_arrow.arrow = Arrows.findArrowById( id );
                connect_arrow.in_or_out_arrow = ( InOrOutArrow ) Serialize.getSerializeUintParam(
                        serialize_string[0],
                        "ConnectArrowInOrOut : "
                    );
                serialize_string.RemoveAt( 0 );
                connect_arrow.angle =
                    Serialize.getSerializeDoubleParam( serialize_string[0], "ConnectArrowAngle : " );
                serialize_string.RemoveAt( 0 );
                data_area.OutBorder.connectArrow( connect_arrow );
            }
        }

        /// <summary>
        /// Возвращает область данных у которой id соответствует переданному. 
        /// </summary>
        /// <param name="id">Id искомой области данных.</param>
        /// <returns>Найденная область данных.</returns>
        private static DataArea FindById( uint id ) {
            foreach ( var child in WorkPanel.CurrentWorkPanel.Children ) {
                if ( child is ContentContainer &&
                     ( child as ContentContainer ).Content is DataArea &&
                     ( ( child as ContentContainer ).Content as DataArea ).Id == id ) {
                    return ( child as ContentContainer ).Content as DataArea;
                }
            }
            return null;
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на событие изменения значения <see cref="DataArea.Center"/>.
        /// </summary>
        /// <param name="d">Источник.</param>
        /// <param name="e">Параметры.</param>
        private static void centerUpdate( DependencyObject d, DependencyPropertyChangedEventArgs e ) { }

        /// <summary>
        /// Реализует реакцию на событие Initialized для подсказки над элементом области данных.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void tool_tip_Initialized( object sender, EventArgs e ) {
            text_block = sender as TextBlock;
        }

        /// <summary>
        /// Описывает реакцию на событие MouseDoubleClick для <see cref="DataArea"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void data_area_button_MouseDoubleClick( object sender, MouseButtonEventArgs e ) {
            data_area.Template = this.Resources["tDataAreaElementTextFocused"] as ControlTemplate;
            focusTextBox();
        }

        /// <summary>
        /// Описывает реакцию на событие LostKeyboardFocus для объекта типа TextBox расположенного внутри <see cref="DataArea"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void text_box_LostKeyboardFocus( object sender, KeyboardFocusChangedEventArgs e ) {
            data_area.Template = this.Resources["tDataAreaElement"] as ControlTemplate;
            container.Active = false;
        }

        /// <summary>
        /// Описывает реакцию на событие GotKeyboardFocus для объекта типа TextBox расположенного внутри <see cref="DataArea"/>.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void text_box_GotKeyboardFocus( object sender, KeyboardFocusChangedEventArgs e ) {
            container.Active = false;
        }

        #endregion

        #region Внешний бордер

        /// <summary>
        /// Описывает реакцию на событие Initialized для внешнего бордера.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void out_border_Initialized( object sender, EventArgs e ) {
            this.OutBorder = sender as DataAreaBorder;
            this.OutBorder.SetBinding( Borders.CenterProperty, this.container.CreateConnectorBinding() );
            this.OutBorder.ActiveStyle = this.Resources["sBorderActive"] as Style;
            this.OutBorder.DefaultStyle = this.Resources["sBorderDefault"] as Style;
        }

        /// <summary>
        /// Описывает реакцию на событие PreviewMouseMove для внешнего бордера.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void DataArea_PreviewMouseMove( object sender, MouseEventArgs e ) {
            if ( this.Text != "" ) {
                tool_tip.Visibility = System.Windows.Visibility.Visible;
                text_block.Text = this.Text;
            }
            else {
                tool_tip.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        #endregion
    }
}
