﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Security.Cryptography;

namespace Constantine_Diagram
{
    /// <summary>
    /// Логика взаимодействия для CryptWindow.xaml
    /// </summary>
    public partial class CryptWindow : Window
    {
        #region Конструкторы

        public CryptWindow() {
            InitializeComponent();
            txbx_Password.Focus();
        }

        #endregion

        #region События

        /// <summary>
        /// Описывает реакцию на события Click на элементе btn_Crypt.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры</param>
        private void btn_Crypt_Click( object sender, RoutedEventArgs e ) {
            if ( txbx_Password.Password == "" ) {
                MessageBox.Show( "Пароль не может быть пустым!", "Пустой пароль", MessageBoxButton.OK, MessageBoxImage.Warning );
                return;
            }
            DialogResult = true;
            this.Close();
        }
        /// <summary>
        /// Описывает реакцию на события Click на элементе btn_Cancel.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры</param>
        private void btn_Cancel_Click( object sender, RoutedEventArgs e ) {
            this.Close();
        }

        #endregion

        #region Функции-члены

        /// <summary>
        /// Получение пароля из элемента txbx_Password.
        /// </summary>
        public string getPassword() {
            return txbx_Password.Password;
        }

        #endregion
    }
}
