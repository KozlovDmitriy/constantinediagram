﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Constantine_Diagram
{
    /// <summary>
    /// Окно сохранения шаблонов.
    /// </summary>
    public partial class SaveDiagrammTemplateWindow : Window
    {
        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public SaveDiagrammTemplateWindow() {
            InitializeComponent();
            txt_box.Focus();
        }

        #endregion

        #region События

        /// <summary>
        /// Описание реакции на событие Click для btn_Save.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void btn_Save_Click( object sender, RoutedEventArgs e ) {
            if ( txt_box.Text == "" ) {
                MessageBox.Show( "Имя шаблона не может быть пустым!", "Пустое имя шаблона", MessageBoxButton.OK, MessageBoxImage.Warning );
                Project.save_template_name = null;
            }
            else if ( System.IO.Directory.GetFiles( "Templates" ).Contains( "Templates\\" + txt_box.Text + ".cdt" ) ) {
                MessageBox.Show( "Шаблон с введенным именем уже существует!", "Некорректное имя шаблона", MessageBoxButton.OK, MessageBoxImage.Warning );
                Project.save_template_name = null;
            }
            else {
                this.DialogResult = true;
                Project.save_template_name = txt_box.Text;
                this.Close();
            }
        }

        /// <summary>
        /// Описание реакции на событие Click для btn_Cancel.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void btn_Cancel_Click( object sender, RoutedEventArgs e ) {
            this.DialogResult = false;
            this.Close();
        }

        #endregion
    }
}
