﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Constantine_Diagram.Elements;
using Constantine_Diagram.Interfaces;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram.MathModel
{
    /// <summary>
    /// Реализует алгоритм обхода препятствий на диаграмме.
    /// </summary>
    public class AstarAlgoritm
    {
        #region Вложенные классы

        /// <summary>
        /// Реализует точку алгоритма
        /// </summary>
        private class PathNode
        {
            #region Независмые свойства

            /// <summary>
            /// Координаты точки на карте.
            /// </summary> 
            public Point Position {
                get;
                set;
            }

            /// <summary>
            /// Длина пути от старта (G).
            /// </summary>
            public double PathLengthFromStart {
                get;
                set;
            }

            /// <summary>
            /// Точка, из которой пришли в эту точку.
            /// </summary> 
            public PathNode CameFrom {
                get;
                set;
            }

            /// <summary>
            ///  Примерное расстояние до цели (H).
            /// </summary>
            public double HeuristicEstimatePathLength {
                get;
                set;
            }

            /// <summary>
            /// Ожидаемое полное расстояние до цели (F).
            /// </summary> 
            public double EstimateFullPathLength {
                get {
                    return this.PathLengthFromStart + this.HeuristicEstimatePathLength;
                }
            }

            #endregion
        }

        #endregion

        #region Поля

        /// <summary>
        /// Расчитанные ячейки поля типа <see cref="CanvasCell"/>.
        /// </summary>
        private static Dictionary<Point, CanvasCell> field = new Dictionary<Point, CanvasCell>();

        /// <summary>
        /// Геометрии препятствий для обхода.
        /// </summary>
        private static List<Rect> work_panel_element_rects = new List<Rect>();

        /// <summary>
        /// Стартовая точка работы алгоритма.
        /// </summary>
        private static Point start_point;

        /// <summary>
        /// Конечная точка работы алгоритма.
        /// </summary>
        private static Point goal_point;

        /// <summary>
        /// Ширина шага алгоритма.
        /// </summary>
        private static double cell_width = 10;

        /// <summary>
        /// Высота шага алгоритма.
        /// </summary>
        private static double cell_height = 10;

        #endregion

        #region Функции-члены

        /// <summary>
        /// Проверяет пуста ли ячейка <see cref="CanvasCell"/> с центром в переданной точке.
        /// </summary>
        /// <param name="point">Центр ячейки.</param>
        /// <param name="width">Ширина ячейки.</param>
        /// <param name="height">Высота ячейки.</param>
        /// <param name="optimization">Флаг говорящий о том, нужно ли пропускать при анализе
        /// уже расчитанные ячейки рабочего поля.</param>
        /// <returns>true - если ячейка пуста.</returns>
        private static bool isCellFieldEmpty( Point point, double width = 0, double height = 0, bool optimization = true ) {
            width = ( width == 0 ) ? cell_width : width;
            height = ( height == 0 ) ? cell_height : height;
            //new RectanglePrintDebug( point, width - 4, height - 4, Brushes.Red );
            if ( !optimization || !field.ContainsKey( point ) ) {
                if ( field.ContainsKey( point ) ) {
                    field.Remove( point );
                }
                field.Add( point, new CanvasCell( point.X - width / 2, point.Y - height / 2,
                    width, height, work_panel_element_rects ) );
            }
            return field[point].Empty;
        }

        /// <summary>
        /// Возвращает растояние между соседними ячейками.
        /// </summary>
        /// <returns>Растояние между соседними ячейками.</returns>
        private static int GetDistanceBetweenNeighbours() {
            return 1;
        }

        /// <summary>
        /// Рассчитывает растояние между двумя точками.
        /// </summary>
        /// <param name="from">Первая точка.</param>
        /// <param name="to">Вторая точка.</param>
        /// <returns>Растояние.</returns>
        private static double GetHeuristicPathLength( Point from, Point to ) {
            return Math.Abs( from.X - to.X ) + Math.Abs( from.Y - to.Y );
        }

        /// <summary>
        /// Удаляет пустые сегменты из сплайна.
        /// </summary>
        /// <param name="points">Точки сплайна.</param>
        private static void deleteEmptySegments( List<Point> points ) {
            if ( points.Count >= 3 ) {
                int i = 2;
                while ( i < points.Count ) {
                    if ( Math.Abs( points[i - 1].X - points[i].X ) < 0.02 &&
                         Math.Abs( points[i - 2].X - points[i - 1].X ) < 0.02 ) {
                        //new RectanglePrintDebug( points[i - 1], 10, 10, Brushes.Green );
                        points.RemoveAt( i - 1 );
                    }
                    if ( i < points.Count &&
                         Math.Abs( points[i - 1].Y - points[i].Y ) < 0.02 &&
                         Math.Abs( points[i - 2].Y - points[i - 1].Y ) < 0.02 ) {
                        //new RectanglePrintDebug( points[i - 1], 10, 10, Brushes.Green );
                        points.RemoveAt( i - 1 );
                    }
                    ++i;
                }
            }
        }

        /// <summary>
        /// Удаляет из сплайна некорректные сегменты. 
        /// </summary>
        /// <param name="points">Точки сплайна.</param>
        private static void deleteIncorrectSegments( List<Point> points ) {
            if ( points.Count > 3 &&
                 Math.Abs( points[points.Count - 1].X - points[points.Count - 2].X ) < 0.02
                 && Math.Abs( points[points.Count - 2].X - points[points.Count - 3].X ) < 0.02 ) {
                points.RemoveAt( points.Count - 2 );
            }
            if ( points.Count > 3 &&
                 Math.Abs( points[points.Count - 1].Y - points[points.Count - 2].Y ) < 0.02
                 && Math.Abs( points[points.Count - 2].Y - points[points.Count - 3].Y ) < 0.02 ) {
                points.RemoveAt( points.Count - 2 );
            }
            if ( points.Count > 1 &&
                 points[0].X != points[1].X && points[0].Y != points[1].Y ) {
                points[1] = new Point( points[0].X, points[1].Y );
            }
            if ( points.Count >= 4 ) {
                int i = 1;
                while ( i < points.Count - 2 ) {
                    if ( i >= 1 && i < points.Count - 2 &&
                         Math.Abs( points[i - 1].X - points[i].X ) < 0.02 &&
                         Math.Abs( points[i].X - points[i + 1].X ) >= 0.02 &&
                         Math.Abs( points[i + 1].X - points[i + 2].X ) < 0.02 ) {
                        Point center = new Point(
                                    ( points[i + 1].X + points[i].X ) / 2,
                                    ( points[i - 1].Y + points[i].Y ) / 2
                                );
                        double width = Math.Abs( points[i + 1].X - points[i].X );
                        double height = Math.Abs( points[i - 1].Y - points[i].Y );
                        if ( AstarAlgoritm.isCellFieldEmpty( center, width, height, false ) ) {
                            points[i] = new Point( points[i + 1].X, points[i - 1].Y );
                            AstarAlgoritm.deleteEmptySegments( points );
                            i -= 2;
                            //new RectanglePrintDebug( center, width - 4 , height - 4, Brushes.Purple );
                        }
                    }
                    if ( i >= 1 && i < points.Count - 2 &&
                         Math.Abs( points[i - 1].Y - points[i].Y ) < 0.02 &&
                         Math.Abs( points[i].Y - points[i + 1].Y ) >= 0.02 &&
                         Math.Abs( points[i + 1].Y - points[i + 2].Y ) < 0.02 ) {
                        Point center = new Point(
                                    ( points[i - 1].X + points[i].X ) / 2,
                                    ( points[i + 1].Y + points[i].Y ) / 2
                                );
                        double width = Math.Abs( points[i - 1].X - points[i].X );
                        double height = Math.Abs( points[i + 1].Y - points[i].Y );
                        if ( AstarAlgoritm.isCellFieldEmpty( center, width, height, false ) ) {
                            points[i] = new Point( points[i - 1].X, points[i + 1].Y );
                            AstarAlgoritm.deleteEmptySegments( points );
                            i -= 2;
                            //new RectanglePrintDebug( center, width - 4, height - 4, Brushes.Red );
                        }
                    }
                    ++i;
                }
            }
        }

        /// <summary>
        /// Возвращает путь от старта до переданного <see cref="PathNode"/>.
        /// </summary>
        /// <param name="pathNode">Конец пути.</param>
        /// <returns>Путь.</returns>
        private static List<Point> GetPathForNode( PathNode pathNode ) {
            var result = new List<Point>();
            var currentNode = pathNode;
            while ( currentNode != null ) {
                if ( result.Count >= 2 &&
                     ( ( currentNode.Position.X == result[result.Count - 1].X
                     && currentNode.Position.X == result[result.Count - 2].X ) ||
                     ( currentNode.Position.Y == result[result.Count - 1].Y
                     && currentNode.Position.Y == result[result.Count - 2].Y ) ) ) {
                    result[result.Count - 1] = currentNode.Position;
                }
                else {
                    result.Add( currentNode.Position );
                }
                currentNode = currentNode.CameFrom;
            }
            result.Reverse();
            if ( AstarAlgoritm.start_point != new Point( -100, -100 ) ) {
                result.Insert( 0, AstarAlgoritm.start_point );
            }
            if ( AstarAlgoritm.goal_point != new Point( -100, -100 ) ) {
                result.Insert( result.Count, AstarAlgoritm.goal_point );
            }
            return result;
        }

        /// <summary>
        /// Получает список соседних для переданной ячеек.
        /// </summary>
        /// <param name="pathNode">Анализируемая ячейка.</param>
        /// <param name="goal">Конечная точка искомого пути.</param>
        /// <returns>Список соседних ячеек.</returns>
        private static Collection<PathNode> GetNeighbours( PathNode pathNode, Point goal ) {
            var result = new Collection<PathNode>();
            // Соседними точками являются соседние по стороне клетки.
            Point[] neighbourPoints = new Point[4];
            neighbourPoints[0] = new Point( pathNode.Position.X + AstarAlgoritm.cell_width, pathNode.Position.Y );
            neighbourPoints[1] = new Point( pathNode.Position.X - AstarAlgoritm.cell_width, pathNode.Position.Y );
            neighbourPoints[2] = new Point( pathNode.Position.X, pathNode.Position.Y + AstarAlgoritm.cell_height );
            neighbourPoints[3] = new Point( pathNode.Position.X, pathNode.Position.Y - AstarAlgoritm.cell_height );
            foreach ( var point in neighbourPoints ) {
                // Проверяем, что по клетке можно ходить.
                if ( !isCellFieldEmpty( point ) )
                    continue;
                // Заполняем данные для точки маршрута.
                var neighbourNode = new PathNode() {
                    Position = point,
                    CameFrom = pathNode,
                    PathLengthFromStart = pathNode.PathLengthFromStart + GetDistanceBetweenNeighbours(),
                    HeuristicEstimatePathLength = GetHeuristicPathLength( point, goal )
                };
                result.Add( neighbourNode );
            }
            return result;
        }

        /// <summary>
        /// Получает список геометрий объектов рабочей области.
        /// </summary>
        /// <returns>Список геометрий объектов рабочей области.</returns>
        private static List<Rect> getWorkPanelElementRects() {
            List<Rect> ret = new List<Rect>();
            List<IDiagrammElement> elements = new List<IDiagrammElement>();
            Helpers.FindAllDiagrammElements( WorkPanel.CurrentWorkPanel, elements );
            foreach ( IDiagrammElement element in elements ) {
                ret.Add( new Rect(
                    element.Center.X - ( element as UserControl ).ActualWidth / 2,
                    element.Center.Y - ( element as UserControl ).ActualHeight / 2,
                    ( element as UserControl ).ActualWidth,
                    ( element as UserControl ).ActualHeight )
                );
            }
            return ret;
        }

        /// <summary>
        /// Определяет находится ли точка внутри <see cref="Rect"/> объекта.
        /// </summary>
        /// <param name="point">Анализируемая точка.</param>
        /// <param name="rect">Анализируемый <see cref="Rect"/> объект.</param>
        /// <returns>true - если точка находиться внутри <see cref="Rect"/> объекта.</returns>
        private static bool isPointInRect( Point point, Rect rect ) {
            return point.X >= rect.X && point.X <= rect.X + rect.Width
                   && point.Y >= rect.Y && point.Y <= rect.Y + rect.Height;
        }

        /// <summary>
        /// Ищет ближайшую точку на свободное место.
        /// </summary>
        /// <param name="point">Анализируемая точка.</param>
        /// <returns>Найденная точка.</returns>
        private static Point getCorrectPoint( Point point ) {
            Point ret = new Point();
            foreach ( Rect rect in AstarAlgoritm.work_panel_element_rects ) {
                /*new RectanglePrintDebug( 
                        new Point( rect.X + rect.Width / 2, rect.Y + rect.Height / 2 ), 
                        rect.Width - 4, 
                        rect.Height - 4, 
                        Brushes.Red 
                    );*/
                if ( AstarAlgoritm.isPointInRect( point, rect ) ) {
                    double L = point.X - rect.X;
                    double R = rect.X + rect.Width - point.X;
                    double T = point.Y - rect.Y;
                    double B = rect.Y + rect.Height - point.Y;
                    if ( L <= R && L <= T && L <= B ) {
                        ret = new Point( point.X - L - 13, point.Y );
                    }
                    else if ( R <= L && R <= T && R <= B ) {
                        ret = new Point( point.X + R + 13, point.Y );
                    }
                    else if ( T <= L && T <= R && T <= B ) {
                        ret = new Point( point.X, point.Y - T - 13 );
                    }
                    else if ( B <= L && B <= R && B <= T ) {
                        ret = new Point( point.X, point.Y + B + 13 );
                    }
                    return ret;
                }
            }
            return point;
        }

        #endregion

        #region Статические методы

        /// <summary>
        /// Ищет путь от начальной до целевой точки.
        /// </summary>
        /// <param name="_start">Начальная точка.</param>
        /// <param name="_goal">Целевая точка.</param>
        /// <returns>Найденный путь.</returns>
        public static List<Point> FindPath( Point _start, Point _goal ) {
            //RectanglePrintDebug.deleteAllDebugRectangles();
            AstarAlgoritm.work_panel_element_rects = AstarAlgoritm.getWorkPanelElementRects();
            AstarAlgoritm.field.Clear();
            AstarAlgoritm.start_point = new Point( -100, -100 );
            AstarAlgoritm.goal_point = new Point( -100, -100 );
            Point start = AstarAlgoritm.getCorrectPoint( new Point( _start.X, _start.Y ) );
            if ( _start.X != start.X || _start.Y != start.Y ) {
                AstarAlgoritm.start_point = _start;
            }
            Point goal = AstarAlgoritm.getCorrectPoint( new Point( _goal.X, _goal.Y ) );
            if ( _goal.X != goal.X || _goal.Y != goal.Y ) {
                AstarAlgoritm.goal_point = _goal;
            }
            int count = ( int ) ( Math.Abs( start.X - goal.X ) / 10.0 );
            AstarAlgoritm.cell_width = ( count != 0 )
                                       ? 10 + ( Math.Abs( start.X - goal.X ) % 10.0 ) / count
                                       : Math.Abs( start.X - goal.X ) % 10.0;
            count = ( int ) ( Math.Abs( start.Y - goal.Y ) / 10.0 );
            AstarAlgoritm.cell_height = ( count != 0 )
                                        ? 10 + ( Math.Abs( start.Y - goal.Y ) % 10.0 ) / count
                                        : Math.Abs( start.Y - goal.Y ) % 10.0;
            // Шаг 1.
            var closedSet = new Collection<PathNode>();
            var openSet = new Collection<PathNode>();
            // Шаг 2.
            PathNode startNode = new PathNode() {
                Position = start,
                CameFrom = null,
                PathLengthFromStart = 0,
                HeuristicEstimatePathLength = GetHeuristicPathLength( start, goal )
            };
            openSet.Add( startNode );
            int it = 0;
            while ( openSet.Count > 0 && it < 200 ) {
                ++it;
                // Шаг 3.
                var currentNode = openSet.OrderBy( node =>
                    node.EstimateFullPathLength ).ElementAt( 0 );
                // Шаг 4.
                Point position = currentNode.Position;
                var rect = new Rect( position, new Size( AstarAlgoritm.cell_width, AstarAlgoritm.cell_height ) );
                if ( Math.Abs( currentNode.Position.X - goal.X ) < 0.02
                    && Math.Abs( currentNode.Position.Y - goal.Y ) < 0.02 ) {
                    List<Point> ret = GetPathForNode( currentNode );
                    AstarAlgoritm.deleteIncorrectSegments( ret );
                    return ret;
                }
                // Шаг 5.
                openSet.Remove( currentNode );
                closedSet.Add( currentNode );
                // Шаг 6.
                foreach ( var neighbourNode in GetNeighbours( currentNode, goal ) ) {
                    // Шаг 7.
                    if ( closedSet.Count( node => node.Position == neighbourNode.Position ) <= 0 ) {
                        var openNode = openSet.FirstOrDefault( node =>
                        node.Position == neighbourNode.Position );
                        // Шаг 8.
                        if ( openNode == null ) {
                            openSet.Add( neighbourNode );
                        }
                        else {
                            if ( openNode.PathLengthFromStart > neighbourNode.PathLengthFromStart ) {
                                // Шаг 9.
                                openNode.CameFrom = currentNode;
                                openNode.PathLengthFromStart = neighbourNode.PathLengthFromStart;

                            }
                        }
                    }
                }
            }
            // Шаг 10.
            return null;
        }

        #endregion
    }
}
