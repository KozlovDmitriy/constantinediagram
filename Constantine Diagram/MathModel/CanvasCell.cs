﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Constantine_Diagram.Elements;
using Constantine_Diagram.UserControls;

namespace Constantine_Diagram.MathModel
{
    /// <summary>
    /// Представляет элементарную ячейку для работы <see cref="AstarAlgoritm"/>.
    /// </summary>
    public class CanvasCell
    {
        #region Поля

        /// <summary>
        /// Геометрическое представление ячейки.
        /// </summary>
        private Rect cell_rect = new Rect( 0, 0, 10, 10 );

        /// <summary>
        /// Содержит геометрии объектов рабочей области.
        /// </summary>
        private List<Rect> rects = new List<Rect>();

        #endregion

        #region Независимые свойства

        /// <summary>
        /// Возвращает или задает геометрическое представление ячейки.
        /// </summary>
        public Rect CellRect {
            get {
                return this.cell_rect;
            }
            private set {
                this.cell_rect = value;
            }
        }

        /// <summary>
        /// Возвращает или задает координату X левого верхнего угла ячейки.
        /// </summary>
        public double X {
            get {
                return this.cell_rect.Location.X;
            }
            set {
                this.cell_rect.X = value;
            }
        }

        /// <summary>
        /// Возвращает или задает координату Y левого верхнего угла ячейки.
        /// </summary>
        public double Y {
            get {
                return this.cell_rect.Location.Y;
            }
            set {
                this.cell_rect.Y = value;
            }
        }

        /// <summary>
        /// Возвращает или задает ширину ячейки.
        /// </summary>
        public double Width {
            get {
                return this.cell_rect.Width;
            }
            set {
                this.cell_rect.Width = value;
            }
        }

        /// <summary>
        /// Возвращает или задает высоту ячейки.
        /// </summary>
        public double Height {
            get {
                return this.cell_rect.Height;
            }
            set {
                this.cell_rect.Height = value;
            }
        }

        /// <summary>
        /// Возвращает флаг, говорящий о том, является ли данная ячейка рабочей области пустой.
        /// </summary>
        public bool Empty {
            get;
            private set;
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Инициализирует объект класса <see cref="CanvasCell"/>.
        /// </summary>
        /// <param name="X">Координата X левого верхнего угла ячейки.</param>
        /// <param name="Y">Координата Y левого верхнего угла ячейки.</param>
        /// <param name="rects">Геометрии объектов рабочей области.</param>
        public CanvasCell( double x, double y, double width, double height, List<Rect> rects ) {
            this.X = x;
            this.Y = y;
            this.Width = width;
            this.Height = height;
            this.rects = rects;
            this.Empty = this.isEmpty();
        }

        #endregion

        #region Функции-члены

        /// <summary>
        /// Проверяет, является ли данная ячейка рабочей области пустой.
        /// </summary>
        /// <returns>true - если ячейка пустая.</returns>
        private bool isEmpty() {
            foreach ( Rect rect in this.rects ) {
                if ( rect.IntersectsWith( this.cell_rect ) ) {
                    return false;
                }
            }
            return true;
        }

        #endregion
    }
}
