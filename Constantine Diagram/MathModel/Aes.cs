﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Constantine_Diagram
{
    /// <summary>
    /// Класс для шифрования методом AES.
    /// </summary>
    class Aes
    {
        // Эта строка используется в качестве значения "соли" для вызовов функций PasswordDeriveBytes.
        // Размер IV (в байтах) = (KeySize / 8). По умолчанию размер ключа 256, так что IV должна быть
        // 32 байта. Использовать строку в 16 символов дает нам 32 байта при преобразовании в массив байтов.
        private const string initVector = "tu89geji340t89u2";

        // Эта константа используется для определения размера ключа алгоритма шифрования, в нашем случае AES256
        private const int keysize = 256;

        /// <summary>
        /// Реализует шифрование строки.
        /// </summary>
        /// <param name="plainText">Текст для шифровки.</param>
        /// <param name="passPhrase">Пароль для шифрования.</param>
        public static string Encrypt( string plainText, string passPhrase ) {
            // Проверяем входные данные.
            if ( plainText == null || plainText.Length <= 0 ) {
                throw new ArgumentNullException( "cipherText" );
            }
            if ( passPhrase == null || passPhrase.Length <= 0 ) {
                throw new ArgumentNullException( "passPhrase" );
            }
            byte[] initVectorBytes = Encoding.UTF8.GetBytes( initVector );
            byte[] plainTextBytes = Encoding.UTF8.GetBytes( plainText );
            PasswordDeriveBytes password = new PasswordDeriveBytes( passPhrase, null );
            byte[] keyBytes = password.GetBytes( keysize / 8 );
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor( keyBytes, initVectorBytes );
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream( memoryStream, encryptor, CryptoStreamMode.Write );
            cryptoStream.Write( plainTextBytes, 0, plainTextBytes.Length );
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String( cipherTextBytes );
        }

        /// <summary>
        /// Реализует дешифровку строки.
        /// </summary>
        /// <param name="cipherText">Шифрованнный текст.</param>
        /// <param name="passPhrase">Пароль для дешифрования.</param>
        public static string Decrypt( string cipherText, string passPhrase ) {
            // Проверяем входные данные.
            if ( cipherText == null || cipherText.Length <= 0 ) {
                throw new ArgumentNullException( "cipherText" );
            }
            if ( passPhrase == null || passPhrase.Length <= 0 ) {
                throw new ArgumentNullException( "passPhrase" );
            }
            byte[] initVectorBytes = Encoding.ASCII.GetBytes( initVector );
            byte[] cipherTextBytes = Convert.FromBase64String( cipherText );
            PasswordDeriveBytes password = new PasswordDeriveBytes( passPhrase, null );
            byte[] keyBytes = password.GetBytes( keysize / 8 );
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor( keyBytes, initVectorBytes );
            MemoryStream memoryStream = new MemoryStream( cipherTextBytes );
            CryptoStream cryptoStream = new CryptoStream( memoryStream, decryptor, CryptoStreamMode.Read );
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read( plainTextBytes, 0, plainTextBytes.Length );
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString( plainTextBytes, 0, decryptedByteCount );
        }
    }
}
