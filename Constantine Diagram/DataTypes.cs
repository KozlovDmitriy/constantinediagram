﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constantine_Diagram
{
    /// <summary>
    /// Описывает состояние характеризующее грань привязки [LEFT|RIGHT|TOP|BOTTOM].
    /// </summary>
    public enum ConnectOrientation : uint
    {
        LEFT = 0,
        RIGHT = 1,
        TOP = 2,
        BOTTOM = 3
    }

    /// <summary>
    /// Множество классифицирующее стрелку как входящюю [IN] или исходящюю [OUT]
    /// </summary>
    public enum InOrOutArrow : uint
    {
        IN = 0,
        OUT = 1
    }
}
