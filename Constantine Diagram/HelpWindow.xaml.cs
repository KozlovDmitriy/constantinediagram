﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace Constantine_Diagram
{
    /// <summary>
    /// Реализует класс окна помощи программы.
    /// </summary>
    public partial class HelpWindow : Window
    {
        #region Конструкторы

        public HelpWindow() {
            InitializeComponent();
            this.ShowInTaskbar = false;
            try {
                help_browser.Source = new Uri( new System.IO.FileInfo( "Resources/help.mht" ).FullName );
                this.Show();
            }
            catch ( Exception ex ) {
                MessageBox.Show( ex.Message );
            }
        }

        #endregion
    }
}
